module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    screens: {
      sm: '480px',
      md: '768px',
      lg: '976px',
      xl: '1360px',
    },
    colors: {
      'blue':  "var(--blue)",
      'themBlue':  "var(--themBlue)",
      'blue-300':  "var(--blue-300)",
      'gray-300':  "var(--gray-300)",
      'white':  "var(--white)",
      'red':  "var(--red)",
      'red-100':  "var(--red-100)",
      'black':  "var(--black)",
      'black-200':  "var(--black-200)",
      'black-400':  "var(--black-400)",
      'black-600':  "var(--black-600)",
      'green':  "var(--green)",
      'backgroundBody':  "var(--backgroundBody)",


    },

    extend: {
      zIndex: {
        '100': '100',
      },
      dropShadow: {
        'xl': 'var(--shadow)',

      },
      spacing: {
        '128': '32rem',
        '144': '36rem',
      },
      lineHeight: {
        'extra-loose': '3.5',

      },


      borderRadius: {
        'sm': 'var(--rounded-sm)',
        'md': 'var(--rounded)',
        'xl': 'var(--rounded-md)',
        '2xl': 'var(--rounded-lg)',
        'full': 'var(--rounded-full)',
        '0': 'var(--rounded-none)'








      }
    }
  }
}