import React from 'react';
import {Skeleton} from 'antd';

const OrderLoading = () => {
    return (
        <div className={'w-72'}>

            <Skeleton title={false} active block={true}
                      paragraph={{
                          rows: 5,
                      }}
                      className={'!w-full'}
                      width={'100%'}  />
        </div>
    );
};

export default OrderLoading


