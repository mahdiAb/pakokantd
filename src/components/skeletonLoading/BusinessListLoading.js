import React from 'react';
import { Skeleton } from 'antd';
const BusinessListLoading = () => {


    return (<>
        {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((item, index) => (
            <div key={index} className="border-2 border-black-200 p-3   rounded-2xl defBox">

                <Skeleton width={'100%'} height={220} active
                    avatar paragraph={{rows:5}}

                />
            </div>


        ))}


    </>);
};

export default BusinessListLoading;