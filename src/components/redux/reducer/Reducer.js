const initialState = {
    accTabList: [],
    accNewTabIndex: '2',
    collapseSidebar: 1,
    accActiveKey: '1',
    accNewTab: null,


    installmentTabList: [],
    installmentNewTabIndex: '2',
    installmentActiveKey: '1',
    installmentNewTab: null,


    invoiceTabList: [],
    invoiceNewTabIndex: '2',
    invoiceActiveKey: '1',
    invoiceNewTab: null,


    settingTabList: [],
    settingNewTabIndex: '2',
    settingActiveKey: '1',
    settingNewTab: null,

    financialTabList: [],
    financialNewTabIndex: '2',
    financialActiveKey: '1',
    financialNewTab: null,
}



const Reducer = (state = initialState, action) => {


    switch (action.type) {

        case COLLAPSE_SIDEBAR:
            return {...state, collapseSidebar: action.payload}


        // case HIDDEN_SIDEBAR:
        //     return {...state, hiddenSidebar: action.payload}


        case ACC_TAB_LIST:
            return {...state, accTabList: action.payload}
        case ACC_ACTIVE_KEY:
            return {...state, accActiveKey: action.payload}
        case ACC_MULTIPLE:

            return {...state, accNewTabIndex: String(Number(state.accNewTabIndex) + 1)}
        case ACC_NEW_TAB:

            return {...state, accNewTab: action.payload}




        case INSTALLMENT_TAB_LIST:
            return {...state, installmentTabList: action.payload}
        case INSTALLMENT_ACTIVE_KEY:
            return {...state, installmentActiveKey: action.payload}
        case INSTALLMENT_MULTIPLE:

            return {...state, installmentNewTabIndex: String(Number(state.installmentNewTabIndex) + 1)}
        case INSTALLMENT_NEW_TAB:

            return {...state, installmentNewTab: action.payload}




        case INVOICE_TAB_LIST:
            return {...state, invoiceTabList: action.payload}
        case INVOICE_ACTIVE_KEY:
            return {...state, invoiceActiveKey: action.payload}
        case INVOICE_MULTIPLE:
            return {...state, invoiceNewTabIndex: String(Number(state.invoiceNewTabIndex) + 1)}
        case INVOICE_NEW_TAB:
            return {...state, invoiceNewTab: action.payload}




        case SETTING_TAB_LIST:
            return {...state, settingTabList: action.payload}
        case SETTING_ACTIVE_KEY:
            return {...state, settingActiveKey: action.payload}
        case SETTING_MULTIPLE:
            return {...state, settingNewTabIndex: String(Number(state.settingNewTabIndex) + 1)}
        case SETTING_NEW_TAB:
            return {...state, settingNewTab: action.payload}







        case FINANCIAL_TAB_LIST:
            return {...state, financialTabList: action.payload}
        case FINANCIAL_ACTIVE_KEY:
            return {...state, financialActiveKey: action.payload}
        case FINANCIAL_MULTIPLE:
            return {...state, financialNewTabIndex: String(Number(state.financialNewTabIndex) + 1)}
        case FINANCIAL_NEW_TAB:
            return {...state, financialNewTab: action.payload}


        default:
            return state;
    }
}

export default Reducer;

/*****************action************/
const COLLAPSE_SIDEBAR = "handleCollapseSidebar"
const ACC_TAB_LIST = "handleAccTabList"
const ACC_MULTIPLE = "handleAccNewTabIndex"
const ACC_ACTIVE_KEY = "handleAccActiveKey"
const ACC_NEW_TAB = "handleAccNewTab"


const INSTALLMENT_TAB_LIST = "handleInstallmentTabList"
const INSTALLMENT_MULTIPLE = "handleInstallmentNewTabIndex"
const INSTALLMENT_ACTIVE_KEY = "handleInstallmentActiveKey"
const INSTALLMENT_NEW_TAB = "handleInstallmentNewTab"


const INVOICE_TAB_LIST = "handleInvoiceTabList"
const INVOICE_MULTIPLE = "handleInvoiceNewTabIndex"
const INVOICE_ACTIVE_KEY = "handleInvoiceActiveKey"
const INVOICE_NEW_TAB = "handleInvoiceNewTab"


const SETTING_TAB_LIST = "handleSettingTabList"
const SETTING_MULTIPLE = "handleSettingNewTabIndex"
const SETTING_ACTIVE_KEY = "handleSettingActiveKey"
const SETTING_NEW_TAB = "handleSettingNewTab"


const FINANCIAL_TAB_LIST = "handleFinancialTabList"
const FINANCIAL_MULTIPLE = "handleFinancialNewTabIndex"
const FINANCIAL_ACTIVE_KEY = "handleFinancialActiveKey"
const FINANCIAL_NEW_TAB = "handleFinancialNewTab"




export function handleCollapseSidebar(x) {
    return {
        type: COLLAPSE_SIDEBAR,
        payload: x

    }
}

export function handleInstallmentTabList(x) {
    return {
        type: INSTALLMENT_TAB_LIST,
        payload: x
    }
}


export function handleInstallmentNewTabIndex() {
    return {
        type: INSTALLMENT_MULTIPLE,

    }
}

export function handleInstallmentActiveKey(x) {
    console.log(x)
    return {
        type: INSTALLMENT_ACTIVE_KEY,
        payload: x
    }
}

export function handleInstallmentNewTab(x) {
    return {
        type: INSTALLMENT_NEW_TAB,
        payload: x

    }
}


export function handleAccTabList(x) {
    return {
        type: ACC_TAB_LIST,
        payload: x
    }
}


export function handleAccNewTabIndex() {
    return {
        type: ACC_MULTIPLE,

    }
}

export function handleAccActiveKey(x) {
    return {
        type: ACC_ACTIVE_KEY,
        payload: x
    }
}

export function handleAccNewTab(x) {
    return {
        type: ACC_NEW_TAB,
        payload: x

    }
}




export function handleInvoiceTabList(x) {
    return {
        type: INVOICE_TAB_LIST,
        payload: x
    }
}


export function handleInvoiceNewTabIndex() {
    return {
        type: INVOICE_MULTIPLE,

    }
}

export function handleInvoiceActiveKey(x) {
    return {
        type: INVOICE_ACTIVE_KEY,
        payload: x
    }
}

export function handleInvoiceNewTab(x) {
    return {
        type: INVOICE_NEW_TAB,
        payload: x

    }
}


// region Setting
export function handleSettingTabList(x) {
    return {
        type: SETTING_TAB_LIST,
        payload: x
    }
}


export function handleSettingNewTabIndex() {
    return {
        type: SETTING_MULTIPLE,

    }
}

export function handleSettingActiveKey(x) {
    return {
        type: SETTING_ACTIVE_KEY,
        payload: x
    }
}

export function handleSettingNewTab(x) {
    return {
        type: SETTING_NEW_TAB,
        payload: x

    }
}

//  region end

// region Financial
export function handleFinancialTabList(x) {
    return {
        type: FINANCIAL_TAB_LIST,
        payload: x
    }
}


export function handleFinancialNewTabIndex() {
    return {
        type: FINANCIAL_MULTIPLE,

    }
}

export function handleFinancialActiveKey(x) {
    return {
        type: FINANCIAL_ACTIVE_KEY,
        payload: x
    }
}

export function handleFinancialNewTab(x) {
    return {
        type: FINANCIAL_NEW_TAB,
        payload: x

    }
}

//  region end