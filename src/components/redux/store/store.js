import {createStore} from "redux";
import {GrMoney} from "@react-icons/all-files/gr/GrMoney";

import React from 'react';

import Reducer from "../reducer/Reducer";
import PersonList from "../../pages/services/setting/person/PersonList";
import AccDashboard from "../../pages/services/acc/tabs/AccDashboard";
import InstallmentDashboard from "../../pages/services/installment/tabs/InstallmentDashboard";
import InvoiceDashboard from "../../pages/services/invoice/tabs/InvoiceDashboard";
import SettingDashboard from "../../pages/services/setting/SettingDashboard";
import FinancialDashboard from "../../pages/financial/tabs/FinancialDashboard";

export default createStore(Reducer, {
    collapseSidebar: 1,
    // hiddenSidebar: false,

    accTabList: [{label: 'داشبورد', icon: <GrMoney size={20}/>, children: <AccDashboard/>, key: '1', closable: true}],
    accNewTabIndex: '2',
    accActiveKey: '1',
    accNewTab: null,


    installmentTabList: [
        {label: 'داشبورد', icon: <GrMoney size={20}/>, children: <InstallmentDashboard/>, key: '1', closable: true}

    ],
    installmentNewTabIndex: '2',
    installmentActiveKey: '1',
    installmentNewTab: null,


    invoiceTabList: [
        {
            label: 'داشبورد',
            id: 'CreateInvoice',
            reopen: true,
            icon: <GrMoney size={20}/>,
            children: <InvoiceDashboard pattern={1}/>,
            key: '1',
            closable: true
        },

    ],
    invoiceNewTabIndex: '2',
    invoiceActiveKey: '1',
    invoiceNewTab: null,

    settingTabList: [
        {
            label: 'داشبورد',
            id: 'PersonList',
            reopen: true,
            icon: <GrMoney size={20}/>,
            children: <SettingDashboard/>,
            key: '1',
            closable: true
        }


    ],
    settingNewTabIndex: '2',
    settingActiveKey: '1',
    settingNewTab: null,



    financialTabList: [
        {
            label: 'داشبورد',
            id: 'FinancialDashboard',
            reopen: true,
            icon: <GrMoney size={20}/>,
            children: <FinancialDashboard/>,
            key: '1',
            closable: true
        }


    ],
    financialNewTabIndex: '2',
    financialActiveKey: '1',
    financialNewTab: null,


})



