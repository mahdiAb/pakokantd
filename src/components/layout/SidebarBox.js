import React, {useState,useEffect} from 'react';

import {connect} from "react-redux";
import {handleCollapseSidebar} from "../redux/reducer/Reducer";

import {Menu} from 'antd';
import SubMenu from "antd/es/menu/SubMenu";
import {AiOutlineCloseCircle} from "@react-icons/all-files/ai/AiOutlineCloseCircle";


const SidebarBox = ({
                        handleAddTab,

                        collapseSidebar,

                        items,
                        handleCollapseSidebar,

                    }) => {

    const [openKeys, setOpenKeys] = useState(['s1']);

    const rootSubmenuKeys = ['s1', 's2', 's3', 's4', 's5', 's6', 's7', 's8', 's9', 's10',];


    const multi = (data) => {
        return <SubMenu
            key={data.key}
            icon={data.icon}
            title={<span>  <span>{data.label}</span> </span>}
        >
            {
                data.submenu.map((x, i) => (<>{x.submenu?.length > 0 ? multi(x) : single(x)}</>))
            }
        </SubMenu>


    }


    const single = (data, index) => {
        // data.key = handleNewTabIndex
        return <Menu.Item key={index} icon={data.icon} onClick={() => handleAddTab(data)}>
            <span>  {data.label}</span>
        </Menu.Item>


    }

    useEffect(() => {
        const handleResize = () => {
            if (window.innerWidth < 768) {
                handleCollapseSidebar(0)
            } else if (window.innerWidth < 1200 && window.innerWidth > 768) {
                handleCollapseSidebar(2)
            } else if (window.innerWidth > 1200) {
                handleCollapseSidebar(1)
            }
        }

        handleResize()
        window.addEventListener('resize', handleResize)


    }, [])
    // const toggleCollapsed = () => {
    //     handleCollapseSidebar(!collapseSidebar);
    // };
    const onOpenChange = (keys) => {
        const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);
        if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
            setOpenKeys(keys);
        } else {
            setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
        }
    };
    return (
        <>
            <div className={`layerSidebar md:hidden ${collapseSidebar == 0 ? 'hidden' : null}`}
                 onClick={() => handleCollapseSidebar(0)}></div>
            <div
                className={`overflow-hidden transition-all duration-300 shadow-md flex flex-col justify-between fixed serviceSidebar
          
             ${collapseSidebar === 2 ? 'collapseSidebarHalfOpen' : collapseSidebar === 1 ? 'collapseSidebarOpen' : 'collapseSidebarHidden'}`}
            >

                <button
                    onClick={() => handleCollapseSidebar(0)}
                    className={'flex font-black-600 justify-end m-4 md:hidden'}>
                    <AiOutlineCloseCircle
                        size={20} color='#3d3d3d'/></button>
                <Menu
                    className={'transition-all duration-300 h-full '}
                    onOpenChange={onOpenChange}
                    defaultSelectedKeys={['1']}
                    defaultOpenKeys={['sub1']}
                    mode="inline"
                    openKeys={openKeys}
                    inlineCollapsed={collapseSidebar === 2 ? true : false}
                >

                    {items.map((data, index) => (data.submenu?.length > 0 ? multi(data, index) : single(data, index)))}


                </Menu>

            </div>
        </>
    );
};


function mapStateToProps(state) {
    return {
        collapseSidebar: state.collapseSidebar,

        tabListState: state.settingTabList,
    }
}


export default connect(mapStateToProps, {
    handleCollapseSidebar,


})(SidebarBox);