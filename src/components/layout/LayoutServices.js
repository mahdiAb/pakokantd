import React from 'react';


import SidebarBox from "./SidebarBox";
import {connect} from "react-redux";
import {handleCollapseSidebar} from "../redux/reducer/Reducer";
import {Tabs} from 'antd';


const LayoutServices = ({
                            collapseSidebar,
                            tabs, handleTabList,
                            items,
                            handleNewTabIndex,
                            hiddenSidebar,
                            handleAddTab,
                            activeKey,
                            handleActiveKey
                        }) => {




    const onChange = (newActiveKey) => {
        handleActiveKey(newActiveKey);
    };

    const remove = (targetKey) => {
        let newActiveKey = activeKey;
        let lastIndex = -1;
        tabs.forEach((item, i) => {
            if (item.key === targetKey) {
                lastIndex = i - 1;
            }
        });
        const newPanes = tabs.filter((item) => item.key !== targetKey);
        if (newPanes.length && newActiveKey === targetKey) {
            if (lastIndex >= 0) {
                newActiveKey = newPanes[lastIndex].key;
            } else {
                newActiveKey = newPanes[0].key;
            }
        }
        handleTabList(newPanes);
        handleActiveKey(newActiveKey);
    };
    const onEdit = (targetKey, action) => {
        if (action === 'add') {

        } else {
            remove(targetKey);
        }
    };


    return (<div className='servicesLayout   '>
        <SidebarBox items={items} handleAddTab={handleAddTab} handleNewTabIndex={handleNewTabIndex}/>
        <div className={`transition-all duration-300 tabsBox   
         
        ${collapseSidebar === 2 ? 'collapseSidebarHalfOpen' : collapseSidebar === 1 ? 'collapseSidebarOpen' : 'collapseSidebarHidden'}
        
        
        
        `}
             >

            <Tabs
                  onChange={onChange} onEdit={onEdit} hideAdd
                items={tabs} activeKey={activeKey} type="editable-card"

            />


        </div>


    </div>);
};

function mapStateToProps(state) {
    return {
        collapseSidebar: state.collapseSidebar,
        hiddenSidebar: state.hiddenSidebar,

    }
}


export default connect(mapStateToProps, {
    handleCollapseSidebar,

})(LayoutServices);




