const NumSperator = (num) => {
  let number = num.toString().replace(/[^0-9]/g, "");
  let final = number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return final;
};

export default NumSperator;
