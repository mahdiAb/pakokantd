const Domain = 'http://gateway.pakok.ir/api/'


const oauthApiService = Domain + 'oauth/v1/';
const Login = oauthApiService + 'user/Login'
const GetBusinessToken = oauthApiService + 'token/GetBusinessToken'


const businessService = Domain + 'business/v1/';
const PersonsService = Domain + 'Persons/v1/';
const InvoicesService = Domain + 'Invoices/v1/';
const WalletService = Domain + 'wallet/v1/';
const GetTransactionInfo = WalletService + 'Transaction/GetTransactionInfo'


const CatalogService = Domain + 'Catalog/v1/Catalog/';
const ProductCategory = Domain + 'Catalog/v1/ProductCategory/';


const BuyCheckPaymentTypesBusiness = WalletService + 'MoneyWallet/BuyCheckPaymentTypesBusiness'
const Payment = WalletService + 'Payment/Payment'
const RemainingInvoiceWallet = WalletService + 'InvoiceWallet/RemainingWallet'
const RemainingMoneyWallet = WalletService + 'MoneyWallet/RemainingWallet'
const ChargeCheckPaymentTypesInvoiceWallet = WalletService + 'InvoiceWallet/ChargeCheckPaymentTypes'
const ChargeCheckPaymentTypesMoneyWallet = WalletService + 'MoneyWallet/ChargeCheckPaymentTypes'
const Logout = oauthApiService + 'user/Logout'
const GenerateSMSOTP = oauthApiService + 'otp/GenerateSMSOTP'
const ChangeForgottenPassword = oauthApiService + 'user/ChangeForgottenPassword'
const RegisterUser = oauthApiService + 'user/RegisterUser'
const ChangePassword = oauthApiService + 'user/ChangePassword'
const GetUserByMobileNumber = oauthApiService + 'user/GetUserByMobileNumber'
const GetServiceListByBusinessTypeID = businessService + 'service/GetServiceListByBusinessTypeID'
const CalculatedCostSelectedService = businessService + 'service/CalculatedCostSelectedService'
const GetBusinessTypeList = businessService + 'Business/GetBusinessTypeList'
const GetBusinessByBusinessIDForExtension = businessService + 'Business/GetBusinessByBusinessIDForExtension'
const GetBusinessByBusinessIDForUpgrade = businessService + 'Business/GetBusinessByBusinessIDForUpgrade'
const GetBusinessList = businessService + 'Business/GetBusinessList'
const CreateBusiness = businessService + 'Business/CreateBusiness'
const UpdateBusiness = businessService + 'Business/UpdateBusiness'
const CreateMapUserBusiness = businessService + 'Business/CreateMapUserBusiness'
const AddOrderExtensionBusiness = businessService + 'order/AddOrderExtensionBusiness'
const GetOrderByBusinessId = businessService + 'order/GetOrderByBusinessId'
 const DeleteOrder = businessService + 'order/DeleteOrder'
 const GetAllMapUserBusiness = businessService + 'Business/GetAllMapUserBusiness'
 const RemoveMapUserBusiness = businessService + 'Business/RemoveMapUserBusiness'

const CreatePerson = PersonsService + 'Person/CreatePerson'
const GetAllPerson = PersonsService + 'Person/GetAllPerson'
const SearchPerson = PersonsService + 'Person/SearchPerson'
const GetAllPersonTypes = PersonsService + 'PersonType/GetAllPersonTypes'
const GetAllPersonCategory = PersonsService + 'PersonCategory/GetAllPersonCategory'
const UpdatePerson = PersonsService + 'Person/UpdatePerson'
const RemovePerson = PersonsService + 'Person/RemovePerson'
const GetPersonByID = PersonsService + 'Person/GetPersonByID'
const PersonsServiceCities = PersonsService + 'City/GetAllCities';
const PersonsServiceStates = PersonsService + 'State/GetAllStates';


const CatalogCreate = CatalogService + 'CreateCatalog'
const UpdateCatalog = CatalogService + 'UpdateCatalog'
const DeleteCatalog = CatalogService + 'DeleteCatalog'
const GetAllCatalog = CatalogService + 'GetAllCatalog'
const GetCatalogByID = CatalogService + 'GetCatalogByID'


const CreateProductCategory = ProductCategory + 'CreateProductCategory'
const EditProductCategory = ProductCategory + 'EditProductCategory'
const DeleteProductCategory = ProductCategory + 'DeleteProductCategory'
const GetProductCategoryByID = ProductCategory + 'GetProductCategoryByID'
const GetAllProductCategory = ProductCategory + 'GetAllProductCategory'



const CreateNormalSaleInvoice = InvoicesService + 'Invoice/CreateNormalSaleInvoice'
const GetInvoiceByID = InvoicesService + 'Invoice/GetInvoiceByID'
const GetAllUnSendInvoices = InvoicesService + 'Invoice/GetAllUnSendInvoices'
const SendInvoiceToTax = InvoicesService + 'Invoice/SendInvoiceToTax'
const GetAllInvoices = InvoicesService + 'Invoice/GetAllInvoices'
const GetLastSentInvoice = InvoicesService + 'Invoice/GetLastSentInvoice';
const UpdateNormalSaleInvoices = InvoicesService + 'Invoice/UpdateNormalSaleInvoices';
const InvoiceDelete = InvoicesService + 'Invoice/DeleteInvoice';
const CancellationInvoiceApi = InvoicesService + 'Invoice/CancellationInvoice';


export {
    Login, PersonsServiceCities, PersonsServiceStates,
    GetBusinessToken,InvoiceDelete,CancellationInvoiceApi,
    GetServiceListByBusinessTypeID,
    GenerateSMSOTP,GetAllInvoices,GetLastSentInvoice,
    ChangeForgottenPassword,CreateNormalSaleInvoice,
    ChangePassword,GetInvoiceByID,
    RegisterUser,GetAllUnSendInvoices,UpdateNormalSaleInvoices,
    CreateBusiness,
    CatalogCreate,
    DeleteCatalog,SendInvoiceToTax,
    GetCatalogByID,
    UpdateCatalog,
    GetAllCatalog,
    GetOrderByBusinessId,
    GetAllPerson,
    CreateProductCategory,
    EditProductCategory,
    GetProductCategoryByID,
    GetAllProductCategory,
    GetAllPersonTypes,
    GetAllPersonCategory,
    DeleteProductCategory,
    BuyCheckPaymentTypesBusiness,
    RemovePerson,
    UpdatePerson,
    GetBusinessByBusinessIDForExtension,
    CreateMapUserBusiness,
    GetBusinessTypeList,RemoveMapUserBusiness,
    GetPersonByID,
    GetBusinessList,
    CalculatedCostSelectedService,
    GetUserByMobileNumber, Payment,
    Logout,
    DeleteOrder, SearchPerson,
    CreatePerson,
    UpdateBusiness, GetTransactionInfo, GetAllMapUserBusiness,
    AddOrderExtensionBusiness,
    GetBusinessByBusinessIDForUpgrade,
    RemainingInvoiceWallet,
    RemainingMoneyWallet,
    ChargeCheckPaymentTypesInvoiceWallet,
    ChargeCheckPaymentTypesMoneyWallet,
}