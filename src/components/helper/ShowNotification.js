
import React from 'react';
import {notification} from "antd";

export const ShowNotification = (
    description, type,
    Title = type ? 'عملیات موفق' : 'عملیات ناموفق!',
    icon = type ? 'icon-Tick' : 'icon-Group-164794',
    className = type ? 'greenNotification' : 'redNotification',
    duration = 4
) => {
    notification.open({
        message: Title,
        description: description,
        icon: <i className={icon}> </i>,
        className: className,
        duration: duration
    });
};


