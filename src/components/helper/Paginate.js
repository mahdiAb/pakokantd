import React, {useEffect, useState} from 'react';
import ReactPaginate from "react-paginate";
import {AiOutlineArrowLeft} from "@react-icons/all-files/ai/AiOutlineArrowLeft";
import {AiOutlineArrowRight} from "@react-icons/all-files/ai/AiOutlineArrowRight";

const Paginate = (props) => {
    const [pageNumber, setPageNumber] = useState()


    useEffect(() => {
        setPageNumber(props.pageNumber)
    }, [props])
    const pageChange = ({selected}) => {
        setPageNumber(selected + 1)
        props.pageNumberValue(selected + 1)
    };


    return (
        <div className="flex  justify-center   items-center ReactPaginateBox">
            <ReactPaginate
                previousLabel={<AiOutlineArrowRight/>}
                nextLabel={<AiOutlineArrowLeft/>}
                breakLabel="..."
                forcePage={pageNumber - 1}
                marginPagesDisplayed={1}
                pageCount={props.totalRecord / 10}

                onPageChange={(e) => pageChange(e)}
                containerClassName="listPaginate"
                breakLinkClassName="dotsPaginate"
                nextLinkClassName="nextPaginate"
                previousLinkClassName="prevPaginate"
                pageLinkClassName="paginateNumbers"
                activeLinkClassName="active"
            />
        </div>
    );
};

export default Paginate;

 