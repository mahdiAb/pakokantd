export const ConvertToPrice = (x) => {
    return x
        ?.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

export const ConvertPriceToNumber = (x) => {

    let number =  x?.toString().replace(/,/g, '')

    return Number(number)
}



