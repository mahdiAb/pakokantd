import React, {useEffect, useState} from 'react';
import FormItem from "../uikits/FormItem";
import TextAreaBox from "../uikits/TextAreaBox";
import BusinessRequest from "../../../helper/request/BusinessRequest";
import {PersonsServiceCities, PersonsServiceStates} from "../../../helper/apiList/apiListBusiness";
import {useNavigate, useParams} from "react-router-dom";
import {Select} from "antd";


const GetAddress = ({itemClassName,showAdd, required = false}) => {
    let [cityLoading, setCityLoading] = useState(false)
    let [stateLoading, setStateLoading] = useState(true)


    let [cityList, setCityList] = useState([])
    let [stateList, setStateList] = useState([])
    const navigate = useNavigate();
    let {id} = useParams();

    const getCities = (stateId) => {

        setCityLoading(true)

        let data = {StateID: stateId}
        BusinessRequest.init(PersonsServiceCities, (data) => {

            setCityList(data.data)
            setCityLoading(false)
        }, (error) => {

        }).setBID(id).setMainData(data).setRouter(navigate).callRequest()


    }

    useEffect(() => {


        BusinessRequest.init(PersonsServiceStates, function (data) {
            setStateLoading(false)
            setStateList(data.data)


            if (data.data.messageID === 0) {


            } else {

            }
        }, function (error) {
            setStateLoading(false)

        }).setBID(id).setRouter(navigate).callRequest()


    }, [id,navigate])
    return (<>
        <div className={itemClassName}>
            <FormItem title={'استان'} valuePropName={true} name={'StateID'} required={required}>

                <Select onChange={getCities} loading={stateLoading} showSearch
                        optionFilterProp="children"

                        filterOption={(input, option) => (option?.title ?? '').toLowerCase().includes(input.toLowerCase())}



                >
                    {
                        stateList.map((item, index) => (
                            <Select.Option key={index} value={`${item.id}`}>{item.title}</Select.Option>
                        ))
                    }
                </Select>

            </FormItem>
        </div>
        <div className={itemClassName}>


            <FormItem title={'شهر'} name={'CityID'} required={required} valuePropName={true}>


                <Select loading={cityLoading} showSearch
                        optionFilterProp="children"

                        filterOption={(input, option) => (option?.title ?? '').toLowerCase().includes(input.toLowerCase())}
                >
                    {
                        cityList.map((item, index) => (
                            <Select.Option key={index} value={`${item.id}`}>{item.title}</Select.Option>
                        ))
                    }
                </Select>

            </FormItem>


        </div>
        <div className={itemClassName}><FormItem title={'آدرس کامل'} name={'Extra'} required={required}>
            <TextAreaBox rows={3}/></FormItem></div>
    </>);

}

export default GetAddress;
