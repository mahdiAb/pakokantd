import React, {useEffect, useState} from 'react';

import {DatePicker} from "antd";
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';

dayjs.extend(customParseFormat);


const DateBox = ({handleChangeDate, defValue, disable, className}) => {
    const [value, setValue] = useState(null)


    const dateFormat = 'YYYY/MM/DD';


    useEffect(() => {
        setValue(defValue)
    }, [defValue])

    const onChange = (e) => {
        console.log(e)
        setValue(e)
        handleChangeDate(e)
    }

    return (<>
            <DatePicker onChange={onChange} disabled={disable} className={className}
                        value={!!value ? dayjs(value, dateFormat) : null}
                        format={dateFormat}
            />

        </>


    );
};

export default DateBox;