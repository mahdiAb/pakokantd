import React from 'react';
import {Select} from "antd";

const SearchSelect = (props) => {

    const onChange = (value) => {
     };
    const onSearch = (value) => {
     };

    return (


        <Select  {...props} style={{width: "100%"}}   className={'defInput'}
                 showSearch
                 placeholder="Select a person"
                 optionFilterProp="children"
                 onChange={onChange}
                 onSearch={onSearch}
                 filterOption={(input, option) =>
                     (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                 }
                 options={props.options}
        />


    );
};
export default SearchSelect;

