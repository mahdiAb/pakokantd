import React from 'react';
import {Form} from "antd";

const FormItem = ({
                      valuePropName,
                      title = '',
                      type = '',
                      hasFeedback = false,
                      name,
                      hidden,
                      children,
                      className = 'input_wrap',
                      pattern,
                      defaultValue = null,
                      required,
                      typeMessage = `لطفا  ${title}  را درست وارد کنید!`,
                      patternMessage = `لطفا  ${title}  را درست وارد کنید!`,
                      requiredMessage = `لطفا  ${title}  را  وارد کنید!`,
                  }) => {

    const toEnDigit = (e) => {
        return e.target.value.replace(/[\u0660-\u0669\u06f0-\u06f9]/g, function (a) {
            return a.charCodeAt(0) & 0xf
        })

    }

    return (<Form.Item
            hasFeedback={hasFeedback}
            hidden={hidden}
            defaultValue={defaultValue}
            valuePropName={valuePropName}
            label={title}
            name={name}
            className={className}
            getValueFromEvent={typeof valuePropName == undefined && toEnDigit}
            // getValueFromEvent={(e)=>console.log(e)}
            rules={[{type: type, message: typeMessage}, {
                pattern: pattern, message: patternMessage
            }, {required: required, message: requiredMessage}]}
        >
            {children}


        </Form.Item>
    );
};

export default FormItem;