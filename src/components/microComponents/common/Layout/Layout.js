import React from 'react';
import {Dropdown} from "antd";
import ExitUser from "../../../pages/Business/ExitUser";
import {HiOutlineUser} from "@react-icons/all-files/hi/HiOutlineUser";
import {AiOutlineUser} from "@react-icons/all-files/ai/AiOutlineUser";

const Layout = ({children}) => {

    const items = [
        {
            label: <ExitUser/>,
            key: '0',
        },

    ];


    return (
        <div className={' pt-20'}>
            <header className='w-full top-0 fixed p-4 shadow-md z-10 flex justify-between items-center z-100 bg-themBlue'>


                <div>
                    <img src="/logo.pngs" width={80} alt=""/>
                </div>


                <div>

                    <Dropdown overlayStyle={{width: '180px'}} arrow menu={{items}} trigger={['click']}>
                        <AiOutlineUser size={24} color={'white'}/>
                    </Dropdown>
                </div>
            </header>

            <div className="  px-2 md:px-7 max-w-7xl m-auto  ">
                {children}

            </div>
        </div>
    );
};

export default Layout;