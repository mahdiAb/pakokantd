import React from 'react';

const KeyValue = ({keyText, value,hiddenColon, className, keyClass, valueClass}) => {
    return (

        <div className={`px-2 my-3 flex items-center  ${className}`}>
            <div className={`font-bold text-xs ml-2 flex items-center whitespace-nowrap ${keyClass}`}>{keyText   }<span> {!hiddenColon && ':'}</span>    </div>
            <div className={`text-sm ml-1 text-black-800  ${valueClass}`}>{value}</div>
        </div>
    );
};

export default KeyValue;