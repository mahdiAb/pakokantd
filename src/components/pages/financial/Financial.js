import React from 'react';
import {connect} from "react-redux";
import LayoutServices from "../../layout/LayoutServices";
import {
    handleFinancialActiveKey,
    handleFinancialNewTabIndex,
    handleFinancialTabList
} from "../../redux/reducer/Reducer";
import Wallet from "./tabs/wallet/Wallet";
import {IoWalletOutline} from "@react-icons/all-files/io5/IoWalletOutline";


let items = [
    {
        key: 's1',
        label: 'کیف پول',
        icon: <IoWalletOutline size={20}/>,
        reopen: false,
        id: 'Wallet',
        children: <Wallet/>,
    },


]


const Financial = ({
                       handleFinancialTabList, handleFinancialActiveKey, handleFinancialNewTabIndex,
                       tabListState, activeKey, newTabIndex,
                   }) => {


    const add = (data) => {
        let result = tabListState.findIndex(item => item.id == data.id);

        const handleAdd = () => {
            handleFinancialNewTabIndex()
            const newPanes = [...tabListState];
            newPanes.push({
                label: data.label, children: data.children, id: data.id, reopen: data.reopen, key: newTabIndex,
            });
            handleFinancialTabList(newPanes)
            handleFinancialActiveKey(newTabIndex);
        }
        if (result > -1) {
            if (data.reopen) {
                handleAdd()
            } else {

                handleFinancialNewTabIndex()
                const newPanes = [...tabListState];
                newPanes[result].key = newTabIndex
                handleFinancialTabList(newPanes)
                handleFinancialActiveKey(newTabIndex);


            }
        } else {
            handleAdd()
        }


    };

    const remove = (item) => {
        const newTabListState = [...tabListState];
        const index = newTabListState.indexOf(item);

        newTabListState.splice(index, 1);


        if (index >= newTabListState.length && index > 0) {
            handleFinancialActiveKey(newTabListState[index - 1]);
        }

        handleFinancialTabList(newTabListState)


    };


    const handleFinancialActive = (x) => {
        handleFinancialActiveKey(x)
    }
    return (


        <LayoutServices tabs={tabListState} items={items}
                        activeKey={activeKey}
                        handleNewTabIndex={handleFinancialNewTabIndex}
                        handleCloseTab={remove}
                        handleAddTab={add}
                        handleActiveKey={handleFinancialActive}
                        handleTabList={handleFinancialTabList}
        />

    );
};


function mapStateToProps(state) {
    return {
        tabListState: state.financialTabList,
        newTabIndex: state.financialNewTabIndex,
        activeKey: state.financialActiveKey,
        newTab: state.financialNewTab,
    }
}


export default connect(mapStateToProps, {
    handleFinancialTabList,
    handleFinancialActiveKey,
    handleFinancialNewTabIndex
})(Financial);



