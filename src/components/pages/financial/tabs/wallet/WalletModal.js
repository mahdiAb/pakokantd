import {Button, Form, Input, Modal} from 'antd';
import React, {useState} from 'react';
import PaymentBox from "../Payment";
import {useNavigate, useParams} from "react-router-dom";
import BusinessRequest from "../../../../helper/request/BusinessRequest";
import {ShowNotification} from "../../../../helper/ShowNotification";
import {GreaterThanZero} from "../../../../helper/validation/Validation";

const WalletModal = ({url, addonAfter, name}) => {

    const [open, setOpen] = useState(false);
    const [paymentData, setPaymentData] = useState();
    const [getLoading, setGetLoading] = useState(false);
    const navigate = useNavigate();
    let {id} = useParams();
    const [form] = Form.useForm();

    const showModal = () => {
        setOpen(true);
    };

    const handleOk = (values, id) => {


    };
    const handleCancel = () => {
        setOpen(false);
    };

    const onFinishFailed = (values) => {
        console.log(values)
    }
    const getPaymentTypes = async (values) => {


        setGetLoading(true)
        await BusinessRequest.init(url, function (res) {
            setGetLoading(false)
            if (res.data.messageCode === 0) {
                showModal()
                setPaymentData(res.data)
            } else {
                ShowNotification(res.data.message, false)

            }


        }, function (error) {
            setGetLoading(false)
        }).setBID(id).setMainData(values).setRouter(navigate).callRequest()


    }

    return (<>


        <Form
            form={form}
            name="basic"
            layout="vertical"
            onFinish={getPaymentTypes}
            onFinishFailed={onFinishFailed}
            autoComplete="0n"
        >
            <div className="flex items-start justify-end">
                <Form.Item className={'input_wrap mx-4 mb-0 mt-1'} name={'amount'}
                           rules={[{
                               required: true, message: `لطفا ${name} را وارد کنید!`
                           }, {pattern: GreaterThanZero, message: `لطفا ${name} را درست وارد کنید!`}]}>
                    <Input type={'number'} placeholder={`${name} مورد نظر برای افزایش`} addonAfter={addonAfter}/>
                </Form.Item>
                <Form.Item className={'mb-0'}>
                    <Button loading={getLoading} htmlType="submit" className='defBtn submitBtn  w-44 '>افزایش
                        موجودی</Button>
                </Form.Item></div>
        </Form>


        <Modal
            width={800}
            open={open}
            title={'افزایش موجودی'}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={null}>
            <PaymentBox isModal url={url} value={form.getFieldValue('amount')} paymentData={paymentData}
                        paymentValue={paymentData?.paymentAmount}/>

        </Modal>
    </>);
};
export default WalletModal;