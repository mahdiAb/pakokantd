import React, {useCallback, useEffect, useState} from 'react';


import {useNavigate, useParams} from "react-router-dom";
import KeyValue from "../../../../microComponents/common/keyValue/KeyValue";
import WalletModal from "./WalletModal";
import BusinessRequest from "../../../../helper/request/BusinessRequest";
import {
    ChargeCheckPaymentTypesInvoiceWallet,
    ChargeCheckPaymentTypesMoneyWallet,
    RemainingInvoiceWallet,
    RemainingMoneyWallet,
} from "../../../../helper/apiList/apiListBusiness";
import {ConvertToPrice} from "../../../../helper/ConvertToPrice";
import {Skeleton} from "antd";


function Wallet() {
    const [loadingMoney, setLoadingMoney] = useState(false);
    const [loadingInvoice, setLoadingInvoice] = useState(false);
    const [moneyWallet, setMoneyWallet] = useState(0);
    const [invoiceWallet, setInvoiceWallet] = useState(0);

    const [visibilityState, setVisibilityState] = useState(true);

    const navigate = useNavigate();
    let {id} = useParams();
    const handleVisibilityChange = useCallback(() => {
        setVisibilityState(document.visibilityState === 'visible');
    }, []);

    useEffect(() => {
        document.addEventListener("visibilitychange", handleVisibilityChange)
        return () => {
            document.removeEventListener("visibilitychange", handleVisibilityChange)
        }
    }, []);


    useEffect(() => {
        if (visibilityState) {
            setLoadingMoney(true)

            BusinessRequest.init(RemainingMoneyWallet, function (res) {

                setMoneyWallet(res.data)


                setLoadingMoney(false)

            }, function (error) {
                setLoadingMoney(false)
            }).setBID(id).setRouter(navigate).callRequest()
        }
    }, [visibilityState])
    useEffect(() => {
        if (visibilityState) {


            setLoadingInvoice(true)

            BusinessRequest.init(RemainingInvoiceWallet, function (res) {
                setInvoiceWallet(res.data)


                setLoadingInvoice(false)

            }, function (error) {
                setLoadingInvoice(false)
            }).setBID(id).setRouter(navigate).callRequest()
        }
    }, [visibilityState])




    return <>

        <div className="">

            <div className="defBox">
                <div
                    className='lg:flex mb-3   items-end  xl:items-start justify-between border-2 border-black-200 p-2 lg:p-4   rounded-2xl '>
                    <Skeleton loading={loadingMoney} style={{width: '100%'}} paragraph={{rows: 3}} active title={false}
                              rootClassName={'loadingJustifyBetween'}>
                        <div className="flex lg:block xl:flex justify-between flex-1 mt-2">

                            <p className={'mb-0 font-bold text-lg'}>کیف پول </p>
                            <KeyValue className={'xl:!my-0 !pr-0 xl:px-2 mt-3  '}
                                      value={`${ConvertToPrice(moneyWallet)} تومان`}
                                      keyText={'موجودی'}/>


                        </div>
                        <WalletModal url={ChargeCheckPaymentTypesMoneyWallet} name={'مبلغ'}
                                     addonAfter={'تومان'}/>
                    </Skeleton>
                </div>


                <div
                    className='lg:flex mb-3   items-end  xl:items-start justify-between border-2 border-black-200 p-2 lg:p-4   rounded-2xl '>
                    <Skeleton loading={loadingInvoice} style={{width: '100%'}} paragraph={{rows: 3}} active
                              title={false}
                              rootClassName={'loadingJustifyBetween'}>
                        <div className="flex lg:block xl:flex justify-between flex-1 mt-2">
                            <p className="mb-0 font-bold text-lg"> صورت حساب</p>
                            <KeyValue className={'xl:!my-0 !pr-0 xl:px-2 mt-3  '} value={`${invoiceWallet} عدد فاکتور`}
                                      keyText={'موجودی'}/>
                        </div>
                        <WalletModal url={ChargeCheckPaymentTypesInvoiceWallet} name={'تعداد'}
                                     addonAfter={'عدد'}/>
                    </Skeleton></div>


            </div>

        </div>


    </>
}

export default Wallet;

