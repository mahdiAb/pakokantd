import React, {useState} from 'react';
import {Button, Form, Radio, Skeleton} from "antd";
import {useNavigate, useParams} from "react-router-dom";
import {Payment} from "../../../helper/apiList/apiListBusiness";
import BusinessRequest from "../../../helper/request/BusinessRequest";
import KeyValue from "../../../microComponents/common/keyValue/KeyValue";
import {ConvertToPrice} from "../../../helper/ConvertToPrice";

const PaymentBox = ({isModal, paymentValue, paymentData}) => {
    const [sendLoading, setSendLoading] = useState(false);
    const [loadingDelete, setLoadingDelete] = useState(false);
    const [bankID, setBankID] = useState()

    const navigate = useNavigate();
    const [getLoading, setGetLoading] = useState(false);
    const [form] = Form.useForm();
    let {id} = useParams();

    const handlePayment = (values) => {
        values.UseWallet = values?.UseWallet ? values.UseWallet : false
        values.paymentID = paymentData?.paymentID

        setSendLoading(true)
        BusinessRequest.init(Payment, function (data) {

            setSendLoading(false)
            window.open(data.data.dtoipgUrlResponse.fullURL);
        }, function (error) {

            setSendLoading(false)
        }).setBID(id).setRouter(navigate).setMainData(values).callRequest()


    }
    const onFinishFailed = (values) => {
        console.log(values)
    }
    return (
        <div>
            <div className={`flex-1 ml-3 ${!isModal && 'defBox'}`}>

                <Skeleton loading={getLoading} style={{width: '100%'}} paragraph={{rows: 5}} title={false}
                          rootClassName={'loadingJustifyBetween'}>
                    <Form className={'flex justify-between flex-col h-full'}
                          form={form}
                          name="basic"
                          layout="vertical"
                          onFinish={handlePayment}
                          onFinishFailed={onFinishFailed}
                          autoComplete="0n"
                          initialValues={{
                              bankID: paymentData?.ipgList[0]?.bankID,
                              UseWallet: true
                          }}
                    >

                        <div>    {


                            <Form.Item name={'bankID'}>
                                <Radio.Group
                                    value={bankID}
                                    onChange={(e) => setBankID(e.target.value)}
                                    // name={'InvoiceSettlementMethodID'}
                                    // value={invoiceSettlementMethodIDValue}
                                >
                                    {paymentData?.ipgList?.map((item, index) => (


                                        <Radio value={item?.bankID} className={'defBoxBorder'}>
                                            <div className=' flex justify-center items-center flex-col'>
                                                <img width={100} src={'/cropped-Asset-1@10x.png'}/>
                                                <p className={'my-4 text-center'}>{item?.name}</p>

                                            </div>
                                        </Radio>


                                    ))}
                                </Radio.Group></Form.Item>

                        }
                            {paymentData?.moneyWalletRemainingValue ?
                                <Form.Item name={'UseWallet'}>
                                    <Radio.Group>

                                        <Radio value={true}><p className={'my-4 text-center'}>
                                            {
                                                !paymentData.paymentWithMoneyWallet ?
                                                    'پرداخت از کیف پول و درگاه'
                                                    : ' پرداخت از کیف پول'
                                            }
                                        </p></Radio>


                                        <Radio value={false}>


                                            <p className={'my-4 text-center'}> پرداخت از درگاه</p>


                                        </Radio>


                                    </Radio.Group>


                                </Form.Item>


                                : null}
                        </div>
                        <KeyValue className={''} valueClass={'!text-base'} value={`${ConvertToPrice(paymentValue)} ریال`}
                                  keyText={' مبلغ قابل پرداخت     '}/>
                        <div className='flex justify-end  mb-0 items-center mt-5'>

                            <Button loading={sendLoading} className='defBtn submitBtn'
                                    htmlType="submit">
                                پرداخت
                            </Button>
                        </div>
                    </Form>
                </Skeleton>


            </div>
        </div>
    );
};

export default PaymentBox;