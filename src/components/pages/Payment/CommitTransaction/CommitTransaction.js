import React, {useEffect, useState} from 'react';


import {NavLink, useNavigate, useParams} from "react-router-dom";
import {GetTransactionInfo} from "../../../helper/apiList/apiListBusiness";
import Request from "../../../helper/request/Request";
import {DateTimeString} from "../../../helper/DateSolarString";
import {Skeleton} from "antd";


const CommitTransaction = (props) => {

    const [CommitTransactionInfo, setCommitTransactionInfo] = useState(props.GetCommitTransaction)
    const [loading, setLoading] = useState(true);
    const [status, setStatus] = useState(false);
    console.log(loading)
    const navigate = useNavigate();


    let {id} = useParams();

    const handlePrintPage = () => {
        window.print();
    }

    useEffect(() => {

        let data = {
            TransactionID: id
        }

        Request.init(GetTransactionInfo, function (data) {
            setCommitTransactionInfo(data.data)
            setLoading(false)
            setStatus(data.data.statusID === 4)
        }, function (error) {
            setLoading(false)
        }).setMainData(data).setRouter(navigate).callRequest()


    }, [])
    return (


        <div className={"CommitTransaction"}>
            <div className={`ReceiptBody ${status ? 'Success' : 'Failed'}`}>
                <div
                    className={`CircleBg ${loading ? 'bg-black-400' : status ? 'Success' : 'Failed'}`}></div>

                {
                    loading ? <div className="Head">
                        {/*<img src={Success} alt="ReceiptImage"/>*/}
                        <div>لطفا منتظر بمانید...</div>
                    </div> : status ?
                        (
                            <div className="Head">
                                {/*<img src={Success} alt="ReceiptImage"/>*/}
                                <div> تراکنش موفق</div>
                            </div>
                        )
                        :
                        (
                            <div className="Head">
                                {/*<img src={Failed} alt="ReceiptImage"/>*/}
                                <div> تراکنش ناموفق</div>
                            </div>

                        )
                }

                <div className={"m-auto mt-10"} style={{maxWidth: '100px'}}>
                    <img width={'100%'} src="/logo1.png" alt="logo pakok"/>
                </div>

                {
                    status ?
                        (
                            <div className="Informations">
                                <Skeleton loading={loading} style={{width: '100%'}} paragraph={{rows: 5}}
                                          title={false} active
                                          rootClassName={'loadingJustifyBetween'}>
                                    <div
                                        className={`Item ${status ? 'Success' : 'Failed'}`}>
                                        <div> مبلغ</div>
                                        <div>{CommitTransactionInfo?.amount} <span>تومان</span></div>
                                    </div>

                                    <div
                                        className={`Item ${status ? 'Success' : 'Failed'}`}>
                                        <div> تاریخ</div>
                                        <div>{DateTimeString(CommitTransactionInfo?.transactionDate)}</div>
                                    </div>

                                    <div
                                        className={`Item ${status ? 'Success' : 'Failed'}`}>
                                        <div id="AgentCode"> شناسه کاربر</div>
                                        <div>{CommitTransactionInfo?.AgentCode}</div>
                                    </div>

                                    <div
                                        className={`Item ${status ? 'Success' : 'Failed'}`}>
                                        <div> شماره رسید</div>
                                        <div>{CommitTransactionInfo?.ResNum}</div>
                                    </div>

                                    <div
                                        className={`Item ${status ? 'Success' : 'Failed'}`}>
                                        <div> کد رهگیری</div>
                                        <div>{CommitTransactionInfo?.RefNum}</div>
                                    </div>

                                    {
                                        CommitTransactionInfo && <div
                                            className={`Item ${status ? 'Success' : 'Failed'}`}>
                                            <div> پیغام</div>
                                            <div>{CommitTransactionInfo?.status}</div>
                                        </div>}

                                </Skeleton>
                            </div>
                        )
                        :
                        (CommitTransactionInfo &&
                            <div className="Informations">
                                <div
                                    className={`Item ${status ? 'Success' : 'Failed'}`}>
                                    <div> پیغام</div>
                                    <div>{CommitTransactionInfo?.status}</div>
                                </div>
                            </div>

                        )
                }

                {
                    CommitTransactionInfo?.DTOSimCard ? (
                        <div className="ReportTableWrap">


                            <p> شماره سیم کارت: {CommitTransactionInfo?.DTOSimCard[0]?.SimCardNumber}</p>
                            <p> کد فعال سازی: {CommitTransactionInfo?.DTOSimCard[0]?.ActiveCode} </p>


                        </div>
                    ) : null
                }

                {
                    CommitTransactionInfo?.AppID === "2" ?
                        (
                            <div className="WrapButton">
                                <NavLink to={'/'}><a>
                                    <div
                                        className={`CAT ${status ? 'Success' : 'Failed'}`}>
                                        بازگشت به اپلیکشن
                                    </div>
                                </a></NavLink>
                            </div>
                        ) :
                        (
                            <div className="WrapButton">

                                <NavLink to={'/'}><a>
                                    <div
                                        className={`CAT ${loading ? 'bg-black-400' : status ? 'Success' : 'Failed'}`}>
                                        بازگشت به سایت
                                    </div>
                                </a></NavLink>

                                <div onClick={() => handlePrintPage()}
                                     className={`CAT ${loading ? 'bg-black-400' : status ? 'Success' : 'Failed'}`}>
                                    چاپ
                                </div>

                            </div>
                        )
                }

            </div>
        </div>


    );

}

export default CommitTransaction;
