import {tabsData} from '../../data/headerJson.js';
import React, {useState} from 'react';
import {connect} from "react-redux";
import {handleCollapseSidebar} from "../../redux/reducer/Reducer";
import {Dropdown} from "antd";
import ExitUser from "../Business/ExitUser";
import {NavLink} from "react-router-dom";
import ExitBusiness from "../Business/ExitBusiness";
import {RiMenuUnfoldLine} from "@react-icons/all-files/ri/RiMenuUnfoldLine";
import {AiOutlineUser} from "@react-icons/all-files/ai/AiOutlineUser";

const Main = ({collapseSidebar, handleCollapseSidebar,}) => {


    let [selectedIndex, setSelectedIndex] = useState(0)
    const items = [{
        label: <NavLink target="_blank" to={`/`}>لیست کسب و کارها</NavLink>, key: '0',
    },

        {
            label: <ExitBusiness/>, key: '2',
        }, {
            label: <ExitUser/>, key: '1',
        },


    ];
    const handleSidebar = () => {

        if (collapseSidebar == 0 | collapseSidebar == 1) {
            handleCollapseSidebar(collapseSidebar + 1)
        } else if (collapseSidebar == 2) {
            handleCollapseSidebar(0)
        }

    }
    return <>
        <header className={`serviceHeader  transition-all duration-300   `}>
            <div className={`flex px-1 sm:px-3 items-center justify-between  shadow-md p-3 bg-themBlue text-white`}>
                <div className={'flex items-center flex-1'}>


                    <div className={'flex items-center'}>
                        <button
                            onClick={() => handleSidebar()}
                            className={'sidebarBtnToggle    transition-all duration-300 '}>
                            <RiMenuUnfoldLine
                                size={20}/></button>


                        <img className='mr-2' width={80} src="/logo.png" alt="logo"/>

                    </div>


                    <ul className="tabs flex mb-0 mr-4 sm:mr-10 ">
                        {tabsData.map((item, index) => (
                            <li className={`items-center relative  cursor-pointer flex tab ml-4 text-sm  `}
                                onClick={() => setSelectedIndex(index)}>
                                <span className={'hidden sm:block'}>{item.icon}</span>
                                <span className='mr-1 whitespace-nowrap'>{item.text}</span>{selectedIndex === index &&
                                <div className='headerActive'></div>}
                            </li>


                        ))}


                    </ul>
                </div>


                <Dropdown overlayStyle={{width: '170px'}} arrow menu={{items}} trigger={['click']}>
                    <AiOutlineUser size={24}/>
                </Dropdown>

            </div>
        </header>
        <div className="tab-panes servicesTabs  ">

            {tabsData.map((item, index) => (

                <div className={`tab-pane ${selectedIndex === index && 'active'}`}>{item.content}</div>


            ))}
        </div>

    </>


}


function mapStateToProps(state) {
    return {
        collapseSidebar: state.collapseSidebar,
        hiddenSidebar: state.hiddenSidebar,

    }
}


export default connect(mapStateToProps, {
    handleCollapseSidebar,

})(Main);

