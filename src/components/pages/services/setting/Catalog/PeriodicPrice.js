import React from 'react';
import DateBox from "../../../../microComponents/common/uikits/DateBox";
import FormItem from "../../../../microComponents/common/uikits/FormItem";
import TextInput from "../../../../microComponents/common/uikits/TextInput";

const PeriodicPrice = () => {
    return (
        <div>


            <FormItem title='تاریخ شروع' name={'Name'} required>
                <DateBox/>
            </FormItem>
            <FormItem title='تاریخ اتمام' name={'Name'} required>
                <DateBox/>
            </FormItem>
            <FormItem title={'قیمت'} name={'Name'} required>
                <TextInput/>
            </FormItem>
        </div>
    );
};

export default PeriodicPrice;