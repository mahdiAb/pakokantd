import {Button, Form, Modal} from 'antd';
import React, {useState} from 'react';
import {AiFillEdit} from "@react-icons/all-files/ai/AiFillEdit";
import FormItem from "../../../../../microComponents/common/uikits/FormItem";
import TextInput from "../../../../../microComponents/common/uikits/TextInput";
import {useNavigate, useParams} from "react-router-dom";
import BusinessRequest from "../../../../../helper/request/BusinessRequest";
import {ShowNotification} from "../../../../../helper/ShowNotification";
import TextAreaBox from "../../../../../microComponents/common/uikits/TextAreaBox";
import {EditProductCategory} from "../../../../../helper/apiList/apiListBusiness";

const EditCategory = ({data, getData}) => {


    const [open, setOpen] = useState(false);


    const [form] = Form.useForm();
    const [loading, setLoading] = useState(false);
    let initialValues = data.data
    const navigate = useNavigate();


    let {id} = useParams();
    const handleEditCatalog = (values) => {


        setLoading(true)

        values.id = initialValues.id
        BusinessRequest.init(EditProductCategory, function (data) {


            setLoading(false)
            handleCancel()
            // if (data.data.messageCode === 0) {

            getData()
            ShowNotification(' دسته بندی با موفقیت ویرایش شد', true)
            // } else {
            //     ShowNotification(data.data.message, false)
            //     setLoading(false)
            // }
        }, function (error) {
            setLoading(false)
            handleCancel()

        }).setBID(id).setRouter(navigate).setMainData(values).callRequest()
    }


    const onFinishFailed = (errorInfo) => {

    };


    const showModal = () => {
        setOpen(true);
    };

    const handleOk = () => {
form.submit()
    };
    const handleCancel = () => {
        setOpen(false);
    };


    return (<>


        <Button
            className='  iconBtn yellow' onClick={showModal}><AiFillEdit size={18}/></Button>


        <Modal
            width={800}
            open={open}
            title={'ویرایش دسته بندی'}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[<Button key="submit" className='defBtn submitBtn' type="primary" onClick={handleOk}>
                ثبت
            </Button>]}>

            <Form

                form={form}
                name="basic"
                layout="vertical"
                initialValues={initialValues}
                onFinish={handleEditCatalog}
                onFinishFailed={onFinishFailed}
                autoComplete="0n"
            >
                <div
                    className="grid gap-4   xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 gap-6 grid-flow-row">


                    <FormItem title='نام  دسته بندی' name={'name'} required>
                        <TextInput/>
                    </FormItem>

                    <FormItem title='کد  دسته بندی' name={'code'}>
                        <TextInput/>
                    </FormItem>
                    <FormItem title='توضیحات  دسته بندی' name={'description'}>
                        <TextAreaBox/>
                    </FormItem>

                    {/*<PeriodicPrice/>*/}
                </div>
                {/*<div className="flex justify-end">*/}


                {/*    <Form.Item className='mb-0'>*/}
                {/*        <Button loading={loading} className='' htmlType="submit">*/}
                {/*            ثبت*/}
                {/*        </Button>*/}
                {/*    </Form.Item>*/}
                {/*</div>*/}


            </Form>


        </Modal>
    </>);
};
export default EditCategory;