import React, {useEffect, useMemo, useRef, useState} from 'react';
import {AgGridReact} from 'ag-grid-react';
import {useNavigate, useParams} from "react-router-dom";
import BusinessRequest from "../../../../../helper/request/BusinessRequest";

import EditCategory from "./EditCategory";
import {GetAllProductCategory} from "../../../../../helper/apiList/apiListBusiness";
import DeleteCategory from "./DeleteCategory";
import Paginate from "../../../../../helper/Paginate";
import {AG_GRID_LOCALE_FA} from "../../../../../helper/locale.fa";
import {Button} from "antd";
import CreateCatalog from "../CreateCatalog";
import CreateCategoryModal from "./CreateCategoryModal";


const CategoryList = () => {
    const [params, setParams] = useState({PageNumber: '1', CountInPage: '10'})
    const gridRef = useRef(null);
    const [totalRecord, setTotalRecord] = useState(0)
    let {id} = useParams();
    const [loading, setLoading] = useState(false);
    const [data, setData] = useState([]);
    const navigate = useNavigate();
    const [reCall, setReCall] = useState(false)
    const [didMount, setDidMount] = useState(false);

    const BtnCellRenderer = (data) => {


        return (
            <div className='flex items-center h-full mt-0.5'>
                <DeleteCategory data={data} getData={() => setReCall(prev => (!prev))}/>
                 <EditCategory data={data} getData={() => setReCall(prev => (!prev))}/>

            </div>
        )

    }
    const [columnDefs, setColumnDefs] = useState([

        {
            headerName: "",
            valueGetter: "node.rowIndex + 1",
            width: 60, editable: false,
            maxWidth: 60,
        },

        {
            headerName: 'نام  دسته بندی',
            field: 'name',


        },
        {
            headerName: 'کد  دسته بندی',
            field: 'code',


        },

        {
            headerName: 'توضیحات  دسته بندی',
            field: 'description',


        },

        {
            headerName: 'ویرایش',
            field: 'edit',
            pinned: 'left',
            width: 100,
            minWidth: 100,

            cellRenderer: BtnCellRenderer,
            // cellRendererParams: {
            //     clicked: function(field) {
            //         alert(`${field} was clicked`);
            //     },
            // },

        },
    ]);

    const defaultColDef = useMemo(() => {
        return {
            flex: 1,
            sortable: true,
            filter: true,
            minWidth: 150,
        };
    }, []);



    const localeText = useMemo(() => {
        return AG_GRID_LOCALE_FA;
    }, []);

    const onGridReady = () => {
        gridRef.current.api.showLoadingOverlay();
        BusinessRequest.init(GetAllProductCategory, function (data) {
            setData(data.data.list)
            gridRef.current.api.hideOverlay();
            setTotalRecord(data.data.totalRecord)
            setLoading(false)
        }, function (error) {
            setLoading(false)
            gridRef.current.api.hideOverlay();
        }).setBID(id).setMainData(params).setRouter(navigate).callRequest()
    }


    const pageNumberValue = (value) => {
        setParams(prev => ({...prev, PageNumber: String(value)}));
    }


    useEffect(() => {


        if (didMount) {
            onGridReady()

        } else {
            setDidMount(true)
        }
    }, [params, reCall])


    return (
        <div className='h-full'>
            <div className="flex justify-end mb-2"><CreateCategoryModal getData={() => setReCall(prev => (!prev))}/>
            </div>
            <div className="h-full ag-theme-alpine defBox">
                <AgGridReact Scrolling
                             rowData={data}
                             enableRtl
                             columnDefs={columnDefs}
                             defaultColDef={defaultColDef}
                             ref={gridRef}
                             onGridReady={onGridReady}
                             localeText={localeText}
                >

                </AgGridReact>
                {
                    totalRecord > 10 &&         <Paginate totalRecord={totalRecord}

                                                          pageNumberValue={pageNumberValue}
                                                          pageNumber={params.PageNumber}/>
                }

            </div>
        </div>
    );
};


export default CategoryList;