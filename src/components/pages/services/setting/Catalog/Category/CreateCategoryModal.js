import React, {useState} from 'react';
import TextInput from "../../../../../microComponents/common/uikits/TextInput";
import {useNavigate, useParams} from "react-router-dom";
import {Button, Form, Modal} from "antd";
import FormItem from "../../../../../microComponents/common/uikits/FormItem";
import TextAreaBox from "../../../../../microComponents/common/uikits/TextAreaBox";
import {CreateProductCategory} from "../../../../../helper/apiList/apiListBusiness";
import BusinessRequest from "../../../../../helper/request/BusinessRequest";
import {ShowNotification} from "../../../../../helper/ShowNotification";


const CreateCategoryModal = ({getData,getCategory}) => {

    const [open, setOpen] = useState(false);

    const showModal = () => {
        setOpen(true);
        // openSelect()
    };

    const handleCancel = () => {
        setOpen(false);

    };

    const [form] = Form.useForm();
    const [loading, setLoading] = useState(false);

    const navigate = useNavigate();


    let {id} = useParams();
    const handleCreateCategory = async (values) => {

        setLoading(true)


        await BusinessRequest.init(CreateProductCategory, function (data) {


            setLoading(false)
            if (data.data.messageCode === 0) {
                form.resetFields()
                handleCancel()
                getData()
                getCategory(data.data.valueText )
                ShowNotification(' دسته بندی با موفقیت ثبت شد', true)
            } else {
                ShowNotification(data.data.message, false)

            }

        }, function (error) {
            setLoading(false)

        }).setBID(id).setMainData(values).setRouter(navigate).callRequest()


    }


    const onFinishFailed = (errorInfo) => {

    };
    const handleOk = () => {
        form.submit()
    };
    return (<>   <Button
        className='defBtn submitBtn  ' onClick={showModal}>ایجاد دسته بندی</Button>


        <Modal
            width={800}
            open={open}
            title={'ایجاد دسته بندی'}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[<div className='flex justify-end items-center'>
                <Button key="cancel" className='defBtn borderBtn  ' type="primary" onClick={handleCancel}>
                    انصراف
                </Button>
                <Button key="submit " htmlType="submit" loading={loading} className='defBtn submitBtn ' type="primary" onClick={handleOk}>
                    ثبت
                </Button>

            </div>]}>


            <Form
                form={form}
                name="basic"
                layout="vertical"
                onFinish={handleCreateCategory}
                onFinishFailed={onFinishFailed}
                autoComplete="0n"
            >
                <div
                    className="grid gap-4   xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 gap-6 grid-flow-row">


                    <FormItem title='نام  دسته بندی' name={'name'} required>
                        <TextInput/>
                    </FormItem>

                    <FormItem title='کد  دسته بندی' name={'code'}>
                        <TextInput/>
                    </FormItem>
                    <FormItem title='توضیحات  دسته بندی' name={'description'}>
                        <TextAreaBox/>
                    </FormItem>
                </div>


            </Form>


        </Modal></>);

};


export default CreateCategoryModal;