import React, {useState} from 'react';
import {Button, Modal} from 'antd';
import {useNavigate, useParams} from "react-router-dom";
import BusinessRequest from "../../../../../helper/request/BusinessRequest";
import {ShowNotification} from "../../../../../helper/ShowNotification";
import {DeleteProductCategory} from "../../../../../helper/apiList/apiListBusiness";
import {FiTrash2} from "@react-icons/all-files/fi/FiTrash2";


const DeleteCategory = ({data, getData}) => {

    const [open, setOpen] = useState(false);


    const [loading, setLoading] = useState(false);

    const navigate = useNavigate();


    let {id} = useParams();


    const showModal = () => {
        setOpen(true);
    };

    const handleOk = () => {
        setLoading(true)
        let values = {
            ID: data.data.id
        }

        BusinessRequest.init(DeleteProductCategory, function (data) {


            setLoading(false)
            handleCancel()
            if (data.data.messageCode === 0) {

            getData()
            ShowNotification(' دسته بندی با موفقیت حذف شد', true)
            } else {
                ShowNotification(data.data.message, false)
                setLoading(false)
            }
        }, function (error) {
            setLoading(false)
            handleCancel()

        }).setBID(id).setRouter(navigate).setMainData(values).callRequest()
    };
    const handleCancel = () => {
        setOpen(false);
    };


    return (<>


        <Button
            className='  iconBtn red' onClick={showModal}><FiTrash2 size={18}/></Button>


        <Modal
            width={800}
            open={open}
            title={'حذف دسته بندی'}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[<div className='flex justify-end items-center'>
                <Button key="cancel" className='defBtn borderBtn  ' type="primary" onClick={handleCancel}>
                    انصراف
                </Button>
                <Button key="submit " loading={loading} className='defBtn submitBtn ' type="primary" onClick={handleOk}>
                    ثبت
                </Button>

            </div>]}>

            <p className='font-bold text-sm'>آیا از حذف این دسته بندی اطمینان دارید؟ </p>

        </Modal>
    </>);
};
export default DeleteCategory;