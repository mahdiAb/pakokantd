import React, {useState} from 'react';
import {Button, Modal} from 'antd';
import CreateCatalog from "./CreateCatalog";


const CreateCatalogModal = ({disabled, getValueSearch}) => {

    const [open, setOpen] = useState(false);


    const showModal = () => {
        setOpen(true);
    };

    const handleCancel = () => {
        setOpen(false);
    };


    const handleOk = (values, id) => {
        handleCancel()
        console.log(values)
        getValueSearch(values,id)
    };

    return (<>


        <Button className='addBtn text-blue' onClick={showModal} disabled={disabled}
                type="primary">+</Button>
        <Modal
            width={800}
            open={open}
            title={'ایجاد کالا'}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={false}>

            <CreateCatalog isModal handleCancel={handleCancel} getData={handleOk}/>

        </Modal>
    </>);
};
export default CreateCatalogModal;