import { Button, Modal } from "antd";
import React, { useState } from "react";
import CreateCatalog from "./CreateCatalog";

const CreatCatalogModal = ({ getValueSearch, disabled, getData }) => {
  const [open, setOpen] = useState(false);

  const showModal = () => {
    setOpen(true);
  };

  const handleOk = (values, id) => {
    handleCancel();

    getValueSearch(values, id);
  };
  const handleCancel = () => {
    setOpen(false);
  };

  return (
    <>
      <Button
        className="addBtn text-blue"
        onClick={showModal}
        disabled={disabled}
        type="primary"
      >
        +
      </Button>

      <Modal
        width={800}
        open={open}
        title={"ایجاد محصول"}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={null}
      >
        <CreateCatalog isModal handleCancel={handleCancel} getData={handleOk} />
      </Modal>
    </>
  );
};
export default CreatCatalogModal;
