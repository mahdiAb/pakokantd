import React, {useEffect, useMemo, useRef, useState} from 'react';
import {AgGridReact} from 'ag-grid-react';
import {useNavigate, useParams} from "react-router-dom";
import BusinessRequest from "../../../../helper/request/BusinessRequest";
import {GetAllCatalog} from "../../../../helper/apiList/apiListBusiness";
import EditCatalog from "./EditCatalog";
import DeleteCatalog from "./DeleteCatalog";
import Paginate from "../../../../helper/Paginate";
import {AG_GRID_LOCALE_FA} from "../../../../helper/locale.fa";
import {Button} from "antd";
import CreateCatalog from "./CreateCatalog";
import {connect} from "react-redux";
import {
    handleSettingActiveKey,
    handleSettingNewTabIndex,
    handleSettingTabList
} from "../../../../redux/reducer/Reducer";


const CatalogList = ({
                         handleSettingTabList, handleSettingActiveKey, handleSettingNewTabIndex,
                         newTab, tabListState, newTabIndex,
                     }) => {
    const [params, setParams] = useState({PageNumber: '1', CountInPage: '10'})
    const [totalRecord, setTotalRecord] = useState(0)
    const [didMount, setDidMount] = useState(false);
    const [reCall, setReCall] = useState(false)
    const gridRef = useRef(null);
    const [addTab, setAddTab] = useState({})
    useEffect(() => {

        if (addTab.id) {
            add(addTab)
        }
    }, [addTab])
    const add = (data) => {

        handleSettingNewTabIndex()
        const newPanes = [...tabListState];
        newPanes.push({
            label: data.label, children: data.children, id: data.id, reopen: true, key: newTabIndex,
        });
        handleSettingTabList(newPanes)
        handleSettingActiveKey(newTabIndex);

    };
    const BtnCellRenderer = (x) => {


        return (


            <div className='flex items-center h-full mt-0.5'>
                <DeleteCatalog data={x} getData={() =>  setReCall(prev => (!prev))}/>
                <EditCatalog data={x} getData={() =>  setReCall(prev => (!prev))}/>
            </div>
        )

    }


    const priceRenderer = ({data}) => {

        return <span>{data.prices[0]?.priceValue}</span>
        // return <Select style={{width: '100px'}}
        //
        //
        //                defaultValue={data.prices[0]?.priceValue}
        //                fieldNames={{label: 'priceValue', value: 'id'}}
        //                options={data.prices}
        // />
    }


    const [columnDefs, setColumnDefs] = useState([

        {
            headerName: "",

            valueGetter: "node.rowIndex + 1",
            width: 60,    editable: false,
            maxWidth: 60,
        },
        {
            headerName: 'نام محصول',
            field: 'name',
            minWidth: 120,

        },

        {
            headerName: 'توضیحات محصول',
            field: 'description',
            minWidth: 145,

        },
        {
            headerName: 'کد محصول',
            field: 'code',
            minWidth: 120,

        },
        {headerName: 'سایر وجوه قانونی', field: 'legalRate',  minWidth: 130,},
        {headerName: 'سایر مالیات وعوارض', field: 'otherRate',  minWidth: 150,},
        {headerName: 'مالیات بر ارزش افزوده', field: 'taxRate',  minWidth: 165,},


        {
            headerName: 'قیمت محصول',
            field: 'price',minWidth: 125,
            cellRenderer: priceRenderer

        },

        {
            headerName: 'دسته بندی محصول',
            field: 'productCategoryObj.name',

        },

        {
            headerName: 'ویرایش',
            field: 'PersonCategoryID',

            cellRenderer: BtnCellRenderer,
            pinned: 'left',
            width: 100,
            minWidth: 100,


        },
    ]);
    const localeText = useMemo(() => {
        return AG_GRID_LOCALE_FA;
    }, []);

    const defaultColDef = useMemo(() => {
        return {
            flex: 1,
            sortable: true,
            filter: true,

            minWidth: 150,
        };
    }, []);

    let {id} = useParams();


    const [loading, setLoading] = useState(false);

    const [data, setData] = useState([]);


    const navigate = useNavigate();
    useEffect(() => {


        if (didMount) {
            onGridReady()

        } else {
            setDidMount(true)
        }
    }, [params, reCall])

    const onGridReady = async () => {




        gridRef.current.api.showLoadingOverlay();

        await BusinessRequest.init(GetAllCatalog, function (res) {
            setData(res.data.list)
            gridRef.current.api.hideOverlay();

            setTotalRecord(res.data.totalRecord)
            console.log(res.data)
            setLoading(false)
        }, function (error) {
            gridRef.current.api.hideOverlay();

            setLoading(false)
        }).setBID(id).setMainData(params).setRouter(navigate).callRequest()

    }

    const pageNumberValue = (value) => {
        setParams(prev => ({...prev, PageNumber: String(value)}));
    }
    return (
        <div className='h-full'>

            <div className="flex justify-end mb-2"><Button onClick={() => setAddTab({
                label: 'ایجاد محصول',
                id: 'CreateCatalog',
                children: <CreateCatalog/>,
            })} className='defBtn submitBtn'>ایجاد محصول</Button>
            </div>
            <div className="h-full ag-theme-alpine defBox">
                <AgGridReact Scrolling
                             rowData={data}
                             enableRtl
                             ref={gridRef} enableCellTextSelection={true}
                             columnDefs={columnDefs}
                             defaultColDef={defaultColDef}
                             onGridReady={onGridReady}
                             localeText={localeText}

                >

                </AgGridReact>
                {
                    totalRecord > 10 && <Paginate totalRecord={totalRecord}

                                                  pageNumberValue={pageNumberValue}
                                                  pageNumber={params.PageNumber}/>}
            </div>
        </div>
    );
};


function mapStateToProps(state) {
    return {
        tabListState: state.settingTabList,
        newTabIndex: state.settingNewTabIndex,
        activeKey: state.settingActiveKey,
        newTab: state.settingNewTab,
    }
}


export default connect(mapStateToProps, {
    handleSettingTabList,
    handleSettingActiveKey,
    handleSettingNewTabIndex
})(CatalogList);




