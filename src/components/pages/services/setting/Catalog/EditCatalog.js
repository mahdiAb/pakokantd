import {Button, Form, Input, Modal, Select} from 'antd';
import React, {useEffect, useState} from 'react';
import {AiFillEdit} from "@react-icons/all-files/ai/AiFillEdit";
import {useNavigate, useParams} from "react-router-dom";
import BusinessRequest from "../../../../helper/request/BusinessRequest";
import {GetAllProductCategory, UpdateCatalog} from "../../../../helper/apiList/apiListBusiness";
import {ShowNotification} from "../../../../helper/ShowNotification";
import {number13} from "../../../../helper/validation/Validation";

const EditCatalog = ({data, getData}) => {
    console.log(data)
    let initialValues = data.data
    initialValues.price = initialValues?.prices[0]?.priceValue
    const [form] = Form.useForm();
    form.setFieldsValue({
        someFormField: initialValues
    });



    const [productCategoryList, setProductCategoryList] = useState([]);
    const [productCategoryLoading, setProductCategoryLoading] = useState(true);
    const [open, setOpen] = useState(false);



    const [loading, setLoading] = useState(false);

    const navigate = useNavigate();


    useEffect(() => {
        if (open) {

            let data = {PageNumber: '1', CountInPage: '1000000'}

            BusinessRequest.init(GetAllProductCategory, function (data) {
                setProductCategoryList(data.data.list)

                setProductCategoryLoading(false)
            }, function (error) {
                setProductCategoryLoading(false)
            }).setBID(id).setMainData(data).setRouter(navigate).callRequest()

        }

    }, [open])
    let {id} = useParams();
    const handleEditCatalog = (values) => {

        values.prices = [{
            "PriceValue": values.price, "FromDate": "", "ToDate": ""
        }]
        setLoading(true)

        values.id = initialValues.id
        BusinessRequest.init(UpdateCatalog, function (data) {




            setLoading(false)
            handleCancel()
            // if (data.data.messageCode === 0) {

            getData()
            ShowNotification('محصول با موفقیت ویرایش شد', true)
            // } else {
            //     ShowNotification(data.data.message, false)
            //     setLoading(false)
            // }
        }, function (error) {
            setLoading(false)
            handleCancel()

        }).setBID(id).setRouter(navigate).setMainData(values).callRequest()
    }


    const onFinishFailed = (errorInfo) => {

    };


    const showModal = () => {
        setOpen(true);
    };

    const handleOk = () => {
        form.submit()
    };
    const handleCancel = () => {
        setOpen(false);
    };


    return (<>


        <Button className='iconBtn yellow' onClick={showModal}><AiFillEdit size={18}/></Button>


        <Modal
            width={800}
            open={open}
            title={'ویرایش کالا خدمات'}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[<Button className='defBtn borderBtn'   loading={loading} type="primary"
                             onClick={handleCancel}>
                انصراف
            </Button>,<Button className='defBtn submitBtn' key="submit" loading={loading} type="primary"
                             onClick={handleOk}>
                ثبت
            </Button>]}>

            <Form

                form={form}
                name="basic"
                layout="vertical"
                initialValues={initialValues}
                onFinish={handleEditCatalog}
                onFinishFailed={onFinishFailed}
                autoComplete="0n"
            >
                <div
                    className="grid gap-4   xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 gap-6 grid-flow-row">


                    <Form.Item className={'input_wrap'} label='نام محصول' name={'name'}
                               rules={[{required: true, message: 'لطفا نام محصول را وارد کنید!'}]}>
                        <Input className={'defInput'}/>
                    </Form.Item>
                    <Form.Item className={'input_wrap'} label="شناسه محصول" name={"productTaxID"} rules={[{
                        pattern: number13, message: `لطفا  شناسه محصول  را درست وارد کنید!`
                    }]}>
                        <Input className={'defInput'}/>
                    </Form.Item>
                    <Form.Item className={'input_wrap'} label='کد محصول' name={'code'}>
                        <Input className={'defInput'}/>
                    </Form.Item>
                    <Form.Item className={'input_wrap'} label='قیمت محصول' name={'price'}>
                        <Input className={'defInput'}/>
                    </Form.Item>
                    <Form.Item className={'input_wrap'} label='سایر وجوه قانونی' name={'legalRate'}>
                        <Input className={'defInput'}/>
                    </Form.Item>
                    <Form.Item className={'input_wrap'} label='سایر مالیات وعوارض' name={'otherRate'}>
                        <Input className={'defInput'}/>
                    </Form.Item>
                    <Form.Item className={'input_wrap'} label='مالیات بر ارزش افزوده' name={'taxRate'}>
                        <Input className={'defInput'}/>
                    </Form.Item>
                    <Form.Item className={'input_wrap'} label='توضیحات محصول' name={'description'}>
                        <Input.TextArea className={'defInput'}/>
                    </Form.Item>
                    <Form.Item label={' دسته بندی'} name={'productCategoryID'}
                               rules={[{required: true, message: 'لطفا نام محصول را وارد کنید!'}]}>


                        <Select loading={productCategoryLoading} showSearch
                                optionFilterProp="children"

                                filterOption={(input, option) => (option?.name ?? '').toLowerCase().includes(input.toLowerCase())}
                                fieldNames={{label: 'name', value: 'id'}}
                                options={productCategoryList}
                        />


                    </Form.Item>

                </div>
                {/*<div className="flex justify-end">*/}


                {/*    <Form.Item className='mb-0'>*/}
                {/*        <Button loading={loading} className='defBtn submitBtn' htmlType="submit">*/}
                {/*            ثبت*/}
                {/*        </Button>*/}
                {/*    </Form.Item>*/}
                {/*</div>*/}


            </Form>


        </Modal>
    </>);
};
export default EditCatalog;