import React, {useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import BusinessRequest from "../../../../helper/request/BusinessRequest";
import {ShowNotification} from "../../../../helper/ShowNotification";
import {Button, Form, Input, Select} from "antd";
import TextAreaBox from "../../../../microComponents/common/uikits/TextAreaBox";
import {CatalogCreate, GetAllProductCategory,} from "../../../../helper/apiList/apiListBusiness";
import {number13} from "../../../../helper/validation/Validation";

const CreateCatalog = ({isModal, handleCancel, getData}) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [productCategoryList, setProductCategoryList] = useState([]);
  const [productCategoryLoading, setProductCategoryLoading] = useState(true);
  const [open, setOpen] = useState(false);
  const navigate = useNavigate();
  const selectRef = React.useRef(null);

  let {id} = useParams();
  const handleCreateCatalog = async (values) => {
    setLoading(true);

    values.prices = [
      { PriceValue: Number(values.price) },
      // , "FromDate": "", "ToDate": ""}
    ];

    await BusinessRequest.init(
      CatalogCreate,
      function (res) {
        console.log(values);
        setLoading(false);
        if (res.data.messageCode === 0) {
          ShowNotification("کالا با موفقیت ثبت شد", true);
          form.resetFields();
          getData(values, res.data.valueText);



        } else {
          ShowNotification(res.data.message, false);
        }
      },
      function (error) {
        setLoading(false);
      }
    )
      .setBID(id)
      .setMainData(values)
      .setRouter(navigate)
      .callRequest();
  };

  useEffect(() => {
    getCategory();
  }, []);
  const getCategory = (x) => {
    form.setFieldValue("ProductCategoryId", x);
    let data = { PageNumber: "1", CountInPage: "2147000000" };

    BusinessRequest.init(
      GetAllProductCategory,
      function (data) {
        setProductCategoryList(data.data.list);

        setProductCategoryLoading(false);
      },
      function (error) {
        setProductCategoryLoading(false);
      }
    )
      .setBID(id)
      .setMainData(data)
      .setRouter(navigate)
      .callRequest();
  };
  const onFinishFailed = (errorInfo) => {};
  const handleOpenSelect = () => {
    selectRef.current.blur();
    setOpen(false);
  };
  return (
    <div>
      <Form
        form={form}
        name="basic"
        className={`${!isModal && "defBox"}`}
        layout="vertical"
        onFinish={handleCreateCatalog}
        onFinishFailed={onFinishFailed}
        autoComplete="0n"
      >
        <div className="grid gap-4   xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 gap-6 grid-flow-row">
          <Form.Item className={'input_wrap'} label="نام محصول" name={"name"}
                     rules={[{required: true, message: 'لطفا نام محصول را وارد کنید!'}]}>
            <Input className={'defInput'}/>
          </Form.Item>

          <Form.Item className={'input_wrap'} label="شناسه محصول" name={"productTaxID"} rules={[{
            pattern: number13, message: `لطفا  شناسه محصول  را درست وارد کنید!`
          }]}>
            <Input className={'defInput'}/>
          </Form.Item>
          <Form.Item className={'input_wrap'} label="کد محصول" name={"code"}>
            <Input className={'defInput'}/>
          </Form.Item>
          <Form.Item className={'input_wrap'} label="قیمت محصول" name={"price"}>
            <Input className={'defInput'}/>
          </Form.Item>
          <Form.Item className={'input_wrap'} label="سایر وجوه قانونی" name={"legalRate"}>
            <Input className={'defInput'}/>
          </Form.Item>
          <Form.Item className={'input_wrap'} label="سایر مالیات وعوارض" name={"otherRate"}>
            <Input className={'defInput'}/>
          </Form.Item>
          <Form.Item className={'input_wrap'} label="مالیات بر ارزش افزوده" name={"taxRate"}>
            <Input className={'defInput'}/>
          </Form.Item>
          <Form.Item className={'input_wrap'} label="توضیحات محصول" name={"description"}>
            <TextAreaBox/>
          </Form.Item>

          <Form.Item className={'input_wrap'} label={" دسته بندی"} name={"ProductCategoryId"}
                     rules={[{required: true, message: 'لطفا  دسته بندی را وارد کنید!'}]}>
            <Select
                loading={productCategoryLoading}
                showSearch
                optionFilterProp="children"
                open={open}
                ref={selectRef}
                filterOption={(input, option) =>
                    (option?.name ?? "").toLowerCase().includes(input.toLowerCase())
                }
                fieldNames={{label: "name", value: "id"}}
                onDropdownVisibleChange={(visible) => setOpen(visible)}
                dropdownRender={(menu) => (
                    <>
                      {menu}
                      {/*setParams(prev => ({...prev, page: value}));*/}

                      {/*<CreateCategoryModal openSelect={handleOpenSelect}*/}
                      {/*                     getCategory={getCategory}/>*/}
                    </>
                )}
                options={productCategoryList}
            />
          </Form.Item>
        </div>
        <div className="flex justify-end">
          {isModal && (
              <Form.Item className="mb-0">
                <Button className="defBtn borderBtn ml-2" onClick={handleCancel}>
                  انصراف
                </Button>
              </Form.Item>
          )}
          <Form.Item className="mb-0">
            <Button
                loading={loading}
                className="defBtn submitBtn"
                htmlType="submit"
            >
              ثبت
            </Button>
          </Form.Item>
        </div>
      </Form>
    </div>
  );
};

export default CreateCatalog;
