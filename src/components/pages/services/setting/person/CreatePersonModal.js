import {Button, Modal} from 'antd';
import React, {useState} from 'react';
import Person from "./Person";


const CreatePersonModal = ({getValueSearch, disabled, getData}) => {

    const [open, setOpen] = useState(false);


    const showModal = () => {
        setOpen(true);
    };

    const handleOk = (values,id) => {
        handleCancel()

        getValueSearch(values,id)
    };
    const handleCancel = () => {
        setOpen(false);
    };


    return (<>


        <Button className='addBtn text-blue' onClick={showModal} disabled={disabled}
                type="primary">+</Button>

        <Modal
            width={800}
            open={open}
            title={'ایجاد اشخاص'}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={null}>

            <Person isModal handleCancel={handleCancel} getData={handleOk}/>
        </Modal>
    </>);
};
export default CreatePersonModal;