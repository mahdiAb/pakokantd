import React, {useEffect, useMemo, useRef, useState} from 'react';
import {AgGridReact} from 'ag-grid-react';
import {useNavigate, useParams} from "react-router-dom";
import BusinessRequest from "../../../../helper/request/BusinessRequest";
import {GetAllPerson} from "../../../../helper/apiList/apiListBusiness";
import EditPerson from "./EditPerson";
import DeletePerson from "./DeletePerson";
import Paginate from "../../../../helper/Paginate";
import {AG_GRID_LOCALE_FA} from "../../../../helper/locale.fa";
import {Button} from "antd";
import {connect} from "react-redux";
import {
    handleSettingActiveKey,
    handleSettingNewTabIndex,
    handleSettingTabList
} from "../../../../redux/reducer/Reducer";
import CreatePerson from "./CreatePerson";
import {ShowNotification} from "../../../../helper/ShowNotification";
import Person from "./Person";


const PersonList = ({
                        handleSettingTabList, handleSettingActiveKey, handleSettingNewTabIndex,
                        newTab, tabListState, newTabIndex,
                    }) => {
    const gridRef = useRef(null);
    const [loading, setLoading] = useState(false);
    const [params, setParams] = useState({PageNumber: '1', CountInPage: '10'})
    const [reCall, setReCall] = useState(false)
    const [totalRecord, setTotalRecord] = useState(0)
    const [didMount, setDidMount] = useState(false);
    const [addTab, setAddTab] = useState({})
    useEffect(() => {

        if (addTab.id) {
            add(addTab)
        }
    }, [addTab])
    const add = (data) => {

        handleSettingNewTabIndex()
        const newPanes = [...tabListState];
        newPanes.push({
            label: data.label, children: data.children, id: data.id, reopen: true, key: newTabIndex,
        });
        handleSettingTabList(newPanes)
        handleSettingActiveKey(newTabIndex);

    };


    let {id} = useParams();
    const navigate = useNavigate();
    const [data, setData] = useState([]);
    const defaultColDef = useMemo(() => {
        return {
            enableRowGroup: true,
            enablePivot: true,
            enableValue: true,
            sortable: true,
            resizable: true,
            filter: true,
            flex: 1,
            minWidth: 150,
        };
    }, []);


    const BtnCellRenderer = (data) => {


        return (


            <div className='flex items-center h-full mt-1'>
                <DeletePerson data={data} getData={() => setReCall(prev => (!prev))}/>
                <EditPerson   data={data} getData={() => setReCall(prev => (!prev))}/>
            </div>
        )

    }
    const [columnDefs, setColumnDefs] = useState([

        {
            headerName: "",

            valueGetter: "node.rowIndex + 1",
            width: 60, editable: false,
            maxWidth: 60,
        },
        {
            headerName: 'نام',
            field: 'firstName',
            minWidth: 90,
        },
        {
            headerName: 'نام خانوادگی',
            field: 'lastName',
            minWidth: 110,
        },
        {
            headerName: 'شماره موبایل',
            field: 'phoneNumber',

            minWidth: 120,
        },

        {
            headerName: 'نوع شخص',
            field: 'personType.title',
            minWidth: 110,
        },
        {
            headerName: 'شماره ملی',
            field: 'nationalID',
            minWidth: 110,
        },


        {
            headerName: 'کد پستی',
            field: 'postalCode',
            minWidth: 110,
        },
        {
            headerName: 'شماره ثبت',
            field: 'registrationNumber',
            minWidth: 100,
        },

        {
            headerName: 'شماره اقتصادی',
            field: 'economicalNumber',
            minWidth: 120,
        },


        {
            headerName: 'عملیات',
            field: 'PersonCategoryID',
            cellRenderer: BtnCellRenderer,
            pinned: 'left',
            width: 180,
            minWidth: 180,

        },


    ]);


    useEffect(() => {


        if (didMount) {
            onGridReady()

        } else {
            setDidMount(true)
        }
    }, [params, reCall])


    const pageNumberValue = (value) => {
        setParams(prev => ({...prev, PageNumber: String(value)}));
    }
    const localeText = useMemo(() => {
        return AG_GRID_LOCALE_FA;
    }, []);


    const onGridReady = () => {
        gridRef.current.api.showLoadingOverlay();
        BusinessRequest.init(GetAllPerson, function (data) {
            gridRef.current.api.hideOverlay();

            setLoading(false)




            if (data.data.messageCode === 0) {

                setData(data.data.result.list)
                setTotalRecord(data.data.result.totalRecord)
             } else {
                ShowNotification(data.data.message, false)

            }



        }, function (error) {
            gridRef.current.api.hideOverlay();
            setLoading(false)
        }).setBID(id).setMainData(params).setRouter(navigate).callRequest()
    }
    return (
        <div className='h-full'>



       <div className="flex justify-end mb-2">     <Button onClick={() => setAddTab({
           label: 'ایجاد شخص',
           id: 'CreatePerson',
           children: <Person/>,
       })} className='defBtn submitBtn'>ایجاد شخص</Button>
       </div>

            <div className="h-full ag-theme-alpine defBox">
                <AgGridReact Scrolling
                             localeText={localeText}
                             ref={gridRef}
                             rowData={data}
                             enableRtl enableCellTextSelection={true}
                             onGridReady={onGridReady}
                             columnDefs={columnDefs}
                             defaultColDef={defaultColDef}

                >

                </AgGridReact>
                {
                    totalRecord > 10 && <Paginate totalRecord={totalRecord}

                                                  pageNumberValue={pageNumberValue}
                                                  pageNumber={params.PageNumber}/>}
            </div>
        </div>
    );
};


function mapStateToProps(state) {
    return {
        tabListState: state.settingTabList,
        newTabIndex: state.settingNewTabIndex,
        activeKey: state.settingActiveKey,
        newTab: state.settingNewTab,
    }
}


export default connect(mapStateToProps, {
    handleSettingTabList,
    handleSettingActiveKey,
    handleSettingNewTabIndex
})(PersonList);




