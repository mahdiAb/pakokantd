import React, {useEffect, useState} from 'react';
import {Button, Form, Input, Select} from "antd";
import {useNavigate, useParams} from "react-router-dom";
import BusinessRequest from "../../../../helper/request/BusinessRequest";
import {
    CreatePerson,
    GetAllPersonTypes,
    PersonsServiceCities,
    PersonsServiceStates
} from "../../../../helper/apiList/apiListBusiness";
import {ShowNotification} from "../../../../helper/ShowNotification";
import FormItem from "../../../../microComponents/common/uikits/FormItem";
import TextAreaBox from "../../../../microComponents/common/uikits/TextAreaBox";
import {mobile, number10, number14} from "../../../../helper/validation/Validation";

const Person = ({isModal, handleCancel, handleOk, data, personId, isEdit, getData, url = CreatePerson}) => {
    const [form] = Form.useForm();
     const [loading, setLoading] = useState(false);
    const [PersonType, setPersonType] = useState(true);
    const [PersonTypes, setPersonTypes] = useState([]);
    const [PersonTypesLoading, setPersonTypesLoading] = useState(true);
    const [cityLoading, setCityLoading] = useState(false)
    const [cityList, setCityList] = useState([])
    const [stateList, setStateList] = useState([])
    let [stateLoading, setStateLoading] = useState(true)

    let initialValues;
    if (data) {

        data.personTypeID = String(data.personTypeID)
        data.cityID = data?.addressList.length > 0 ? data?.addressList[0]?.cityID === 0 ? null : String(data?.addressList[0]?.cityID) : null
        // data.cityID =cityId
        data.stateID = data?.addressList.length > 0 ? data?.addressList[0]?.stateID === 0 ? null : String(data?.addressList[0]?.stateID) : null
        data.extra = data?.addressList[0]?.extra
        initialValues = data
    }
    useEffect(() => {

        if (isEdit && !!data.stateID && data.stateID != 'undefined') {

            getCities(data?.stateID)
        }


    }, [data?.stateID])

    const navigate = useNavigate();
    let {id} = useParams();


    useEffect(() => {


        BusinessRequest.init(GetAllPersonTypes, function (res) {

            setPersonTypesLoading(false)

            setPersonTypes(res.data)
        }, function (error) {
            setPersonTypesLoading(false)
        }).setBID(id).setRouter(navigate).callRequest()


    }, [])


    useEffect(() => {


        BusinessRequest.init(PersonsServiceStates, function (res) {
            setStateLoading(false)
            setStateList(res.data)


            if (res.data.messageID === 0) {


            } else {

            }
        }, function (error) {
            setStateLoading(false)

        }).setBID(id).setRouter(navigate).callRequest()


    }, [])

    const handlePerson = (values) => {

        setLoading(true)

        values.addressList = [{}]
        values.addressList[0].stateID = values.stateID
        values.addressList[0].cityID = values.cityID
        values.addressList[0].extra = values.extra

        values.personTypeID = Number(values.personTypeID)
        if (initialValues) {
            values.id = initialValues?.id
        }

        BusinessRequest.init(url, function (res) {
            setLoading(false)
            if (res.data.messageCode === 0) {
                form.resetFields()

                ShowNotification(isEdit ?  'شخص با موفقیت ویرایش شد': 'شخص با موفقیت ثبت شد', true)
                if (isEdit) {
                    getData()
                } else {
                    getData(values, res.data.valueText,)
                }


            } else {
                ShowNotification(res.data.message, false)

            }
        }, function (error) {
            setLoading(false)
        }).setBID(id).setRouter(navigate).setMainData(values).callRequest()
    }
    const getCities = (stateId) => {
        // setCityId(data?.addressList.length > 0 ? data?.addressList[0]?.cityID === 0 ? null : String(data?.addressList[0]?.cityID) : null)

        form.setFieldValue('cityID', null)
        setCityLoading(true)

        let value = {StateID: stateId}
        BusinessRequest.init(PersonsServiceCities, (res) => {

            setCityList(res.data)
            setCityLoading(false)
        }, (error) => {

        }).setBID(id).setMainData(value).setRouter(navigate).callRequest()


    }
    useEffect(() => {
        if (data) {
            if (data?.addressList.length && data?.addressList[0]?.city?.state) {

                getCities(data?.addressList[0]?.city?.state?.id)
            }
        }


    }, [])


    return (
      <div>

            <Form
                form={form}
                name="basic"
                layout="vertical " className={`${!isModal && 'defBox'}`}
                initialValues={initialValues}
                onFinish={handlePerson}

                autoComplete="0n"
            >
                <div
                    className="grid gap-4   xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 gap-6 grid-flow-row">

                    <Form.Item label='نام' name={'firstName'} className={'input_wrap'}>
                        <Input className={'defInput'}/>
                    </Form.Item>
                    <Form.Item label='نام خانوادگی' name={'lastName'} className={'input_wrap'} rules={[
                        {
                            required: true,
                            message: 'نام خانوادگی را وارد کنید',
                        },

                    ]}>


                        <Input className={'defInput'}/>
                    </Form.Item>


                    <Form.Item label='شماره موبایل' className={'input_wrap'} name={'phoneNumber'}
                               rules={[{
                                   pattern: mobile, message: `لطفا  شماره موبایل  را درست وارد کنید!`
                               }]}

                    >
                        <Input className={'defInput'}/>
                    </Form.Item>

                    <Form.Item label={'نوع شخص'} className={'input_wrap'} name={'personTypeID'} rules={[
                        {
                            required: true,
                            message: 'نوع شخص را وارد کنید',
                        },

                    ]}>

                        <Select loading={PersonTypesLoading} className={''}
                        >
                            {PersonTypes.map((item, index) => (
                                <Select.Option key={index} value={`${item.id}`}>{item.title}</Select.Option>))}


                        </Select>

                    </Form.Item>

                    <Form.Item label={PersonType ? 'کد ملی' : 'شناسه ملی'} name={'nationalID'} className={'input_wrap'}
                               rules={[{
                                   pattern: number10,
                                   message: ` لطفا   ${PersonType ? 'کد ملی' : 'شناسه ملی'}   را درست وارد کنید!`
                               }]}>
                        <Input type={'number'} className={'defInput'}/>
                    </Form.Item>
                    <Form.Item label='شماره ثبت' name={'registrationNumber'} className={'input_wrap'}>


                        <Input type={'number'} className={'defInput'}/>
                    </Form.Item>
                    <Form.Item label='شماره فکس' name={'Fax'} className={'input_wrap'}>


                        <Input type={'number'} className={'defInput'}/>
                    </Form.Item>

                    <Form.Item label='شماره اقتصادی' className={'input_wrap'} name={'economicalNumber'}


                               rules={[{
                                   pattern: number14, message: `لطفا  شماره اقتصادی  را درست وارد کنید!`
                               }]}

                    ><Input
                        className={'defInput'} type={'number'}/></Form.Item>


                    <Form.Item label={'استان'} name={'stateID'} className={'input_wrap'}>

                        <Select onChange={getCities} loading={stateLoading}


                        >
                            {
                                stateList.map((item, index) => (
                                    <Select.Option key={index} value={`${item.id}`}>{item.title}</Select.Option>
                                ))
                            }
                        </Select>

                    </Form.Item>


                    <Form.Item name={'cityID'} className={'input_wrap'} label={'شهر'}>

                        <Select
                            defaultValue={data ? initialValues.cityID : null}
                        >
                            {
                                cityList.map((item, index) => (

                                    <Select.Option key={index} value={`${item.id + ""}`}>{item.title}</Select.Option>
                                ))
                            }
                        </Select>

                    </Form.Item>

                    <FormItem title={'آدرس کامل'} name={'extra'}>
                        <TextAreaBox rows={3}/></FormItem>

                    <Form.Item label='کد پستی' name={'postalCode'} className={'input_wrap'}
                               rules={[{
                                   pattern: number10, message: `لطفا کد پستی  را درست وارد کنید!`
                               }]}
                    >


                        <Input className={'defInput'}/>
                    </Form.Item>

                </div>

                <div className="flex justify-end items-center">


                    {
                        isModal && <Form.Item className='mb-0'>
                            <Button className='defBtn borderBtn ml-2' onClick={handleCancel}>
                                انصراف
                            </Button>
                        </Form.Item>
                    }
                    <Form.Item className='mb-0'>
                        <Button loading={loading} className='defBtn submitBtn' htmlType="submit">
                            ثبت
                        </Button>
                    </Form.Item>
                </div>
            </Form>
        </div>
    );
};

export default Person;