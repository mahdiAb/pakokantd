import {Button, Modal} from 'antd';
import React, {useEffect, useState} from 'react';
import {AiFillEdit} from "@react-icons/all-files/ai/AiFillEdit";
import Person from "./Person";
import {UpdatePerson} from "../../../../helper/apiList/apiListBusiness";


const EditPerson = ({data, getData,}) => {


    const [open, setOpen] = useState(false);
    const [personData, setPersonData] = useState(data.data);


    useEffect(() => {
        setPersonData(data.data)
    }, [data])


    const showModal = () => {
        setOpen(true);
    };


    const handleCancel = () => {
        setOpen(false);
    };


    const handleData = () => {
        getData()
        handleCancel()
    };


    return (<>


        <Button
            className='  iconBtn yellow' onClick={showModal}><AiFillEdit size={18}/></Button>


        <Modal
            width={800}
            open={open}
            title={'ویرایش اشخاص'}

            onCancel={handleCancel}
            footer={null}>

            <Person isEdit isModal data={personData} getData={handleData} url={UpdatePerson}/>


        </Modal>
    </>);
};
export default EditPerson;