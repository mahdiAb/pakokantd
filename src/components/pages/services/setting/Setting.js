import React from 'react';
import LayoutServices from "../../../layout/LayoutServices";
import {connect} from "react-redux";
import {handleSettingActiveKey, handleSettingNewTabIndex, handleSettingTabList} from "../../../redux/reducer/Reducer";
import CreatePerson from "./person/CreatePerson";
import PersonList from "./person/PersonList";
import CreateCatalog from "./Catalog/CreateCatalog";
import CatalogList from "./Catalog/CatalogList";
import CreateCategoryModal from "./Catalog/Category/CreateCategoryModal";
import CategoryList from "./Catalog/Category/CategoryList";
import UserList from "./Users/UserList";
import {IoSettingsOutline} from "@react-icons/all-files/io5/IoSettingsOutline";
import {MdAddShoppingCart} from "@react-icons/all-files/md/MdAddShoppingCart";
import {FiUsers} from "@react-icons/all-files/fi/FiUsers";
import {BsFillCircleFill} from "@react-icons/all-files/bs/BsFillCircleFill";


let items = [
    {
        icon: <IoSettingsOutline size={20}/>,
        key: 's1',
        label: 'اشخاص',
        submenu: [
            // {
            //     icon: <BsFillCircleFill size={5} color={'var(--black-600)'}/>,
            //     id: 'CreatePerson',
            //     reopen: false,  key: '1-1',
            //     children: <CreatePerson/>,
            //
            //     label: 'ایجاد شخص'
            // },
            {
                icon: <BsFillCircleFill size={5} color={'var(--black-600)'}/>,
                id: 'PersonList',
                reopen: true,
                children: <PersonList/>,
                key: '1-2',
                label: 'لیست اشخاص'
            },]
    },
    {
        icon: <MdAddShoppingCart size={20}/>,
        key: 's2',
        label: 'کالا و خدمات',
        submenu: [
            // {
            //     icon: <BsFillCircleFill size={5} color={'var(--black-600)'}/>,
            //     id: 'CreateCatalog',
            //     reopen: false,
            //     children: <CreateCatalog/>,
            //     key: '2-1',
            //     label: 'ایجاد کالا'
            // },
            {
                icon: <BsFillCircleFill size={5} color={'var(--black-600)'}/>,
                id: 'CatalogList',
                reopen: true,
                children: <CatalogList/>,
                key: '2-2',
                label: 'لیست کالا'
            },

            {
                icon: <BsFillCircleFill size={5} color={'var(--black-600)'}/>,
                id: 'CategoryList',
                reopen: true,
                children: <CategoryList/>,
                key: '2-2',
                label: 'لیست  دسته بندی'
            },]
    },
    {
        icon: <FiUsers size={20}  />,
        key: 's3',
        label: 'کاربران',
        id: 'UserList',
        reopen: false,
        children: <UserList/>,
    },

]


const Setting = ({handleSettingTabList, handleSettingActiveKey, handleSettingNewTabIndex,
                     newTab, tabListState, activeKey, newTabIndex,
                 }) => {

   const add = (data) => {

        let result = tabListState.findIndex(item => item.id === data.id);

        const handleAdd = () => {
            handleSettingNewTabIndex()
            const newPanes = [...tabListState];
            newPanes.push({
                label: data.label, children: data.children, id: data.id, reopen: data.reopen, key: newTabIndex,
            });
            handleSettingTabList(newPanes)
            handleSettingActiveKey(newTabIndex);
        }
        if (result > -1) {
            if (data.reopen) {
                handleAdd()
            } else {

                handleSettingNewTabIndex()
                const newPanes = [...tabListState];
                newPanes[result].key = newTabIndex
                handleSettingTabList(newPanes)
                handleSettingActiveKey(newTabIndex);


            }
        } else {
            handleAdd()
        }

    };

    const remove = (item) => {
        const newTabListState = [...tabListState];
        const index = newTabListState.indexOf(item);

        newTabListState.splice(index, 1);


        if (index >= newTabListState.length && index > 0) {
            handleSettingActiveKey(newTabListState[index - 1]);
        }

        handleSettingTabList(newTabListState)


    };


    const handleSettingActive = (x) => {
        handleSettingActiveKey(x)
    }
    return (


        <LayoutServices tabs={tabListState} items={items}
                        activeKey={activeKey}
                        handleNewTabIndex={handleSettingNewTabIndex}
                        handleCloseTab={remove}
                        handleAddTab={add}
                        handleActiveKey={handleSettingActive}
                        handleTabList={handleSettingTabList}


        />

    );
};


function mapStateToProps(state) {
    return {
        tabListState: state.settingTabList,
        newTabIndex: state.settingNewTabIndex,
        activeKey: state.settingActiveKey,
        newTab: state.settingNewTab,
    }
}


export default connect(mapStateToProps, {
    handleSettingTabList,
    handleSettingActiveKey,
    handleSettingNewTabIndex
})(Setting);

