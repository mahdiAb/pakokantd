import React, {useState} from 'react';
import {Button, Modal} from 'antd';
import {useNavigate, useParams} from "react-router-dom";
import BusinessRequest from "../../../../helper/request/BusinessRequest";
import {ShowNotification} from "../../../../helper/ShowNotification";
import {RemoveMapUserBusiness, RemovePerson} from "../../../../helper/apiList/apiListBusiness";
import {FiTrash2} from "@react-icons/all-files/fi/FiTrash2";


const DeleteUser = ({data, getData}) => {
    console.log(data.data)

    const [open, setOpen] = useState(false);


    const [loading, setLoading] = useState(false);

    const navigate = useNavigate();


    let {id} = useParams();


    const showModal = () => {
        setOpen(true);
    };

    const handleOk = () => {
        setLoading(true)
        let values = {
            f_UserID: data.data.f_UserID
        }

        BusinessRequest.init(RemoveMapUserBusiness, function (data) {


            setLoading(false)
            handleCancel()
            if (data.data.messageCode === 0 || data.data.messageID === 0) {

            getData()
            ShowNotification(' کاربر با موفقیت حذف شد', true)
            } else {
                ShowNotification(data.data.message, false)
                setLoading(false)
            }
        }, function (error) {
            setLoading(false)
            handleCancel()

        }).setBID(id).setRouter(navigate).setMainData(values).callRequest()
    };
    const handleCancel = () => {
        setOpen(false);
    };


    return (<>


        <Button
            className='  iconBtn red' onClick={showModal}><FiTrash2 size={18}/></Button>


        <Modal
            width={800}
            open={open}
            title={'حذف کاربر'}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[<div className='flex justify-end items-center'>
                <Button key="cancel" className='defBtn borderBtn  ' type="primary" onClick={handleCancel}>
                    انصراف
                </Button>
                <Button key="submit " loading={loading} className='defBtn submitBtn ' type="primary" onClick={handleOk}>
                    ثبت
                </Button>

            </div>]}>

            <p className='font-bold text-sm'>آیا از حذف این کاربر اطمینان دارید؟ </p>

        </Modal>
    </>);
};
export default DeleteUser;