import React, {useEffect, useMemo, useRef, useState} from 'react';
import {AgGridReact} from 'ag-grid-react';
import {useNavigate, useParams} from "react-router-dom";
import BusinessRequest from "../../../../helper/request/BusinessRequest";
import {GetAllMapUserBusiness} from "../../../../helper/apiList/apiListBusiness";
import Paginate from "../../../../helper/Paginate";
import DeleteUser from "./DeleteUser";
import AddUser from "./AddUser";
import {AG_GRID_LOCALE_FA} from "../../../../helper/locale.fa";


const UserList = () => {
    const gridRef = useRef(null);
    const [loading, setLoading] = useState(false);
    const [params, setParams] = useState({PageNumber: '1', CountInPage: '10'})
    const [reCall, setReCall] = useState(false)
    const [totalRecord, setTotalRecord] = useState(0)
    const [didMount, setDidMount] = useState(false);

    let {id} = useParams();
    const navigate = useNavigate();
    const [data, setData] = useState([]);
    const defaultColDef = useMemo(() => {
        return {
            enableRowGroup: true,
            enablePivot: true,
            enableValue: true,
            sortable: true,
            resizable: true,
             flex: 1,
            minWidth: 150,
        };
    }, []);


    const BtnCellRenderer = (data) => {


        return (


            <div className='flex items-center h-full mt-0.5'>
                <DeleteUser data={data} getData={() => setReCall(prev => (!prev))}/>
                {/*<EditUser data={data} getData={() =>  setReCall(prev => (!prev))}/>*/}
            </div>
        )

    }
    const [columnDefs, setColumnDefs] = useState([

        {
            headerName: "",

            valueGetter: "node.rowIndex + 1",
            width: 60, editable: false,
            maxWidth: 60,
        },
        {
            headerName: 'نام و نام خانوادگی',
            field: 'name',

        },

        {
            headerName: 'شماره موبایل',
            field: 'mobileNumber',


        },


        {
            headerName: 'حذف',
            field: 'PersonCategoryID',
            cellRenderer: BtnCellRenderer,
            pinned: 'left',
            width: 80,
            maxWidth: 80,

        },


    ]);


    useEffect(() => {


        if (didMount) {
            onGridReady()

        } else {
            setDidMount(true)
        }
    }, [params, reCall])


    const pageNumberValue = (value) => {
        setParams(prev => ({...prev, PageNumber: String(value)}));
    }
    const localeText = useMemo(() => {
        return AG_GRID_LOCALE_FA;
    }, []);


    const onGridReady = () => {
        gridRef.current.api.showLoadingOverlay();
        BusinessRequest.init(GetAllMapUserBusiness, function (data) {
            console.log(data)
            gridRef.current.api.hideOverlay();
            setData(data.data)
            // setTotalRecord(data.data.totalRecord)
            setLoading(false)
        }, function (error) {
            gridRef.current.api.hideOverlay();
            setLoading(false)
        }).setBID(id).setMainData(params).setRouter(navigate).callRequest()
    }

    return (
        <div className='h-full'>
            <AddUser getData={() => setReCall(prev => (!prev))}/>
            <div className="h-full ag-theme-alpine defBox">
                <AgGridReact Scrolling
                             localeText={localeText}
                             ref={gridRef}
                             rowData={data}
                             enableRtl
                             onGridReady={onGridReady}
                             columnDefs={columnDefs}
                             defaultColDef={defaultColDef}


                >

                </AgGridReact>
                {
                    totalRecord > 10 && <Paginate totalRecord={totalRecord}

                                                  pageNumberValue={pageNumberValue}
                                                  pageNumber={params.PageNumber}/>}
            </div>
        </div>
    );
};

export default UserList