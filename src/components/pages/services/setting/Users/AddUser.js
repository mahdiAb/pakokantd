import React, {useState} from 'react';
import TextInput from "../../../../microComponents/common/uikits/TextInput";
import {useNavigate, useParams} from "react-router-dom";
import {CreateMapUserBusiness, GetUserByMobileNumber} from "../../../../helper/apiList/apiListBusiness";
import BusinessRequest from "../../../../helper/request/BusinessRequest";
import {ShowNotification} from "../../../../helper/ShowNotification";
import {Button, Form, Input, Modal} from "antd";
import FormItem from "../../../../microComponents/common/uikits/FormItem";
import {mobile} from "../../../../helper/validation/Validation";


const AddPersons = ({getData}) => {

    let initialValues = {}

    const [open, setOpen] = useState(false);


    const [form] = Form.useForm();
    const [loading, setLoading] = useState(false);
    const [findUser, setFindUser] = useState(false);
    const [loadingCheckNumber, setLoadingCheckNumber] = useState(false);
    const [userData, setUserData] = useState('');
    const [userDataDes, setUserDataDes] = useState();

    const navigate = useNavigate();


    let {id} = useParams();
    const handleAddUser = () => {
        setLoading(true)

        let values = {'f_UserID': userData.f_UserID}
        BusinessRequest.init(CreateMapUserBusiness, function (data) {
            setLoading(false)
            // handleCancel()

            if (data.data.messageCode === 0) {

                setUserData('')
                setUserDataDes('')
                form.resetFields()
                getData()
                ShowNotification('کاربر با موفقیت اضافه شد شد', true)
                handleCancel()
            } else {
                ShowNotification(data.data.message, false)

            }
        }, function (error) {
            setLoading(false)
            handleCancel()

        }).setBID(id).setRouter(navigate).setMainData(values).callRequest()
    }

    const handleCheckNumber = (number) => {
        setLoadingCheckNumber(true)
        setFindUser(false)

        BusinessRequest.init(GetUserByMobileNumber, function (data) {
            setLoadingCheckNumber(false)
            setUserData(data.data)


            if (data.data) {
                setFindUser(true)
                setUserDataDes(<p className='text-blue'>کاربری به نام {data.data.f_Name + ' ' + data.data.f_Family} پیدا
                    شد. در صورت تایید دکمه ثبت را بزنید.</p>)
            } else {

                setUserDataDes(<p className='text-red'>کاربری با این مشخصات پیدا نشد.</p>)
            }


        }, function (error) {
            setLoadingCheckNumber(false)
            handleCancel()

        }).setBID(id).setRouter(navigate).setMainData(number).callRequest()
    }
    const onFinishFailed = (errorInfo) => {

    };


    const showModal = () => {
        setOpen(true);
        setFindUser(false)
        setUserData()
        form.resetFields()
        setUserDataDes()
    };

    const handleOk = () => {
        form.submit()
    };
    const handleCancel = () => {
        setOpen(false);
        setUserData()
        form.resetFields()
        setUserDataDes()
    };


    return (<>

   <div className='flex justify-end mb-3'>
       <Button
            className='defBtn submitBtn'


           onClick={showModal}>اضافه کردن کاربر</Button>
   </div>


        <Modal
            width={800}
            open={open}
            title={'اضافه کردن کاربران'}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[<div className='flex justify-end items-center'>
                <Button key="cancel" className='defBtn borderBtn  ' type="primary" onClick={handleCancel}>
                    انصراف
                </Button>
                <Button key="submit " loading={loading} disabled={!findUser} className='defBtn submitBtn '
                        type="primary"
                        onClick={handleAddUser}>
                    ثبت
                </Button>

            </div>]}>

            <Form
                form={form}
                name="basic"
                layout="vertical"
                onFinish={handleCheckNumber}
                onFinishFailed={onFinishFailed}
                autoComplete="0n"
            >
                <div
                    className="flex items-end">


                    <Form.Item label='شماره موبایل' name={'MobileNumber'}   rules={[{required: true, message: 'لطفا شماره موبایل را وارد کنید!'},{pattern: mobile, message: 'لطفا شماره موبایل را درست وارد کنید!'}]}>
                        <Input onChange={()=>{
                            setUserData()

                            setUserDataDes()
                            setFindUser(false)
                        }}/>
                    </Form.Item>
                    <FormItem>
                        <Button loading={loadingCheckNumber} className='defBtn submitBtn mr-3' type="primary"
                                htmlType="submit"
                                >
                            بررسی
                        </Button>
                    </FormItem>


                </div>


                {userDataDes}

            </Form>

        </Modal>
    </>);

};

export default AddPersons;