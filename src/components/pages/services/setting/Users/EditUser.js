import {Button, Form, Modal} from 'antd';
import React, {useState} from 'react';
import {AiFillEdit} from "@react-icons/all-files/ai/AiFillEdit";
import FormItem from "../../../../microComponents/common/uikits/FormItem";
import TextInput from "../../../../microComponents/common/uikits/TextInput";
import {mobile} from "../../../../helper/validation/Validation";
import {useNavigate, useParams} from "react-router-dom";
import BusinessRequest from "../../../../helper/request/BusinessRequest";
import {UpdatePerson} from "../../../../helper/apiList/apiListBusiness";
import {ShowNotification} from "../../../../helper/ShowNotification";

const EditUser = ({data, getData}) => {
    let initialValues = data.data

    const [open, setOpen] = useState(false);


    const [form] = Form.useForm();
    const [loading, setLoading] = useState(false);

    const navigate = useNavigate();


    let {id} = useParams();
    const handleEditPerson = (values) => {
        setLoading(true)



        BusinessRequest.init(UpdatePerson, function (data) {
            setLoading(false)
            handleCancel()
            if (data.data.messageCode === 0) {

                getData()
                ShowNotification('کاربر با موفقیت ویرایش شد', true)
            } else {
                ShowNotification(data.data.message, false)

            }
        }, function (error) {
            setLoading(false)
            handleCancel()

        }).setBID(id).setRouter(navigate).setMainData(values).callRequest()
    }


    const onFinishFailed = (errorInfo) => {

    };



    const showModal = () => {
        setOpen(true);
    };

    const handleOk = () => {
        form.submit()
    };
    const handleCancel = () => {
        setOpen(false);
    };


    return (<>


        <Button
            className='  iconBtn yellow' onClick={showModal}><AiFillEdit size={18}/></Button>


        <Modal
            width={800}
            open={open}
            title={'ویرایش کاربران'}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[<div className='flex justify-end items-center'>
                <Button key="cancel" className='defBtn borderBtn  ' type="primary" onClick={handleCancel}>
                    انصراف
                </Button>
                <Button key="submit " loading={loading} className='defBtn submitBtn ' type="primary" onClick={handleOk}>
                    ثبت
                </Button>

            </div>]}


        >

            <Form
                form={form}
                name="basic"
                layout="vertical"
                initialValues={initialValues}
                onFinish={handleEditPerson}
                onFinishFailed={onFinishFailed}
                autoComplete="0n"
            >
                <div
                    className="grid gap-4   xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 gap-6 grid-flow-row">



                    <FormItem title='شماره موبایل' name={'phoneNumber'} required pattern={mobile}>
                        <TextInput/>
                    </FormItem>



                </div>


            </Form>


        </Modal>
    </>);
};
export default EditUser;