import React from 'react';
import LayoutServices from "../../../layout/LayoutServices";
import {connect} from "react-redux";
import {handleInvoiceActiveKey, handleInvoiceNewTabIndex, handleInvoiceTabList} from "../../../redux/reducer/Reducer";
import {AiOutlineAppstoreAdd} from "@react-icons/all-files/ai/AiOutlineAppstoreAdd";
import {BsFillCircleFill} from "@react-icons/all-files/bs/BsFillCircleFill";
import InvoiceList from "./tabs/InvoiceList";
import {BsListOl} from "@react-icons/all-files/bs/BsListOl";
import SendInvoice from "./tabs/SendInvoice";
import {AiOutlineSend} from "@react-icons/all-files/ai/AiOutlineSend";
 import NormalSaleInvoices from "./tabs/NormalSaleInvoice/NormalSaleInvoices";

let items = [{
    key: 's1',
    label: 'ثبت تکی صورت حساب',
    icon: <AiOutlineAppstoreAdd size={20}/>,
    submenu: [
        {
            children: <NormalSaleInvoices  invoiceSubjectID={1} pattern={1} editTable={true} editPayment={true}   showSubmit={true}
                                          editPerson={true} editCatalog={true} editDate={true} addRow={true}
                                          firstAddRow={false}/>, icon: <BsFillCircleFill size={5} color={'var(--black-600)'}/>,
            id: 'CreateInvoice1',
            reopen: false, key: '1-1', label: 'فروش'
        },
        // {
        //     children: <CreateInvoice pattern={2}/>, icon: <BsFillCircleFill size={5} color={'var(--black-600)'}/>,
        //     id: 'CreateInvoice2',
        //     reopen: true, key: '1-2', label: 'فروش ارزی'
        // },
        // {
        //     children: <CreateInvoice pattern={3}/>, icon: <BsFillCircleFill size={5} color={'var(--black-600)'}/>,
        //     id: 'CreateInvoice3',
        //     reopen: false, key: '1-3', label: 'طلا جواهر و پلاتین'
        // },
        // {
        //     children: <CreateInvoice pattern={4}/>, icon: <BsFillCircleFill size={5} color={'var(--black-600)'}/>,
        //     id: 'CreateInvoice4',
        //     reopen: false, key: '1-4', label: 'قرداد پیمانکاری'
        // },
        // {
        //     children: <CreateInvoice pattern={5}/>, icon: <BsFillCircleFill size={5} color={'var(--black-600)'}/>,
        //     id: 'CreateInvoice5',
        //     reopen: false, key: '1-5', label: 'قبوض خدماتی'
        // },
        // {
        //     children: <CreateInvoice pattern={6}/>, icon: <BsFillCircleFill size={5} color={'var(--black-600)'}/>,
        //     id: 'CreateInvoice6',
        //     reopen: false, key: '1-6', label: 'بلیط هواپیما'
        // },
        // {
        //     children: <CreateInvoice pattern={7}/>, icon: <BsFillCircleFill size={5} color={'var(--black-600)'}/>,
        //     id: 'CreateInvoice7',
        //     reopen: false, key: '1-7', label: 'صادرات'
        // }
    ]
},
    // {
    // children: <CreateInvoice pattern={1}/>, id: 'CreateInvoiceGroup',
    // reopen: false, key: 's2', label: 'ثبت گروهی صورت حساب',
    // icon:<RiPlayListAddLine size={20}/>
// },
    {
        children: <InvoiceList/>, id: 'InvoiceList',
        reopen: true, key: 's2', label: 'لیست صورت حساب ها',
        icon: <BsListOl size={20}/>
    },
    {
        children: <SendInvoice/>, id: 'SendInvoice',
        reopen: true, key: 's3', label: 'ارسال حساب ها',
        icon: <AiOutlineSend size={20}/>
    },
]


const Invoice = ({
                     handleInvoiceTabList, handleInvoiceActiveKey, handleInvoiceNewTabIndex,
                     newTab, tabListState, activeKey, newTabIndex,
                 }) => {

    // useEffect(() => {
    //
    //     if (newTab) {
    //         add({children: newTab.children, label: newTab.tabTitle})
    //     }
    // }, [newTab])


    const add = (data) => {
        let result = tabListState.findIndex(item => item.id == data.id);

        const handleAdd = () => {
            handleInvoiceNewTabIndex()
            const newPanes = [...tabListState];
            newPanes.push({
                label: data.label, children: data.children, id: data.id, reopen: data.reopen, key: newTabIndex,
            });
            handleInvoiceTabList(newPanes)
            handleInvoiceActiveKey(newTabIndex);
        }
        if (result > -1) {
            if (data.reopen) {
                handleAdd()
            } else {

                handleInvoiceNewTabIndex()
                const newPanes = [...tabListState];
                newPanes[result].key = newTabIndex
                handleInvoiceTabList(newPanes)
                handleInvoiceActiveKey(newTabIndex);


            }
        } else {
            handleAdd()
        }


    };

    const remove = (item) => {
        const newTabListState = [...tabListState];
        const index = newTabListState.indexOf(item);

        newTabListState.splice(index, 1);


        if (index >= newTabListState.length && index > 0) {
            handleInvoiceActiveKey(newTabListState[index - 1]);
        }

        handleInvoiceTabList(newTabListState)


    };


    const handleInvoiceActive = (x) => {
        handleInvoiceActiveKey(x)
    }
    return (


        <LayoutServices tabs={tabListState} items={items}
                        activeKey={activeKey}
                        handleNewTabIndex={handleInvoiceNewTabIndex}
                        handleCloseTab={remove}
                        handleAddTab={add}
                        handleActiveKey={handleInvoiceActive}
                        handleTabList={handleInvoiceTabList}
        />

    );
};


function mapStateToProps(state) {
    return {
        tabListState: state.invoiceTabList,
        newTabIndex: state.invoiceNewTabIndex,
        activeKey: state.invoiceActiveKey,
        newTab: state.invoiceNewTab,
    }
}


export default connect(mapStateToProps, {
    handleInvoiceTabList,
    handleInvoiceActiveKey,
    handleInvoiceNewTabIndex
})(Invoice);

