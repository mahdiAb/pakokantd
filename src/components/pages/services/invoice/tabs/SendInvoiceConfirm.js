import React, {useState} from 'react';
import {Button, Modal} from 'antd';
import {useNavigate, useParams} from "react-router-dom";
import BusinessRequest from "../../../../helper/request/BusinessRequest";
import {SendInvoiceToTax} from "../../../../helper/apiList/apiListBusiness";
import {ShowNotification} from "../../../../helper/ShowNotification";


const SendInvoiceConfirm = ({data}) => {

    const [open, setOpen] = useState(false);
    const [sendInvoicesLoading, setSendInvoicesLoading] = useState(false);


    const navigate = useNavigate();


    let {id} = useParams();


    const showModal = () => {
        setOpen(true);
    };

    const handleOk = () => {
        handleSendInvoices()
    };

    const handleSendInvoices = () => {


        setSendInvoicesLoading(true)
        BusinessRequest.init(SendInvoiceToTax, function (data) {
            setSendInvoicesLoading(false)
            if (data.data.messageCode === 0) {
                handleCancel()
                ShowNotification(data.data.message, true)

            } else {
                ShowNotification(data.data.message, false)
            }

        }, function (error) {
            setSendInvoicesLoading(false)

        }).setBID(id).setRouter(navigate).setMainData(data).callRequest()


    }

    const handleCancel = () => {
        setOpen(false);
    };


    return (<>


        <Button className='defBtn submitBtn' onClick={showModal}>
            ارسال فاکتور ها
        </Button>

        <Modal
            width={800}
            open={open}
            title={'ارسال صورت حساب'}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[<div className='flex justify-end items-center'>
                <Button key="cancel" className='defBtn borderBtn  ' type="primary" onClick={handleCancel}>
                    انصراف
                </Button>
                <Button key="submit " loading={sendInvoicesLoading} className='defBtn submitBtn ' type="primary"
                        onClick={handleOk}>
                    ارسال
                </Button>

            </div>]}>

            <p className='font-bold text-sm'>آیا از ارسال صورت حساب اطمینان دارید؟ </p>

        </Modal>
    </>);
};
export default SendInvoiceConfirm;