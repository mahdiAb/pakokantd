import React, {memo, useEffect, useMemo, useRef, useState} from 'react';
import {AgGridReact} from 'ag-grid-react';
import {useNavigate, useParams} from "react-router-dom";
import BusinessRequest from "../../../../helper/request/BusinessRequest";
import {GetAllInvoices, UpdateNormalSaleInvoices} from "../../../../helper/apiList/apiListBusiness";

import Paginate from "../../../../helper/Paginate";
import {AG_GRID_LOCALE_FA} from "../../../../helper/locale.fa";
import {Button, Dropdown, Tooltip} from "antd";
import {ShowNotification} from "../../../../helper/ShowNotification";
import {connect} from "react-redux";
import {
    handleInvoiceActiveKey,
    handleInvoiceNewTabIndex,
    handleInvoiceTabList
} from "../../../../redux/reducer/Reducer";
import {BiShow} from "@react-icons/all-files/bi/BiShow";
import DeleteInvoice from "./DeleteInvoice";
import {DateTimeString} from "../../../../helper/DateSolarString";
import {ConvertToPrice} from "../../../../helper/ConvertToPrice";
import NormalSaleInvoices from "./NormalSaleInvoice/NormalSaleInvoices";
import {BiDotsVerticalRounded} from "@react-icons/all-files/bi/BiDotsVerticalRounded";


const InvoiceList = ({
                         handleInvoiceTabList, handleInvoiceActiveKey, handleInvoiceNewTabIndex,
                         newTab, tabListState, newTabIndex,
                     }) => {
    const [params, setParams] = useState({PageNumber: '1', CountInPage: '10'})
    const [totalRecord, setTotalRecord] = useState(0)
    const [didMount, setDidMount] = useState(false);
    const [reCall, setReCall] = useState(false)
    const gridRef = useRef(null);
    const [addTab, setAddTab] = useState({})
    const add = (data) => {

        handleInvoiceNewTabIndex()
        const newPanes = [...tabListState];
        newPanes.push({
            label: data.label, children: data.children, id: data.id, reopen: true, key: newTabIndex,
        });
        handleInvoiceTabList(newPanes)
        handleInvoiceActiveKey(newTabIndex);

    };

    const BtnCellRenderer = (x) => {

        return (


            <div className='flex items-center h-full justify-center mt-0.5'>

                <Tooltip placement="topLeft" title={'نمایش'}><Button onClick={() => setAddTab({
                    label: 'نمایش  صورت حساب',
                    id: 'ShowInvoice',
                    children: <NormalSaleInvoices invoiceId={x.data.id} pattern={1} editTable={false} showSubmit={false}
                                                  editPayment={false}
                                                  editPerson={false} editCatalog={false} editDate={false} addRow={false}
                                                  firstAddRow={false}/>,
                })} className='iconBtn blue'><BiShow size={17}/></Button></Tooltip>
                <Dropdown
                    trigger={['click']}
                    menu={{

                        items: [
                            {
                                key: '1',
                                label: (
                                    <div onClick={() => setAddTab({
                                        label: 'ویرایش  صورت حساب',
                                        id: 'EditInvoice',
                                        children: <NormalSaleInvoices invoiceId={x.data.id} pattern={1} editTable={true} invoiceData={x.data} invoiceTitle={'ویرایش  صورت حساب'}
                                                                      editPayment={true} showTitle
                                                                      url={UpdateNormalSaleInvoices} showSubmit={true}
                                                                      editPerson={true} editCatalog={true}
                                                                      editDate={true} addRow={true}
                                                                      firstAddRow={true}/>,
                                    })}  >ویرایش</div>
                                ),
                            },
                            {
                                key: '2',
                                label: (
                                    <div onClick={() => setAddTab({
                                        label: 'اصلاح  صورت حساب',
                                        id: 'CorrectionInvoice',
                                        children: <NormalSaleInvoices invoiceSubjectID={2} invoiceId={x.data.id} pattern={1} invoiceData={x.data} invoiceTitle={'اصلاح  صورت حساب'}
                                                                      editTable={true} editPayment={true} url={UpdateNormalSaleInvoices}
                                                                      showSubmit={true} showTitle
                                                                      editPerson={false} editCatalog={true} editDate={true} addRow={true}
                                                                      firstAddRow={true}/>,
                                    })}  >اصلاح</div>
                                ),
                            },

                            {
                                key: '3',
                                label: (
                                    <div onClick={() => setAddTab({
                                        label: 'ابطال  صورت حساب',
                                        id: 'CancellationInvoice',
                                        children: <NormalSaleInvoices invoiceSubjectID={3} invoiceId={x.data.id} pattern={1} invoiceData={x.data} invoiceTitle={'ابطال  صورت حساب'}
                                                                      editTable={false} showSubmit={true} showCancellationInvoice={true}
                                                                      editPayment={false} showTitle
                                                                      editPerson={false} editCatalog={false} editDate={true} addRow={false}
                                                                      firstAddRow={false}/>,
                                    })}  >ابطال</div>
                                ),
                            },
                            {
                                key: '4',
                                label: (
                                    <div onClick={() => setAddTab({
                                        label: 'برگشت از فروش  صورت حساب',
                                        id: 'ReturnSaleInvoice',
                                        children: <NormalSaleInvoices invoiceSubjectID={4} showTitle invoiceId={x.data.id} pattern={1} invoiceData={x.data} invoiceTitle={'برگشت از فروش  صورت حساب'}
                                                                      editTable={true} editPayment={true}  showSubmit={true}
                                                                      editPerson={false} editCatalog={true} editDate={true} addRow={true}
                                                                      firstAddRow={true}/>,
                                    })}  >برگشت از فروش </div>
                                ),
                            },
                            {
                                key: '5',danger: true,
                                label: (
                                    <DeleteInvoice invoiceId={x.data.id} getData={() => setReCall(prev => (!prev))}/>
                                ),
                            },

                        ],
                    }}
                >
                    <Button className={'iconBtn black'} >
                        <BiDotsVerticalRounded size={17}/>
                    </Button>
                </Dropdown>





            </div>
        )

    }
    const DateRenderer = (x) => {
        console.log(x)
        return <span>{DateTimeString(x.value)}</span>
    }
    const convertToPriceRenderer = memo(
        (props) => {
            return ConvertToPrice(props.value)
        }
    )
    const [columnDefs, setColumnDefs] = useState([

        {
            headerName: "",

            valueGetter: "node.rowIndex + 1",
            editable: false,
            maxWidth: 60,
            minWidth: 45,
            width: 60,
        },
        {headerName: 'خریدار', field: 'personName', minWidth: 130,},
        {headerName: 'تاریخ صورت حساب', field: 'invoiceDate', cellRenderer: DateRenderer, minWidth: 140,},
        {headerName: 'مبلغ تخفیف', field: 'discountAmount', cellRenderer: convertToPriceRenderer, minWidth: 110,},
        {headerName: 'مالیات', field: 'taxAmount', cellRenderer: convertToPriceRenderer, minWidth: 100,},
        {headerName: 'مبلغ صورت حساب', field: 'amount', cellRenderer: convertToPriceRenderer, minWidth: 140,},
        {headerName: 'شماره داخلی صورت حساب ', field: 'internalInvoiceNo', minWidth: 180,},
        {headerName: 'شماره منحصر به فرد مالیاتی ', field: 'taxInvoiceNo', minWidth: 200,},
        {headerName: 'موضوع صورت حساب ', field: 'invoiceSubject', minWidth: 160,},
        {headerName: 'الگوی صورت حساب', field: 'invoicePattern', minWidth: 160,},
        // {headerName: 'نوع تسویه', field: 'invoiceSettlementMethodID',},
        {headerName: 'وضعیت ارسال', field: 'sendStatus', minWidth: 115,},
        {headerName: 'نتیجه ارسال', field: 'sendResult', minWidth: 110,},


        {
            headerName: 'عملیات', field: 'PersonCategoryID',
            cellRenderer: BtnCellRenderer,
            pinned: 'left',
            width: 130,
            minWidth: 130,
        },
    ]);
    const localeText = useMemo(() => {
        return AG_GRID_LOCALE_FA;
    }, []);

    const defaultColDef = useMemo(() => {
        return {
            flex: 1,
            sortable: true,
            filter: true,
            minWidth: 150,
        };
    }, []);

    let {id} = useParams();


    const [loading, setLoading] = useState(false);

    const [data, setData] = useState([]);
    useEffect(() => {

        if (addTab.id) {
            add(addTab)
        }
    }, [addTab])

    const navigate = useNavigate();
    useEffect(() => {


        if (didMount) {
            onGridReady()

        } else {
            setDidMount(true)
        }
    }, [params, reCall])

    const onGridReady = async () => {




        gridRef.current.api.showLoadingOverlay();

        await BusinessRequest.init(GetAllInvoices, function (res) {

            gridRef.current.api.hideOverlay();
            setLoading(false)
            if (res.data.messageCode === 0) {

                setTotalRecord(res.data.result.totalRecord)
                setData(res.data.result.list)
            } else {
                ShowNotification(res.data.message, false)
            }


        }, function (error) {
            gridRef.current.api.hideOverlay();

            setLoading(false)
        }).setBID(id).setMainData(params).setRouter(navigate).callRequest()

    }

    const pageNumberValue = (value) => {
        setParams(prev => ({...prev, PageNumber: String(value)}));
    }
    return (
        <div className='h-full'>
            <div className="h-full ag-theme-alpine defBox">
                <AgGridReact Scrolling
                             rowData={data}
                             enableRtl   columnHoverHighlight={true}
                             ref={gridRef}
                             enableCellTextSelection={true}
                             columnDefs={columnDefs}
                             defaultColDef={defaultColDef}
                             onGridReady={onGridReady}
                             localeText={localeText}
                             rowHeight={34}
                >

                </AgGridReact>
                {
                    totalRecord > 10 && <Paginate totalRecord={totalRecord}

                                                  pageNumberValue={pageNumberValue}
                                                  pageNumber={params.PageNumber}/>}
            </div>
        </div>
    );
};


function mapStateToProps(state) {
    return {
        tabListState: state.invoiceTabList,
        newTabIndex: state.invoiceNewTabIndex,
        activeKey: state.invoiceActiveKey,
        newTab: state.invoiceNewTab,
    }
}


export default connect(mapStateToProps, {
    handleInvoiceTabList,
    handleInvoiceActiveKey,
    handleInvoiceNewTabIndex
})(InvoiceList);





