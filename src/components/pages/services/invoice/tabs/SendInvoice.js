import React, {memo, useEffect, useMemo, useRef, useState} from 'react';
import BusinessRequest from "../../../../helper/request/BusinessRequest";
import {GetAllUnSendInvoices, GetLastSentInvoice} from "../../../../helper/apiList/apiListBusiness";
import {useNavigate, useParams} from "react-router-dom";
import KeyValue from "../../../../microComponents/common/keyValue/KeyValue";
import BusinessListLoading from "../../../../skeletonLoading/GetLastSentInvoiceLoading";
import {Button, Form} from "antd";
import {ShowNotification} from "../../../../helper/ShowNotification";
import {DateTimeString} from "../../../../helper/DateSolarString";
import DateBox from "../../../../microComponents/common/uikits/DateBox";
import {ConvertToPrice} from "../../../../helper/ConvertToPrice";
import {AgGridReact} from "ag-grid-react";
import {AG_GRID_LOCALE_FA} from "../../../../helper/locale.fa";
import SendInvoiceConfirm from "./SendInvoiceConfirm";

const SendInvoice = () => {

    const navigate = useNavigate();
    let {id} = useParams();
    const [form] = Form.useForm();
    const [loading, setLoading] = useState(true);
    const [loading2, setLoading2] = useState(false);
    const [getInvoicesData, setGetInvoicesData] = useState();
    const [invoicesDataText, setInvoicesDataText] = useState(false);
    const [lastFactor, setLastFactor] = useState({});
    const [lastFactorNull, setLastFactorNull] = useState(false);
    const gridRef = useRef(null);
    const [data, setData] = useState([]);

    useEffect(() => {
        BusinessRequest.init(GetLastSentInvoice, function (data) {


            setLastFactorNull(data?.data === null)

            setLoading(false)


            if (data.data.messageCode === 0) {
                setLastFactor(data.data.result)
                setLastFactorNull(data?.data === null)

            } else {
                ShowNotification(data.data.message, false)
            }
        }, function (error) {
            setLoading(false)

        }).setBID(id).setRouter(navigate).callRequest()
    }, [])

    const handleGetInvoicesData = (values) => {
        setInvoicesDataText(false)

        setLoading2(true)
        gridRef.current?.api?.showLoadingOverlay();
        BusinessRequest.init(GetAllUnSendInvoices, function (data) {
            setLoading2(false)
            gridRef.current?.api?.hideOverlay();
            if (data.data.messageCode === 0) {
                setGetInvoicesData(data.data.result)
                setInvoicesDataText(data?.data?.result?.total === 0)
                setData(data.data.result.subjectDetailList)

            } else {
                ShowNotification(data.data.message, false)
            }

        }, function (error) {
            setLoading2(false)
            gridRef.current?.api?.hideOverlay();
        }).setBID(id).setRouter(navigate).setMainData(values).callRequest()

    }

    const onFinishFailed = (errorInfo) => {
        console.log(errorInfo)


    };

    const localeText = useMemo(() => {
        return AG_GRID_LOCALE_FA;
    }, []);
    const defaultColDef = useMemo(() => {
        return {
            enableRowGroup: true,
            enablePivot: true,
            enableValue: true,
            sortable: true,
            resizable: true,
            flex: 1,
            minWidth: 150,
        };
    }, []);
    const convertToPriceRenderer = memo(
        (props) => {
            return ConvertToPrice(props.value)
        }
    )
    const [columnDefs, setColumnDefs] = useState([

        {
            headerName: "",

            valueGetter: "node.rowIndex + 1", width: 60, editable: false, minWidth: 60,
        }, {
            headerName: 'عنوان', field: 'title',

        }, {
            headerName: 'مبلغ صورت حساب ها', field: 'amount',       cellRenderer: convertToPriceRenderer,


        }, {
            headerName: 'مبلغ تخفیف ها', field: 'discountAmount', cellRenderer: convertToPriceRenderer,


        },

        {
            headerName: 'مالیات بر ارزش افزوده', field: 'taxAmount', cellRenderer: convertToPriceRenderer,

        }, {
            headerName: 'سایر مالیات ها', field: 'otherTaxAmount',

        },


        {
            headerName: 'وجوه قانونی', field: 'legalAmount',

        },

        {
            headerName: 'تعداد صورت حساب ها', field: 'total',

        },


    ]);


    return (<div className=" max-w-7xl m-auto">

        <div className=' mb-4 defBox '>
            <h1 className={'font-bold text-base mb-5'}>آخرین فاکتور ارسال شده</h1>


            {!loading ? <>
                {!lastFactorNull ? <div className={' grid gap-4  grid-cols-1 sm:grid-cols-2   md:grid-cols-3 '}>
                    <KeyValue keyText={'تاریخ'} value={DateTimeString(lastFactor?.invoiceDate)}/>
                    <KeyValue keyText={'شماره'} value={lastFactor?.internalInvoiceNo}/>
                    <KeyValue keyText={'شناسه'} value={lastFactor?.taxInvoiceNo}/>
                </div> : <p className='text-red'>شما قبلا هیچ فاکتوری ارسال نکرده اید</p>}</> : <BusinessListLoading/>

            }
        </div>
        <div className=' mb-4 defBox '>


            <Form
                name="basic"
                layout="vertical"
                form={form}

                onFinish={handleGetInvoicesData}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <div className="flex  items-end">


                    <div className={'flex-1 grid gap-4  grid-cols-1 sm:grid-cols-2 '}>

                        {/*{!lastFactorNull && <Form.Item label='از تاریخ ' className={'input_wrap '}>*/}
                        {/*    <DateBox defValue={null} disable={true}/>*/}


                        {/*</Form.Item>}*/}


                        <Form.Item
                            rules={[{required: true, message: 'لطفا تاریخ را انتخاب کنید!'}]}
                            className={'input_wrap  ml-3 flex-1'}

                            label='تا تاریخ ' name={'ToDate'}>

                            <DateBox handleChangeDate={(value) => form.setFieldValue('ToDate', value)}/>
                        </Form.Item>


                    </div>


                    <Form.Item className={'input_wrap'}>
                        <Button className='defBtn submitBtn' type="primary" loading={loading2}
                                htmlType="submit">
                            بررسی صورت حساب‌ها
                        </Button>
                    </Form.Item>


                </div>
                {invoicesDataText &&
                    <p className={'text-red text-sm'}>تا تاریخ انتخاب شده هیچ صورت حسابی برای شما ثبت نشده!</p>}
            </Form>
        </div>

        {getInvoicesData?.total > 0 && <>
            <div className="defBox mb-5 ">
                <h1 className={'font-bold text-base mb-5'}>اطلاعات بازه زمانی انتخاب شده</h1>
                <div className={' '}>
                    <div className={'flex-1 grid gap-4  grid-cols-1 sm:grid-cols-2   lg:grid-cols-3 '}>
                        <KeyValue keyText={'تعداد صورت حساب ها'} value={getInvoicesData?.total}/>
                        <KeyValue keyText={'تعداد اقلام صورت حساب ها'} value={getInvoicesData?.detailTotal}/>
                        <KeyValue keyText={'مجموع تخفیف ها'}
                                  value={`${ConvertToPrice(getInvoicesData?.sumDiscount)} ریال`}/>
                        <KeyValue keyText={'مجموع مالیات بر ارزش افزوده'}
                                  value={`${ConvertToPrice(getInvoicesData?.sumTax)} ریال`}/>


                        <KeyValue keyText={'سایر مالیات وعوارض'} value={getInvoicesData?.sumOtherTax}/>
                        <KeyValue keyText={'سایر وجوه قانونی'} value={getInvoicesData?.sumLegalAmount}/>
                        <KeyValue keyText={'مجموع مبلغ صورت حساب ها   '}
                                  value={`${ConvertToPrice(getInvoicesData?.sumAmount)} ریال`}/>
                    </div>


                    <div className="mt-10 flex justify-end">
                        <SendInvoiceConfirm data={form.getFieldsValue()}/>

                    </div>

                </div>

            </div>

            <div className="h-full ag-theme-alpine defBox   CreateGrid">
                <AgGridReact Scrolling
                             localeText={localeText}
                             ref={gridRef}
                             rowData={data}
                             enableRtl

                             columnDefs={columnDefs}
                             defaultColDef={defaultColDef}

                >

                </AgGridReact>
            </div>

        </>}


    </div>);
};

export default SendInvoice;