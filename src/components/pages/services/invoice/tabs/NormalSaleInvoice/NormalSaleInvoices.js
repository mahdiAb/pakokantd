import React, {forwardRef, memo, useCallback, useEffect, useImperativeHandle, useMemo, useRef, useState,} from "react";

import {useNavigate, useParams} from "react-router-dom";
import BusinessRequest from "../../../../../helper/request/BusinessRequest";
import {
  CreateNormalSaleInvoice,
  GetAllCatalog,
  GetAllPerson,
  GetCatalogByID,
  GetInvoiceByID,
  GetPersonByID,
} from "../../../../../helper/apiList/apiListBusiness";
import {Button, Form, Input, Select} from "antd";
import {AgGridReact} from "ag-grid-react";
import {initialDataCreateInvoice, invoiceSettlementMethodIDJson,} from "../../../../../data/data";
import KeyValue from "../../../../../microComponents/common/keyValue/KeyValue";
import {ConvertPriceToNumber, ConvertToPrice,} from "../../../../../helper/ConvertToPrice";
import FormItem from "../../../../../microComponents/common/uikits/FormItem";
import {ShowNotification} from "../../../../../helper/ShowNotification";

import CreatePersonModal from "../../../setting/person/CreatePersonModal";
import {AG_GRID_LOCALE_FA} from "../../../../../helper/locale.fa";
import {FiTrash2} from "@react-icons/all-files/fi/FiTrash2";
import CreatCatalogModal from "../../../setting/Catalog/CreatCatalogModal";
import NormalSaleInvoices from "./NormalSaleInvoices";
import {connect} from "react-redux";
import {
  handleInvoiceActiveKey,
  handleInvoiceNewTabIndex,
  handleInvoiceTabList
} from "../../../../../redux/reducer/Reducer";
import dayjs from "dayjs";
import customParseFormat from 'dayjs/plugin/customParseFormat';
import DateBox from "../../../../../microComponents/common/uikits/DateBox";

dayjs.extend(customParseFormat);
const CreateInvoice = ({
                         handleInvoiceTabList, handleInvoiceActiveKey, handleInvoiceNewTabIndex,
                         tabListState, newTabIndex,
                         invoiceData,
                         pattern, showTitle,
                         showCancellationInvoice,
                         invoiceId,
                         invoiceTitle = 'ثبت صورت حساب',
                         showSubmit,
                         url = CreateNormalSaleInvoice,
                         editTable,
                         editPayment,
                         invoiceSubjectID,
                         editPerson,
                         editCatalog,
                         editDate,
                         addRow,
                         firstAddRow,
                       }) => {
  const [data, setData] = useState(initialDataCreateInvoice);
  const [invoiceSettlementMethodIDValue, setInvoiceSettlementMethodIDValue] =
      useState("1");
  const [catalogList, setCatalogList] = useState([]);
  const [personList, setPersonList] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState({});
  const navigate = useNavigate();
  const [form] = Form.useForm();
  console.log(personList)
  let [loading, setLoading] = useState(false);
  const [result, setResult] = useState(null);
  const [reGetPerson, setReGetPerson] = useState(false);
  const [addTab, setAddTab] = useState({})

  const gridRef = useRef();
  let {id} = useParams();

  let [loadingPerson, setLoadingPerson] = useState(false);
  let [loadingCatalog, setLoadingCatalog] = useState(true);
  let [loadingGetCatalogByID, setLoadingGetCatalogByID] = useState(false);
  const [ready, setReady] = useState(false);
  const [person, setPerson] = useState();
  const [invoiceDateValue, setInvoiceDateValue] = useState();
  const [personValue, setPersonValue] = useState();
  const [getPersonType, setGetPersonType] = useState("scroll");
  const [paramsPerson, setParamsPerson] = useState({
    PageNumber: "1",
    CountInPage: "10",
  });
  const [catalogSearch, setCatalogSearch] = useState();
  const [totalRecordPerson, setTotalRecordPerson] = useState();
  const [dataPerson, setDataPerson] = useState();
  const [searchValue, setSearchValue] = useState();
  useEffect(() => {

    if (addTab.id) {
      add(addTab)
    }
  }, [addTab])
  const add = (data) => {

    handleInvoiceNewTabIndex()
    const newPanes = [...tabListState];
    newPanes.push({
      label: data.label, children: data.children, id: data.id, reopen: true, key: newTabIndex,
    });
    handleInvoiceTabList(newPanes)
    handleInvoiceActiveKey(newTabIndex);

  };

  const getSum = (x) => {
    const rowData = [];
    gridRef?.current?.api?.forEachNode(function (node) {
      rowData.push(node.data);
    });

    let sum = rowData.reduce((data, object) => {
      return data + ConvertPriceToNumber(object[x]);
    }, 0);

    return isNaN(sum) ? 0 : ConvertToPrice(~~ConvertPriceToNumber(sum));
  };

  const [sumData, setSumData] = useState({});

  let sumFactorJason = [
    { title: "مبلغ قبل از تخفیف", value: sumData.amountBeforDiscount },
    { title: "مبلغ تخفیف", value: sumData.discountAmount },
    { title: "مبلغ مالیات بر ارزش افزوده", value: sumData.taxAmount },
    { title: "مبلغ سایر  مالیات و عوارض", value: sumData.otherTaxAmount },
    { title: "مبلغ سایر وجوه قانونی", value: sumData.legalAmount },
    { title: "مبلغ کل", value: sumData.amount },
  ];
  const convertToPriceRenderer = memo((props) => {
    console.log(props.value)
    return ConvertToPrice(~~ConvertPriceToNumber(props.value));
  });

  const onCellValueChanged = async (props) => {
    handleChangeValuesInTable(props);

    handleSum();
    form.setFieldValue("cashPaymentAmount", getSum("amount"));
    form.setFieldValue("loanPaymentAmount", 0);

    if (props.oldValue !== props.newValue && props.node.lastChild && addRow) {
      await addItems();
    }
  };

  const handleChangeValuesInTable = (props) => {
    props.data.amountBeforDiscount = ConvertToPrice(
      ConvertPriceToNumber(props.data.value) *
        ConvertPriceToNumber(props.data.unitAmount)
    );
    props.data.amountAfterDiscount = ConvertToPrice(
      ConvertPriceToNumber(props.data.value) *
        ConvertPriceToNumber(props.data.unitAmount) -
        ConvertPriceToNumber(props.data.discountAmount)
    );
    props.data.taxAmount = ConvertToPrice(
      ((ConvertPriceToNumber(props.data.value) *
        ConvertPriceToNumber(props.data.unitAmount) -
        ConvertPriceToNumber(props.data.discountAmount)) *
        ConvertPriceToNumber(props.data.taxRate)) /
        100
    );
    props.data.otherTaxAmount = ConvertToPrice(
      ((ConvertPriceToNumber(props.data.value) *
        ConvertPriceToNumber(props.data.unitAmount) -
        ConvertPriceToNumber(props.data.discountAmount)) *
        ConvertPriceToNumber(props.data.otherTaxRate)) /
        100
    );
    props.data.legalAmount = ConvertToPrice(
      ((ConvertPriceToNumber(props.data.value) *
        ConvertPriceToNumber(props.data.unitAmount) -
        ConvertPriceToNumber(props.data.discountAmount)) *
        ConvertPriceToNumber(props.data.legalAmountRate)) /
        100
    );
    props.data.amount = ConvertToPrice(
      ConvertPriceToNumber(props.data.value) *
        ConvertPriceToNumber(props.data.unitAmount) -
        ConvertPriceToNumber(props.data.discountAmount) +
        ConvertPriceToNumber(props.data.taxAmount) +
        ConvertPriceToNumber(props.data.otherTaxAmount) +
        ConvertPriceToNumber(props.data.legalAmount)
    );
    props.api.refreshCells({
      columns: [
        "unitAmount",
        "productDescription",
        "legalAmountRate",
        "otherTaxRate",
        "taxRate",
        "amountBeforDiscount",
        "amountAfterDiscount",
        "otherTaxAmount",
        "legalAmount",
        "taxAmount",
        "amount",
      ],
    });
  };
  const handleSum = () => {
    setSumData({
      amountBeforDiscount: getSum("amountBeforDiscount"),
      discountAmount: getSum("discountAmount"),
      taxAmount: getSum("taxAmount"),
      otherTaxAmount: getSum("otherTaxAmount"),
      legalAmount: getSum("legalAmount"),
      amount: getSum("amount"),
    });
  };
  useEffect(() => {
    handleSum();
  }, [data]);

  const addItems = useCallback((addIndex) => {
    const res = gridRef.current.api.applyTransaction({
      add: [
        {
          productID: null,
          productDescription: "",
          value: 1,
          unitAmount: 0,
          taxRate: 0,
          discountAmount: 0,
          amountBeforDiscount: 0,
          amountAfterDiscount: 0,
          otherTaxRate: 0,
          legalAmountRate: 0,
          amount: 0,
          taxAmount: 0,
          otherTaxAmount: 0,
          legalAmount: 0,
        },
      ],
      addIndex: addIndex,
    });
    printResult(res);
  }, []);

  const printResult = (res) => {
    if (res.add) {
      res.add.forEach(function (rowNode) {});
    }
  };

  const ProductIDRenderer = memo((props) => {
    if (props.value !== undefined) {
      const imageForMood = (mood) => {
        if (typeof props.value !== "undefined" && props.value !== null) {
          let data = catalogList.find((element) => element.id == props.value);
          setSelectedProduct(data);
          return data?.name;
        } else {
          return "";
        }
      };

      return <span>{imageForMood(props.value)}</span>;
    }
  });

  const ProductIDEditor = memo(
    forwardRef(
      (props, ref) => {
        const [value, setValue] = useState(null);

        const refContainer = useRef(null);

        useEffect(() => {
          if (value !== null) {
            props.stopEditing();
          }
        }, [value]);

        useImperativeHandle(ref, () => {
          return {
            getValue() {
              return value ?? props.value;
              // return value  ;
              // change
            },
          };
        });
        const onSearchSelect = (value, pId) => {
          let x = { ID: pId };
          BusinessRequest.init(
            GetCatalogByID,
            function (datas) {
              let data = datas.data;
              setLoadingGetCatalogByID(false);
              if (typeof pId != "undefined") {
                props.data.productID = pId;
                props.data.unitAmount = data.prices[0]
                    ? ConvertToPrice(data.prices[0].priceValue)
                    : 0;
                props.data.taxRate = data.taxRate;
                props.data.otherTaxRate = data.otherRate;
                props.data.legalAmountRate = data.legalRate;

                props.data.productDescription = data.productDescription ? data.productDescription : data.name;
                props.data.productTaxID = data.productTaxID;

                handleChangeValuesInTable(props);

                handleSum();
                form.setFieldValue("cashPaymentAmount", getSum("amount"));
                form.setFieldValue("loanPaymentAmount", 0);
                // addItems();
              } else {
                return "";
              }
            },
            function (error) {
              setLoadingGetCatalogByID(false);
            }
          )
            .setBID(id)
            .setMainData(x)
            .setRouter(navigate)
            .callRequest();
        };
        const onChange = (value) => {
          setValue(value);
          console.log("value", value);
          let x = {ID: value};

          BusinessRequest.init(GetCatalogByID, function (data) {
                setLoadingGetCatalogByID(false);
              },
              function (error) {
                setLoadingGetCatalogByID(false);
              }
          ).setBID(id).setMainData(x).setRouter(navigate).callRequest();

          if (typeof value != "undefined") {
            let data = catalogList.find((element) => element.id == value);
            props.data.unitAmount = data.prices[0]
                ? ConvertToPrice(data.prices[0].priceValue)
                : 0;
            props.data.taxRate = data.taxRate;
            props.data.otherTaxRate = data.otherRate;
            props.data.legalAmountRate = data.legalRate;
            props.data.productDescription = data.productDescription ?? data.name;
            props.data.productTaxID = data.productTaxID;
            handleChangeValuesInTable(props);
          } else {
            return "";
          }
        };
        const handleGetCatValueSearch = (values, id) => {
          // console.log(values, id);
          // gridRef.current.api.tabToNextCell();
          // gridRef.current.api.tabToNextCell();
          // gridRef.current.api.tabToNextCell();

          onSearchSelect(values, id);
          setCatalogSearch(id);
          setValue(id);
        };

        return (
          <div
            tabIndex={1}
            className={"input_wrap  SelectBtnFormItem w-full flex"}
          >
            <Select
              autoFocus
              disabled={!editCatalog}
              // defaultOpen={true}
              showSearch
              ref={refContainer}
              optionFilterProp="children"
              className={"selectAdd"}
              style={{ minWidth: "200px", right: "0" }}
              onChange={onChange}
              value={value}
              filterOption={(input, option) =>
                option?.props.label.indexOf(input) >= 0
              }
              onPopupScroll={onPopupScroll}
              onSearch={(value) => {
                // setCatalogList([]);
                // console.log("search key ", value);
              }}
              onSelect={(value) => {
                // console.log("select value", value);
              }}
            >
              {catalogList.map((item, index) => (
                <Select.Option
                  value={item.id}
                  key={index}
                  label={item.name + " " + item.code}
                >
                  <KeyValue value={item.name} keyText={"نام کالا"} />
                  <KeyValue value={item.code} keyText={"کد کالا"} />
                </Select.Option>
              ))}
            </Select>
            <CreatCatalogModal
              disabled={false}
              getValueSearch={handleGetCatValueSearch}
            />
          </div>
        );
      },
      [catalogList]
    )
  );

  const onRemoveSelected = useCallback((props) => {
    console.log(props.data.productID == null);
    if (props.data.productID !== null) {
      let numberOfRow = gridRef.current.api.rowRenderer.allRowCtrls.length;
      gridRef.current.api.applyTransaction({remove: [props.data]});
      if (numberOfRow < 2) {
        addItems();
      }
    }

    form.setFieldValue("cashPaymentAmount", getSum("amount"));
    form.setFieldValue("loanPaymentAmount", 0);

    handleChangeValuesInTable(props);

    handleSum();
  }, []);
  const BtnCellRenderer = (props) => {
    return (
        <>

          <div className="flex items-center h-full mt-1">
            <Button
                disabled={!editTable}
                className="iconBtn red"
                onClick={() => {
                  onRemoveSelected(props);
                }}
            >
              <FiTrash2 size={18}/>
            </Button>
          </div>
        </>
    );
  };
  const [columnDefs, setColumnDefs] = useState([
    {
      headerName: "",
      valueGetter: "node.rowIndex + 1",

      editable: false,
      maxWidth: 60,
      minWidth: 45,
      width: 60,
    },
    {
      headerName: "نام کالا/خدمات",
      field: "productID",
      popupPosition: "under",
      cellEditorPopup: true,
      popup: true,
      editable: editCatalog,
      minWidth: 200,
      cellEditor: ProductIDEditor,
      onCellValueChanged: onCellValueChanged,
    },
    {
      headerName: "توضیحات کالا/خدمات",
      minWidth: 160,
      width: 160,
      field: "productDescription",
    },
    {
      headerName: "شناسه کالا/خدمات",
      minWidth: 130,
      width: 130,
      field: "productTaxID",
    },
    {
      headerName: "تعداد/ مقدار",
      field: "value",
      minWidth: 110,
      width: 110,
      cellClassRules: {"bg-red-100": "x < 1"},
      onCellValueChanged: onCellValueChanged,
    },
    {
      headerName: "مبلغ واحد",
      field: "unitAmount",
      cellRenderer: convertToPriceRenderer,
      cellClassRules: {"bg-red-100": "x < 1"},
      onCellValueChanged: onCellValueChanged,
      minWidth: 110,
      width: 110,
    },


    {
      headerName: "مبلغ قبل از تخفیف",
      minWidth: 140,
      width: 140,
      field: "amountBeforDiscount",
      editable: false,
      cellRenderer: convertToPriceRenderer,

      hide: ![1, 2, 3, 4, 5].includes(pattern),
    },
    {
      headerName: "مبلغ تخفیف",
      minWidth: 110,
      width: 110,
      field: "discountAmount",
      cellRenderer: convertToPriceRenderer,
      hide: ![1, 2, 3, 4, 5].includes(pattern),
      onCellValueChanged: onCellValueChanged,
    },
    {
      headerName: "مبلغ بعد از تخفیف",
      minWidth: 140,
      width: 140,
      field: "amountAfterDiscount",
      editable: false,
      cellRenderer: convertToPriceRenderer,

      hide: ![1, 2, 3, 4, 5].includes(pattern),
    },
    {
      headerName: "نرخ مالیات بر ارزش افزوده",
      field: "taxRate",
      minWidth: 160,
      width: 160,
      onCellValueChanged: onCellValueChanged,
    },
    {
      headerName: "مبلغ مالیات بر ارزش افزوده",
      cellRenderer: convertToPriceRenderer,
      minWidth: 170,
      width: 170,
      field: "taxAmount",
      editable: false,
    },
    {
      headerName: "موضوع سایر  مالیات و عوارض",
      minWidth: 180,
      width: 180,
      field: "otherTaxSubject",
      hide: ![1, 2, 3, 4, 5, 7].includes(pattern),
    },
    {
      headerName: "نرخ سایر  مالیات و عوارض",
      minWidth: 160,
      width: 160,
      field: "otherTaxRate",
      hide: ![1, 2, 3, 4, 5, 7].includes(pattern),
      onCellValueChanged: onCellValueChanged,
    },
    {
      headerName: "مبلغ سایر  مالیات و عوارض",
      minWidth: 160,
      width: 160,
      field: "otherTaxAmount",
      cellRenderer: convertToPriceRenderer,

      hide: ![1, 2, 3, 4, 5, 7].includes(pattern),
      editable: false,
    },

    {
      headerName: "موضوع سایر  وجوه قانونی",
      minWidth: 160,
      width: 160,
      field: "legalAmountSubject",
      hide: ![1, 2, 3, 4, 5, 7].includes(pattern),
    },
    {
      headerName: "نرخ سایر  وجوه قانونی",
      minWidth: 160,
      width: 160,
      field: "legalAmountRate",
      hide: ![1, 2, 3, 4, 5, 7].includes(pattern),
      onCellValueChanged: onCellValueChanged,
    },
    {
      headerName: "مبلغ سایر وجوه قانونی",
      minWidth: 160,
      width: 160,
      field: "legalAmount",
      cellRenderer: convertToPriceRenderer,

      hide: ![1, 2, 3, 4, 5, 7].includes(pattern),
      editable: false,
    },

    {
      headerName: "مبلغ کل کالا/ خدمات",
      field: "amount",
      cellRenderer: convertToPriceRenderer,
      editable: false,
      minWidth: 160,
      width: 160,
    },

    {
      headerName: "شناسه یکتای ثبت قرارداد حق العمل کاری",
      field: "brokerContractID",
      minWidth: 230,
      width: 230,
    },

    {
      headerName: "میزان ارز",
      field: "currencyAmount",
      hide: ![2, 3, 4, 6].includes(pattern),
      minWidth: 160,
      width: 160,
    },
    {
      headerName: "نوع ارز",
      field: "currencyTypeID",
      hide: ![2, 3, 4, 6, 7].includes(pattern),
      minWidth: 160,
      width: 160,
    },

    {
      headerName: "واحد اندازه گیری",
      minWidth: 160,
      width: 160,
      field: "unitOfMeasurement",
      hide: ![2, 3, 4, 5, 6, 7].includes(pattern),
    },
    {
      headerName: "نرخ برابری ارز با ریال",
      minWidth: 160,
      width: 160,
      field: "exchangeRate",
      hide: ![2, 3, 4, 6, 7].includes(pattern),
    },
    {
      headerName: "سهم نقدی از پرداخت",
      minWidth: 160,
      width: 160,
      field: "shareCashPayment",
      hide: ![2, 3, 4].includes(pattern),
    },
    {
      headerName: "سهم مالیات بر ارزش افزوده از پرداخت",
      field: "shareTaxPayment",
      minWidth: 160,
      width: 160,
      hide: ![2, 3, 4].includes(pattern),
    },
    {
      headerName: "اجرت ساخت",
      minWidth: 160,
      width: 160,
      valueGetter: "Number(data.currencyAmount) + Number(data.currencyTypeID)",
      cellClass: "total-col",
      hide: ![3].includes(pattern),
    },
    {
      headerName: "سود فروشنده",
      minWidth: 160,
      width: 160,
      field: "sellerProfit",
      hide: ![3].includes(pattern),
    },
    {
      headerName: "حق العمل",
      minWidth: 160,
      width: 160,
      field: "brokerAmount",
      hide: ![3].includes(pattern),
    },
    {
      headerName: "جمع کل اجرت، حق العمل و سود",
      minWidth: 160,
      width: 160,
      field: "sumConsProfitBroker",
      hide: ![3].includes(pattern),
    },
    {
      headerName: "وزن خالص",
      minWidth: 160,
      width: 160,
      field: "NetWeight",
      hide: ![7].includes(pattern),
    },
    {
      headerName: "ارزش ریالی کالا",
      minWidth: 160,
      width: 160,
      field: "x",
      hide: ![7].includes(pattern),
    },
    {
      headerName: "ارزش ارزی کالا",
      minWidth: 160,
      width: 160,
      field: "x",
      hide: ![7].includes(pattern),
    },
    {
      headerName: "حذف",

      field: "delete",
      editable: false,
      pinned: "left",
      width: 100,
      minWidth: 100,

      cellRenderer: BtnCellRenderer,
    },
  ]);

  useEffect(() => {
    let x = {
      PageNumber: "1",
      CountInPage: "1000000000",
    };
    BusinessRequest.init(
      GetAllCatalog,
      function (data) {
        setCatalogList(data.data.list);
        setLoadingCatalog(false);
      },
      function (error) {
        setLoadingCatalog(false);
      }
    )
      .setBID(id)
      .setRouter(navigate)
      .setMainData(x)
      .callRequest();
  }, [catalogSearch]);

  useEffect(() => {
    form.setFieldValue("personID", person);
  }, [personList, person]);

  useEffect(() => {
    if (catalogList.length > 0) {
      let newColumnDefs = [...columnDefs];
      let index = newColumnDefs.findIndex(
        (element) => element.field == "productID"
      );
      if (index > -1) {
        newColumnDefs[index].cellRenderer = ProductIDRenderer;
        newColumnDefs[index].cellEditor = ProductIDEditor;

        setColumnDefs(newColumnDefs);
      }
    }
  }, [catalogList]);

  const onBtStartEditing = useCallback((index, field, pinned) => {
    gridRef.current.api.setFocusedCell(index, field, pinned);
    gridRef.current.api.startEditingCell({
      rowIndex: index,
      colKey: field,
      rowPinned: pinned,
      key: index,
    });
  }, []);

  const onCellKeyDown = useCallback((params) => {
    if (params?.event?.key === "Enter") {
      gridRef.current.api.tabToNextCell();
    }
    if (ready) {
      switch (params.event.key) {
        case "ArrowRight":
          gridRef.current.api.tabToPreviousCell();
          return false;
        case "ArrowLeft":
          gridRef.current.api.tabToNextCell();
          return false;
        case "ArrowUp":
          if (gridRef.current.props.rowData.length)
            onBtStartEditing(params.rowIndex - 1, params.colDef.field);

          return false;
        case "ArrowDown":
          onBtStartEditing(params.rowIndex + 1, params.colDef.field);
          return false;
      }
    }
  });

  const onFinishFailed = (errorInfo) => {
    onBtStopEditing();
    console.log(777)
    console.log(form.getFieldValue('personID'))
  };

  const defaultColDef = useMemo(() => {
    return {
      flex: 1,

      sortable: false,
      editable: editTable,
      resizable: true,
    };
  }, []);

  const onBtStopEditing = useCallback(() => {
    gridRef.current.api.stopEditing();
  }, []);


  const handleInvoice = (values) => {
    onBtStopEditing();
    console.log(values)
    if (showCancellationInvoice) {
      values.RefInvoiceID = invoiceId;
    }
    if (values.invoiceSettlementMethodID == 3) {
      values.cashPaymentAmount = ConvertPriceToNumber(values.cashPaymentAmount)
      values.loanPaymentAmount = ConvertPriceToNumber(values.loanPaymentAmount)
    }
    let rowData = [];
    gridRef.current.api.forEachNode((node) => rowData.push(node.data));

    values.RefInvoiceID = invoiceId;
    values.InvoiceDetails = JSON.parse(JSON.stringify(rowData));
    if (invoiceSubjectID) {
      values.InvoiceSubjectID = invoiceSubjectID;
    }

    values.InvoiceDetails.map((item, index) => {
      delete item.taxAmount;
      delete item.otherTaxAmount;
      delete item.legalAmount;
      delete item.amount;
      delete item.amountAfterDiscount;
      delete item.amountBeforDiscount;
      item.unitAmount = ConvertPriceToNumber(item.unitAmount);
      if (item.productID == null) {
        values.InvoiceDetails.splice(index, 1);
      }
    });

    setLoading(true);


    console.log(values)
    BusinessRequest.init(
        url,
        function (data) {
          setLoading(false);
          if (data.data.messageCode === 0) {
            resetTable();
            form.setFieldValue("cashPaymentAmount", getSum("amount"));
            form.setFieldValue("loanPaymentAmount", 0);


            handleSum();
            ShowNotification(
                data.data.message ?? " صورت حساب با موفقیت ثبت شد",
                true
            );
          } else {
            ShowNotification(data.data.message, false);
          }
        },
        function (error) {
          setLoading(false);
        }
    )
      .setBID(id)
      .setRouter(navigate)
      .setMainData(values)
      .callRequest();
  };

  const resetTable = () => {
    form.resetFields();
    setDataPerson();
    gridRef?.current?.api?.setRowData([
      {
        productID: null,

        productDescription: "",
        value: 1,
        unitAmount: 0,
        taxRate: 0,
        discountAmount: 0,
        amountBeforDiscount: 0,
        amountAfterDiscount: 0,
        otherTaxRate: 0,
        legalAmountRate: 0,
        amount: 0,
        taxAmount: 0,
        otherTaxAmount: 0,
        legalAmount: 0,
      },
    ]);
  };
  useEffect(() => {
    if (gridRef?.current) {
      setData([
        {
          productID: null,
          productDescription: "",
          value: 1,
          unitAmount: 0,
          taxRate: 0,
          discountAmount: 0,
          amountBeforDiscount: 0,
          amountAfterDiscount: 0,
          otherTaxRate: 0,
          legalAmountRate: 0,
          amount: 0,
          taxAmount: 0,
          otherTaxAmount: 0,
          legalAmount: 0,
        },
      ]);
    }
  }, []);

  const changePaymentAmount = (name, values) => {
    let x = ConvertPriceToNumber(getSum("amount")) - values;
    form.setFieldValue(name, ConvertToPrice(x));
  };

  const onGridReady = () => {
    if (invoiceId) {
      gridRef.current.api.showLoadingOverlay();

      let value = {
        ID: invoiceId,
      };
      BusinessRequest.init(
        GetInvoiceByID,
        function (res) {
          gridRef.current.api.hideOverlay();
          if (res.data.messageCode === 0) {
            setResult(res.data.result);
            if (firstAddRow) {
              setData([...res.data.result.invoiceDetails, ...data]);
            } else {
              setData(res.data.result.invoiceDetails);
            }

            form.setFieldValue(
              "invoiceSettlementMethodID",
              String(res.data.result.invoiceSettlementMethodID)
            );
            form.setFieldValue(
              "cashPaymentAmount",
                ConvertToPrice(res.data.result.cashPaymentAmount)
            );
            form.setFieldValue(
                "loanPaymentAmount",
                ConvertToPrice(res.data.result.loanPaymentAmount)
            );
            form.setFieldValue(
                "personID",
                ConvertToPrice(res.data.result.personID)
            );
            setInvoiceDateValue(res?.data?.result?.invoiceDate)


            setPerson(res.data.result.personID);

            let PersonByID = {ID: res.data.result.personID};

            BusinessRequest.init(
                GetPersonByID,
                function (response) {
                  setLoading(false);

                  if (response.data.messageCode == 0) {
                    value = response.data.result;
                    value.userFullName = value.firstName
                        ? value.firstName + " " + value.lastName
                        : value.lastName;
                    setPersonList([value]);
                    // setPersonList([response.data.result]);

                    setSearchValue(value.userFullName);
                    form.setFieldValue("personID", value.personID);

                    setDataPerson(value);
                    // setDataPerson(response.data.result);

                    handleGetValueSearch(value, value.id);
                    // handleGetValueSearch(
                    //   response.data.result.firstName
                    //     ? response.data.result.firstName +
                    //         " " +
                    //         response.data.result.lastName
                    //     : response.data.result.lastName
                    // );
                  } else {
                    ShowNotification(response.data.message, false);
                  }
              },
              function (error) {
                gridRef.current.api.hideOverlay();

                setLoading(false);
              }
            )
              .setBID(id)
              .setRouter(navigate)
              .setMainData(PersonByID)
              .callRequest();
          } else {
          }
        },
        function (error) {
          gridRef.current.api.hideOverlay();

          setLoading(false);
        }
      )
        .setBID(id)
        .setRouter(navigate)
        .setMainData(value)
        .callRequest();
    }
  };
  const pageNumberValuePerson = (value) => {
    setParamsPerson((prev) => ({
      ...prev,
      PageNumber: String(Number(prev.PageNumber) + 1),
    }));
  };
  const onPopupScroll = (e) => {
    e.persist();
    let target = e.target;
    if (target.scrollTop + target.offsetHeight === target.scrollHeight) {
      if (
        personList.length < totalRecordPerson &&
        personList.length !== totalRecordPerson
      ) {
        setGetPersonType("scroll");
        pageNumberValuePerson();
      }
    }
  };

  const handleGetValueSearch = (value, id) => {
    console.log(value, id)
    setPersonList([value])
    setPerson(id)
    form.setFieldValue("personID", id);
    selectPerson(id, value);
    value.id = id;
    setGetPersonType("search");
    value.userFullName = value.firstName
        ? value.firstName + " " + value.lastName
        : value.lastName;
    setSearchValue(value.userFullName);
    setDataPerson(value);
  };

  useEffect(() => {
    setLoadingPerson(true);

    BusinessRequest.init(
      GetAllPerson,
      function (data) {
        if (data.data.messageCode === 0) {
        } else {
          ShowNotification(data.data.message, false);
        }

        setLoadingPerson(false);
        let newArray = [];
        data.data.result.list.map((item, index) => {
          newArray[index] = {
            ...item,
            userFullName: item.firstName
              ? item.firstName + " " + item.lastName
              : item.lastName,
          };
        });
        setTotalRecordPerson(data.data.result.totalRecord);

        if (getPersonType === "scroll") {
          setPersonList([...personList, ...newArray]);
        } else {
          setPersonList([...newArray]);
        }
      },
        function (error) {
          setLoadingPerson(false);
        }
    )
        .setBID(id)
        .setRouter(navigate)
        .setMainData(paramsPerson)
        .callRequest();
  }, [paramsPerson, reGetPerson]);

  const selectPerson = (e, x) => {
    setPerson(e)
    console.log(e, x)
    setDataPerson(x);
    setPersonValue(e);
    form.setFieldValue("personID", e);

  };

  const localeText = useMemo(() => {
    return AG_GRID_LOCALE_FA;
  }, []);
  return (
      <>
        {
            showTitle &&
            <div className="defBox mb-3">
              <div className="flex justify-between flex-wrap   cursor-pointer hover:text-blue"
                   onClick={() => setAddTab({
                     label: 'نمایش صورت حساب',
                     id: 'ShowInvoice',
                     children: <NormalSaleInvoices invoiceData={invoiceData}
                                                   invoiceTitle={'نمایش  صورت حساب'}
                                                   invoiceId={invoiceId} pattern={1} editTable={false}
                                                   showSubmit={false}
                                                   editPayment={false}
                                                   editPerson={false} editCatalog={false} editDate={false}
                                                   addRow={false}
                                                   firstAddRow={false}/>,
                   })}
              >

                <h2 className='font-bold font-medium my-4'>{invoiceTitle}</h2>
                {invoiceData && <>
                  <KeyValue
                      value={invoiceData.internalInvoiceNo}
                      className={"my-4 mx-0 "}
                      keyText={"سریال داخلی"}
                      keyClass={" themText2 ml-12"}
                  />

                  <KeyValue
                      value={invoiceData.taxInvoiceNo}
                      className={"my-4 mx-0 "}
                      keyText={"سریال مالیاتی"}
                      keyClass={" themText2 ml-12"}
                  />


                </>
                }
              </div>
            </div>
        }
        <Form
            name="basic"
            layout="vertical"
            form={form}
            initialValues={{
              invoiceDate: "",
              personID: "",
              invoiceSettlementMethodID: "1",
            }}
            onFinish={handleInvoice}
            onFinishFailed={(x) => {
              console.log(form.getFieldValue('personID'))
              onFinishFailed(x)
            }}
            autoComplete="off"
        >
          <div className="defBox   mb-3">
            <div className="grid gap-4    grid-cols-1 lg:grid-cols-5">
              <Form.Item
                  className={"input_wrap  SelectBtnFormItem w-full lg:col-span-2"}
                  rules={[
                    {required: true, message: "لطفا شخص خریدار را وارد کنید!"},
                  ]}
                  label="شخص خریدار"
                  name={"personID"}
                  hidden={![1].includes(pattern)}
              >
                <Select allowClear
                        defaultValue={dataPerson?.id}
                        showSearch
                        loading={loadingPerson}
                        onClear={() => {
                          if (paramsPerson == {
                            PageNumber: "1",
                            CountInPage: "10",
                          }) {
                            setReGetPerson(prevState => (!prevState))
                          } else {
                            setParamsPerson({
                              PageNumber: "1",
                              CountInPage: "10",
                            })
                          }


                        }}
                        className={"selectAdd"}
                        disabled={!editPerson}
                        onPopupScroll={onPopupScroll}
                        onSearch={(value) => {
                          setSearchValue(value);
                          setGetPersonType("search");
                          setParamsPerson({
                            ...paramsPerson,
                            PageNumber: "1",
                            contains_FullSearch: value,
                          });
                        }}
                        filterOption={(input, option) =>
                            (option?.userFullName ?? "")
                                .toLowerCase()
                                .includes(input.toLowerCase())
                        }
                    // searchValue={searchValue}
                        onChange={(e, x) => {
                          selectPerson(e, x);
                        }}
                        value={person}
                        fieldNames={{label: "userFullName", value: "id"}}
                        options={personList}
                />
                <CreatePersonModal
                    disabled={!editPerson}
                    getValueSearch={handleGetValueSearch}
                />
              </Form.Item>
              <div
                  className={
                    "grid gap-4 w-full sm:grid-cols-2  md:grid-cols-3 lg:grid-cols-3  col-span-3"
                  }
              >
                <KeyValue
                    value={dataPerson && dataPerson?.userFullName}
                    className={"my-4 mx-0 "}
                    keyText={"نام کاربر"}
                    keyClass={" themText2 ml-12"}
                />

                <KeyValue
                    value={dataPerson?.phoneNumber}
                    className={"my-4 mx-0 "}
                    keyText={"شماره تماس کاربر"}
                    keyClass={" themText2 ml-12"}
                />
                <KeyValue
                    value={dataPerson?.nationalID}
                    className={"my-4 mx-0 "}
                    keyText={"کد ملی"}
                    keyClass={" themText2 ml-12"}
                />
              </div>
          </div>
        </div>
        <div className="defBox   mb-3">
          <div className="grid gap-4 w-full md:grid-cols-2 lg:grid-cols-3 ">
            <Form.Item
                rules={[
                  {required: true, message: "لطفا تاریخ ثبت را انتخاب کنید!"},
                ]}
                className={"input_wrap"}
                label="تاریخ ثبت"
                name={"invoiceDate"}
                hidden={![1].includes(pattern)}
            >


              <DateBox defValue={invoiceDateValue} handleChangeDate={(e) => {

                let ddd = new Date(e)
                 let bbb = new Date(ddd)
                console.log(bbb)

                form.setFieldValue('invoiceDate', bbb)
              }}/>

            </Form.Item>
          </div>
        </div>

        <div className="h-full">
          <div className="h-full ag-theme-alpine defBox CreateGrid">
            <AgGridReact
              Scrolling
              enableCellTextSelection={true}
              ref={gridRef}
              onCellEditingStarted={() => {
                setReady(true);
              }}
              onCellEditingStopped={() => {
                setReady(false);
              }}
              singleClickEdit={true}
              onGridReady={onGridReady}
              onCellKeyDown={onCellKeyDown}
              rowData={data}
              enableRtl
              localeText={localeText}
              columnDefs={columnDefs}
              defaultColDef={defaultColDef}
            > </AgGridReact>
          </div>
        </div>

        <div className="lg:flex justify-between py-3 w-full">
          <div className="flex-1 defBox lg:ml-3 mb-3   ">
            <div className="flex items-end">
              <div className="flex-1 lg:ml-3 mb-3    grid gap-3    grid-cols-1 md:grid-cols-3">
                {/*<div className="flex-1 items-end   lg:ml-3 mb-3 md:flex justify-between w-full">*/}
                <FormItem
                  className={"input_wrap flex-1"}
                  name={"invoiceSettlementMethodID"}
                  title="نوع پرداخت"
                >
                  <Select
                    value={invoiceSettlementMethodIDValue}
                    disabled={!editPayment}
                    onChange={(e) => setInvoiceSettlementMethodIDValue(e)}
                    options={invoiceSettlementMethodIDJson}
                  />
                </FormItem>
                {form.getFieldValue("invoiceSettlementMethodID") == "3" && (
                  <>
                    <Form.Item
                      className="input_wrap flex-1"
                      name={"cashPaymentAmount"}
                      label={"پرداخت نقدی"}
                      rules={[
                        {
                          required: true,
                          message: "لطفا پرداخت نقدی را وارد کنید!",
                        },
                      ]}
                    >
                      <Input
                        className="defInput"
                        disabled={!editPayment}
                        onChange={(e) =>
                          changePaymentAmount(
                            "loanPaymentAmount",
                            e.target.value
                          )
                        }
                      />
                    </Form.Item>
                    <Form.Item
                      className="input_wrap flex-1"
                      name={"loanPaymentAmount"}
                      label={"پرداخت نسیه"}
                      rules={[
                        {
                          required: true,
                          message: "لطفا پرداخت نسیه را وارد کنید!",
                        },
                      ]}
                    >
                      <Input
                        className="defInput"
                        disabled={!editPayment}
                        onChange={(e) =>
                          changePaymentAmount(
                            "cashPaymentAmount",
                            e.target.value
                          )
                        }
                      />
                    </Form.Item>
                  </>
                )}
              </div>

              <Form.Item className={"flex justify-end mb-0 md:mb-10"}>
                <Button
                  className="defBtn submitBtn "
                  loading={loading}
                  type="primary"
                  disabled={!showSubmit}
                  htmlType="submit"
                >
                  {showCancellationInvoice ? "ثبت صورت حساب ابطالی" : "ثبت"}
                </Button>
              </Form.Item>
            </div>
          </div>

          <div className=" flex  defBox justify-end mb-3">
            <div className="  w-full  ">
              {sumFactorJason.length > 0 &&
                sumFactorJason.map((item, index) => (
                  <KeyValue
                      key={index}
                      value={item.value + " تومان "}
                      className={
                        "my-4 mx-0 w-full justify-between border-b pb-3 border-black-200"
                      }
                      keyText={item.title}
                      keyClass={" themText2 !ml-12"}
                  />
                ))}
            </div>
          </div>
        </div>
        </Form>
      </>
  );
};

function mapStateToProps(state) {
  return {
    tabListState: state.invoiceTabList,
    newTabIndex: state.invoiceNewTabIndex,
    activeKey: state.invoiceActiveKey,
    newTab: state.invoiceNewTab,
  }
}


export default connect(mapStateToProps, {
  handleInvoiceTabList,
  handleInvoiceActiveKey,
  handleInvoiceNewTabIndex
})(CreateInvoice);

