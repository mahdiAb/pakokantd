import {Button, Modal} from 'antd';
import React, {useState} from 'react';
import {useNavigate, useParams} from "react-router-dom";
import {Logout} from "../../helper/apiList/apiListBusiness";
import {ShowNotification} from "../../helper/ShowNotification";
import Cookies from 'js-cookie'
import Request from "../../helper/request/Request";

const ExitUser = () => {
    const [open, setOpen] = useState(false);
    const [loading, setLoading] = useState(false);
    let {id} = useParams();
    const navigate = useNavigate();
    const showModal = () => {
        setOpen(true);
    };

    const handleOk = () => {


        let data = {
            RefreshToken: Cookies.get('RefreshToken')
        }

        // let cookiesArray = document.cookie.split(';')


        // var Cookies = document.cookie.split(';');

        // set 1 Jan, 1970 expiry for every cookies
        // for (var i = 0; i < Cookies.length; i++) {
        //     document.cookie = Cookies[i] + "=;expires=" + new Date(0).toUTCString();
        //
        // }


        // cookiesArray.map((item, indexs) => {
        //
        // Cookies.remove(item.substr(0, item.indexOf("=")))
        // })
        // const removeCookies = (i) => {
        //     // Cookies.remove(cookiesArray[i].substr(0, cookiesArray[i].indexOf("=")))
        // }
        // for (var i = 0; i < Cookies.length; i++) {
        //     document.cookie = Cookies[i] + "=; expires="+ new Date(0).toUTCString();
        // }


        setLoading(true)
        Request.init(Logout, function (data) {

            setLoading(false)
            Cookies.remove('Token')
            Cookies.remove('RefreshToken')
            Cookies.remove(`BusinessToken_${id}`)
            Cookies.remove(`BusinessRefreshToken_${id}`)
            navigate('/login')

        }, function (error) {
            ShowNotification({type: false, description: 'خروج از حساب کاربری کامل نشد. دوباره امتحان کنید!',})
            setLoading(false)

        }).setRouter(navigate).setMainData(data).callRequest()


    };
    const handleCancel = () => {
        setOpen(false);
    };


    return (<>


        <div onClick={showModal}>خروج از حساب کاربری</div>


        <Modal
            width={800}
            open={open}
            title={'خروج از حساب کاربری'}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[<Button key="exit"  className='borderBtn defBtn  ' type="primary"
                             onClick={handleCancel}>
                انصراف
            </Button>, <Button loading={loading} key="submit" className='submitBtn defBtn' type="primary" onClick={handleOk}>
                خروج
            </Button>

            ]}>
            <div>

                <p className='font-medium text-sm'>آیا از خروج خود اطمینان دارید؟</p>

            </div>
        </Modal>
    </>);
};
export default ExitUser;