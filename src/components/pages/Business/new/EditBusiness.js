import React, {useEffect, useState} from 'react';

import Layout from "../../../microComponents/common/Layout/Layout";
import {
    GetBusinessByBusinessIDForExtension,
    GetBusinessToken,
    UpdateBusiness
} from "../../../helper/apiList/apiListBusiness";
import Request from "../../../helper/request/Request";
import {NavLink, useNavigate, useParams} from "react-router-dom";
import FormItem from "../../../microComponents/common/uikits/FormItem";
import TextInput from "../../../microComponents/common/uikits/TextInput";
import {Button, Form} from "antd";
import {mobile, number10, number14} from "../../../helper/validation/Validation";
import GetAddress from "../../../microComponents/common/GetAddress/GetAddress";
import Cookies from "js-cookie";
import BusinessRequest from "../../../helper/request/BusinessRequest";
import {ShowNotification} from "../../../helper/ShowNotification";
import TextAreaBox from "../../../microComponents/common/uikits/TextAreaBox";


const EditBusiness = () => {
    const [getLoading, setGetLoading] = useState(true);
    const navigate = useNavigate();
    const params = useParams();
    const [form] = Form.useForm();
    const [UpdateLoading, setUpdateLoading] = useState(false);


    // let {id} = useParams();
    useEffect(() => {


        let data = {
            BusinessID: params.id
        }
        Request.init(GetBusinessByBusinessIDForExtension, function (data) {
            form.setFieldsValue(data.data)
            setGetLoading(false)
        }, function (error) {
            console.log(error)
            setGetLoading(false)
        }).setMainData(data).setRouter(navigate).callRequest()


        Request.init(GetBusinessToken, async function (res) {
            await Cookies.set(`BusinessToken_${params.id}`, res.data.jwtToken, {expires: 7})
            await Cookies.set(`BusinessRefreshToken_${params.id}`, res.data.refreshToken, {expires: 7})


        }, function (error) {


        }).setRouter(navigate).setMainData(data).callRequest()


    }, [form,navigate]);


    const handleEditBusiness = (values) => {

        setUpdateLoading(true)
        BusinessRequest.init(UpdateBusiness, function (data) {

            if (data.data.messageID === 0) {
                ShowNotification(data.data.message, true)

            } else {
                ShowNotification(data.data.message, false)

            }
            setUpdateLoading(false)
        }, function (error) {
            console.log(error)
            setUpdateLoading(false)
        }).setBID(params.id).setRouter(navigate).setMainData(values).callRequest()


    }
    const onFinishFailed = (errorInfo) => {

    };

    return (
        <Layout>
            <Form className={'defBox !px-10 !py-8'}
                form={form}
                name="basic"
                layout="vertical"
                onFinish={handleEditBusiness}
                onFinishFailed={onFinishFailed}
                autoComplete="0n"
            >
                <h1 className="font-bold text-base mb-10">ویرایش کسب و کار</h1>

                <div
                    className="grid gap-x-6 gap-y-4   md:grid-cols-2 lg:grid-cols-3  ">


                    <FormItem title='نام  کسب و کار' name={'businessName'}>
                        <TextInput/>
                    </FormItem>


                    <FormItem title='تلفن' name={'phoneNumber'}>
                        <TextInput type={'number'}/>
                    </FormItem>

                    <FormItem title='شماره موبایل' name={'mobileNumber'} pattern={mobile}>
                        <TextInput/>
                    </FormItem>


                    <FormItem title='کد پستی' name={'postalCode'} pattern={number10}>


                        <TextInput/>
                    </FormItem>
                    <FormItem title={'کد ملی'} name={'nationalCode'}
                              pattern={number10}>


                        <TextInput/>
                    </FormItem>

                    <FormItem title='شماره ثبت' name={'registrationNumber'}  >


                        <TextInput/>
                    </FormItem>
                    <GetAddress StateApi={''} cityApi={''}/>


                    <FormItem title='شماره اقتصادی' name={'economicalNumber'} pattern={number14}>


                        <TextInput type={'number'}/>
                    </FormItem>
                    <FormItem title='شناسه یکتای حافظه مالیاتی' name={'businessTaxID'}  >


                        <TextInput type={'number'}/>
                    </FormItem>


                    <FormItem title='کلید خصوصی' name={'privateKey'}>
                        <TextAreaBox rows={3}/></FormItem>


                </div>
                <div className="flex justify-end">


                    <NavLink to={`/`} className='defBtn borderBlue ml-2 '>
                        بازگشت
                    </NavLink>
                    <Button key="submit " loading={UpdateLoading} className='defBtn submitBtn ' htmlType="submit">
                        ثبت
                    </Button>
                </div>
            </Form>

        </Layout>);
};
export default EditBusiness;


