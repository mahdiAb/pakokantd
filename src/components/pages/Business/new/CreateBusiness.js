import React, {useEffect, useState} from 'react';

import Layout from "../../../microComponents/common/Layout/Layout";
import {GetBusinessTypeList} from "../../../helper/apiList/apiListBusiness";
import Request from "../../../helper/request/Request";
import PurchaseService from "./PurchaseService";
import {useNavigate} from "react-router-dom";


const CreateBusiness = () => {
    let [BusinessTypeList, setBusinessTypeList] = useState()
    const [getLoading, setGetLoading] = useState(true);
    const navigate = useNavigate();


    useEffect(() => {

        (async () => {
            setGetLoading(true)
            await Request.init(GetBusinessTypeList, function (data) {

                setBusinessTypeList(data.data)
                setGetLoading(false)
            }, function (error) {
                console.log(error)
                setGetLoading(false)
            }).setRouter(navigate).callRequest()


        })();
    }, [navigate]);


    return (
        <Layout>
            {
                BusinessTypeList &&
                <PurchaseService BusinessTypeList={BusinessTypeList} BusinessData={{maxNumberOfUser: '3'}} create={true}/>

            }

        </Layout>);
};
export default CreateBusiness;


