import {Button, Checkbox, Modal, Switch} from 'antd';
import React, {useState} from 'react';
import {AiOutlineEye} from "@react-icons/all-files/ai/AiOutlineEye";

const ShowModuleList = ({
                            serviceItem,
                            hiddenSwitch,
                            title = 'انتخاب زیر سیستم',
                            activated,
                            iconBtn,
                            serviceIndex,
                            ChoosePackage
                        }) => {

    const [open, setOpen] = useState(false);

    const showModal = () => {
        setOpen(true);
    };

    const handleOk = () => {
        handleCancel()
    };
    const handleCancel = () => {
        setOpen(false);
    };

    const onChange = (key) => {
        console.log(key);
    };


    return (<>
            {
                iconBtn ?
                    <Button
                        className='  iconBtn green' disabled={activated} onClick={showModal}>
                        <AiOutlineEye size={18}/>
                    </Button>
                    :
                    <Button
                        className='defBtn borderBtn' disabled={activated} type="primary" onClick={showModal}>زیر
                        سیستم‌
                        ها</Button>
            }

            <Modal
                width={1200}
                open={open}
                title={title}
                onOk={handleOk}
                onCancel={handleCancel}
                footer={[
                    <Button key="submit" className='submitBtn   ' type="primary" onClick={handleOk}>
                        ثبت
                    </Button>]}>
                <Checkbox.Group onChange={onChange} className='w-full'>

                        <div className="grid gap-4 m-0 w-full  xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 gap-6 grid-flow-row">


                        {serviceItem.moduleList.map((moduleItem, moduleIndex) => (
                            <>
                                {!moduleItem.hideSelection &&

                                    <div key={moduleIndex} className="defBoxBorder flex flex-col  !bg-gray-300 py-2  text-sm">
                                        <div className="moduleItem p-2">

                                            <div className='flex justify-between'>

                                                <div className="name font-bold">{moduleItem.moduleName}</div>
                                                {
                                                    !hiddenSwitch && <Switch className='bg-black-400'
                                                        onChange={(checked) => {
                                                            ChoosePackage(checked, serviceItem, serviceIndex, moduleItem, moduleIndex)
                                                        }}
                                                        checkedChildren="فعال"
                                                        checked={moduleItem.activated}
                                                        unCheckedChildren="غیرفعال"/>
                                                }


                                            </div>

                                            <div className="  w-full  mt-4">هزینه روزانه
                                                : {moduleItem.costPerDay} تومان
                                            </div>


                                        </div>
                                    </div>

                                }</>
                        ))}
                    </div>
                </Checkbox.Group>
            </Modal>
        </>
    );
};
export default ShowModuleList;