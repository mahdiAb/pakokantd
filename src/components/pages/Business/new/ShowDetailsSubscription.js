import {Button, Form, Modal} from 'antd';
import React, {useState} from 'react';
import {AiOutlineEye} from "@react-icons/all-files/ai/AiOutlineEye";
import KeyValue from "../../../microComponents/common/keyValue/KeyValue";

const ShowDetailsSubscription = ({data}) => {

    const [visible, setVisible] = useState(false);
    const [form] = Form.useForm();
    const showModal = (e) => {
        setVisible(true);
    };
    const handleOk = () => {
        form.submit()
    };


    const handleCancel = () => {
        setVisible(false);
    };


    return (
        <>

            <Button onClick={(e) => showModal(e)} className='iconBtn green'><AiOutlineEye size={18}/></Button>
            <Modal
                width={900}
                visible={visible}
                title=" سرویس های انتخاب شده"
                onOk={handleOk}
                onCancel={handleCancel}
                footer={[
                    <div className='flex justify-end'>
                        <Button key="back" className='defBtn borderBtn' onClick={handleCancel}>
                            انصراف
                        </Button>
                    </div>


                ]}
            >
                <div
                    className="grid gap-4   xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 gap-6 grid-flow-row">

                    {data.map((serviceItem, serviceIndex) => (<>
                            {serviceItem.hideSelectionTypeID !== 2 &&
                                <div key={serviceIndex}
                                     className="shadow flex flex-col p-5  text-sm border rounded border-black-200">
                                    <div className="serviceItem">
                                        <div className="imageBack">
                                            <div className='flex justify-between font-bold'>
                                                <div className=''>

                                                    <div className="name">{serviceItem.serviceName}</div>
                                                    <div className="mt-3 text-black">{serviceItem.sumPrice} تومان</div>


                                                </div>

                                                <img alt='pakok' src='/Group 379.svg'/>
                                            </div>


                                        </div>


                                        {
                                            serviceItem.moduleList.length ? <div className=" bg-white w-full p-3">
                                                    <div className='flex font-bold my-5 justify-between text-bold'>

                                                        <span>زيرسرويس ها</span>
                                                        <span>هزینه روزانه</span>
                                                    </div>
                                                    {serviceItem.moduleList.map((item, index) => (<>

                                                        <KeyValue value={`${item.costPerDay} تومان `} className={'mx-0  px-0  justify-between'} keyText={item.moduleName}/>




                                                    </>))}</div>
                                                : null
                                        }


                                    </div>
                                </div>
                            }</>
                    ))}

                </div>

            </Modal>
        </>
    );
};


export default ShowDetailsSubscription;