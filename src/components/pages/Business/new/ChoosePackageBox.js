import {Button, Switch} from 'antd';
import React, {useState} from 'react';
import {NavLink} from "react-router-dom";
import ShowModuleList from "./ShowModuleList";


const ChoosePackageBox = ({serviceList, next, loading, prev}) => {
    console.log(serviceList)
    const [serviceListNew, setServiceListNew] = useState([...serviceList])


    const ChoosePackage = (checked, serviceItem, serviceIndex, moduleItem, moduleIndex) => {

        let serviceListNewCopy = [...serviceListNew]
        let findService = serviceListNewCopy[serviceIndex]
        if (!moduleItem) {
            findService.activated = checked
        } else {
            let findModule = findService.moduleList[moduleIndex]
            findModule.activated = checked
        }
        setServiceListNew(serviceListNewCopy)

    }


    return (<div>


        <div className="grid gap-4     md:grid-cols-2   lg:grid-cols-3  ">


        {serviceListNew.map((serviceItem, serviceIndex) => (<>
                    {serviceItem.hideSelectionTypeID !== 2 &&
                        <div key={serviceIndex}
                             className=" flex flex-col text-sm  defBoxBorder ">


                            <div className='flex justify-between bg-black-200 rounded-xl p-3'>

                                <div className="font-bold">{serviceItem.serviceName}</div>

                                <img alt='pakok' src='/Group 379.svg'/>
                            </div>


                            <div className="p-3 leading-7 flex flex-col 	flex-1 justify-between ">
                                <div>
                                    <div className="flex items-center justify-between">
                                        <div className="font-bold mb-3">هزینه روزانه : {serviceItem.sumPrice} تومان
                                        </div>
                                        {/*<div className="flex items-center justify-between">*/}
                                        {/*    <NavLink to='/' className='iconBtn green mr-0'><i*/}
                                        {/*        className='icon-question-4'></i></NavLink>*/}
                                        {/*    <NavLink to='/' className='iconBtn blue'><i*/}
                                        {/*        className='icon-megaphone'></i></NavLink>*/}
                                        {/*</div>*/}
                                    </div>
                                    <div className="des mb-3">{serviceItem.description}</div>

                                </div>

                                <div className={`flex items-center  w-100 
                            ${serviceItem.moduleList.length > 0 ? 'justify-between' : 'justify-end'}`}>
                                    {
                                        serviceItem.moduleList.length > 0 &&
                                        <ShowModuleList ChoosePackage={ChoosePackage}
                                                        activated={!serviceItem.activated}
                                                        serviceItem={serviceItem} serviceIndex={serviceIndex}/>
                                    }

                                    <Switch className='bg-black-400'
                                            onChange={(checked) => ChoosePackage(checked, serviceItem, serviceIndex)}
                                            checkedChildren="فعال"
                                            disabled={serviceItem.hideSelectionTypeID === 3}
                                            checked={serviceItem.activated}
                                            unCheckedChildren="غیرفعال"/>
                                </div>


                            </div>

                        </div>
                    }</>
            ))}

        </div>
        <div className=" flex justify-between mt-4">


            <NavLink to={`/`} className='defBtn borderBlue'>لیست کسب و کارها</NavLink>

            <div className="flex  items-center  ">

                <Button className='defBtn greenBtn ml-2' onClick={() => prev()}>مرحله قبل</Button>
                <Button className='defBtn submitBtn ' loading={loading} onClick={() => next(serviceListNew)}>مرحله
                    بعد</Button>
            </div>
        </div>


    </div>);
};
export default ChoosePackageBox;