import React, {useState} from 'react';
import {Button} from "antd";
import ShowDetailsSubscription from "./ShowDetailsSubscription";
import {NavLink} from "react-router-dom";
import {ConvertToPrice} from "../../../helper/ConvertToPrice";
import KeyValue from "../../../microComponents/common/keyValue/KeyValue";


const ChooseSubscription = ({prev, loadingCreateBusiness, formData, selectSubscription}) => {
    const [serviceDay, setServiceDay] = useState();
     const selectPack = (x) => {
        setServiceDay(x)
        selectSubscription(x)
    }
     return (
        <div className="">

            <div className="grid gap-4   md:grid-cols-2 lg:grid-cols-3  ">

                {formData?.packagelist?.map(item => (<>


                    <div className="  flex flex-col p-5  text-sm   defBoxBorder"
                         key={item.numberOfDay}>

                        <div className="item ">
                            <h5 className="font-bold text-md mb-8 text-lg   text-center">{item.packageName}</h5>
                            <div className="inner ">


                                <KeyValue hiddenColon className='mb-6  justify-between'
                                          value={<><span
                                              className='ml-1'>{ConvertToPrice(item.sumAmount)}</span><span>تومان</span></>}

                                          keyText={<span
                                              className='flex items-center'>هزینه سرویس ها <ShowDetailsSubscription
                                              data={formData?.dtoService}/>:</span>}/>

                                <KeyValue className='mb-6  justify-between'
                                          value={<><span
                                              className='ml-1'>{ConvertToPrice(item.sumAmount)}</span><span>تومان</span></>}

                                          keyText={'هزینه نفرات اضافه'}/>


                                <KeyValue className='mb-6  justify-between'
                                          value={<><span
                                              className='ml-1'>{ConvertToPrice(item.sumAmount)}</span><span>تومان</span></>}

                                          keyText={'هزینه شعب'}/>

                                <KeyValue className='mb-6  justify-between'
                                          value={<><span
                                              className='ml-1'>{ConvertToPrice(item.sumAmount)}</span><span>تومان</span></>}

                                          keyText={'قیمت کل'}/>
                                <KeyValue className='mb-6  justify-between'
                                          value={<><span
                                              className='ml-1'>{ConvertToPrice(item.discountAmount)}</span><span>تومان</span></>}

                                          keyText={'  تخفیف'}/>
                                <KeyValue className='mb-6  justify-between'
                                          value={<><span
                                              className='ml-1'>{ConvertToPrice(item.taxAmount)}</span><span>تومان</span></>}

                                          keyText={'  مالیات'}/>

                                <KeyValue className='mb-6  justify-between'
                                          value={<><span
                                              className='ml-1'>{ConvertToPrice(item.paymentAmount)}</span><span>تومان</span></>}

                                          keyText={'  قابل پرداخت'}/>


                                <Button className='submitBtn   mt-5 w-full'
                                        loading={serviceDay === item.numberOfDay && loadingCreateBusiness}
                                        onClick={() => selectPack(item.numberOfDay)}>انتخاب</Button>


                            </div>
                        </div>

                    </div>


                </>))}    </div>

            <div className=" flex justify-between mt-4">


                <NavLink to={`/`} className='defBtn borderBtn'>لیست کسب و کارها</NavLink>

                <div className="flex  items-center  ">

                    <Button className='defBtn submitBtn  ' onClick={() => prev()}>مرحله قبل</Button>

                </div>
            </div>


        </div>
    );
};
export default ChooseSubscription;