import {Button, Form, Select, Switch} from 'antd';
import React, {useEffect, useState} from 'react';

import {NavLink, useNavigate, useParams} from "react-router-dom";
import TextInput from "../../../microComponents/common/uikits/TextInput";

const {Option} = Select;


const BusinessInfo = ({BusinessTypeList, loadingStep1, BusinessInfoDisabled, next, formData}) => {
    let [showNumberBranch, setShowNumberBranch] = useState(formData.maxNumberOfBranch || false)
    let [unPaidOrder, setUnPaidOrder] = useState(formData.unPaidOrder)
    let [loading, setLoading] = useState(loadingStep1)
    const navigate = useNavigate();
    let {id} = useParams();
    const onFinish = (values) => {
        values.maxNumberOfBranch = values.maxNumberOfBranch | 0
        next(values)
    };
    useEffect(() => {


        setLoading(loadingStep1)
    }, [loadingStep1])
    const handleBranch = (values) => {

        setShowNumberBranch(values)
    };

    return (<div className=" ">

            <Form
                name="basic"
                initialValues={formData}

                onFinish={onFinish}

                layout="vertical"
            >
                <div
                    className="grid gap-4   xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 gap-6 grid-flow-row">


                    <Form.Item className={'input_wrap'} label="نام کسب و کار" name={'businessName'}
                               rules={[{required: true, message: 'لطفا نام کسب و کار خود را وارد کنید!'}]}>
                        <TextInput disabled={BusinessInfoDisabled} className={'defInput'}/>
                    </Form.Item>

                    <Form.Item label={' زمینه فعالیت'} name={'businessTypeID'} className={'input_wrap'}
                               rules={[{required: true, message: 'لطفا زمینه فعالیت خود را وارد کنید!'}]}>


                        <Select disabled={BusinessInfoDisabled}>
                            {BusinessTypeList.map((item, index) => (<Option key={index}
                                                                            value={String(item.f_BusinessTypeID)}>{item.f_BusinessTypeName}</Option>

                            ))}

                        </Select>


                    </Form.Item>


                    <Form.Item className={'input_wrap'} label="تعداد کاربران " name={'maxNumberOfUser'}
                               rules={[{required: true, message: 'لطفا تعداد کاربران خود را وارد کنید!'}]}>
                        <TextInput type='tel' controls={false} keyboard={false} className={'defInput'}

                                   min={2}
                                   max={500}
                                   disabled={BusinessInfoDisabled}/>
                    </Form.Item>


                    <div className="flex input_wrap">


                        <Form.Item className={'input_wrap'} label="شعب" name="isMultiBranch" valuePropName="checked"

                        >
                            <Switch onChange={(e) => handleBranch(e)} className='bg-green font-black '
                                    checkedChildren={<span className='pt-1'>فعال</span>}
                                    unCheckedChildren={<span className='pt-1'>غیرفعال</span>}/>
                        </Form.Item>


                        {showNumberBranch && <Form.Item className={'input_wrap mr-3 flex-1'}

                                                        label="تعداد شعب"
                                                        name="maxNumberOfBranch"
                                                        rules={[{
                                                            required: true,
                                                            message: 'لطفا تعداد شعب خود را وارد کنید!',
                                                        },]}
                        >

                            <Select>
                                <Option value={1}>1</Option>
                                <Option value={2}>2</Option>
                                <Option value={3}>3</Option>
                                <Option value={4}>4</Option>
                                <Option value={5}>5</Option>
                                <Option value={6}>6</Option>
                                <Option value={7}>7</Option>
                                <Option value={8}>8</Option>
                                <Option value={9}>9</Option>
                                <Option value={10}>10</Option>


                            </Select>


                        </Form.Item>
                        }

                    </div>


                </div>
                <Form.Item className='mb-0'>
                    <div className="steps-action d-flex justify-content-end mt-4">


                        <div className="steps-action flex justify-between mt-4 w-100">
                            <NavLink to={`/`} className='defBtn borderBlue'>لیست کسب و
                                کارها</NavLink>


                            <div className='flex items-center'>


                                {unPaidOrder && <div><NavLink  to={`/order/${formData.businessID}`}
                                                              className='ml-2 defBtn  borderBtn '>صورت حساب قبلی</NavLink>
                                </div>
                                }
                                <Button disabled={unPaidOrder} loading={loading} type="primary" htmlType="submit"
                                        className='defBtn submitBtn '>مرحله
                                    بعد</Button>
                            </div>
                        </div>


                    </div>
                </Form.Item>

            </Form>
        </div>
    );
};
export default BusinessInfo;