import {Form, Input, Select} from 'antd';
import React from 'react';

const {Option} = Select;

const handleChange = (value) => {
};
const PackageInfo = () => {
    const [form] = Form.useForm();
    const onFinish = (values) => {
    };
    const onFinishFailed = (errorInfo) => {
    };


    const handleFormSubmit = () => {
        form.submit()

    };


    return (
        <div className="">
            <h4 className="title mb-4">
                نیازمندی های پکیج
            </h4>
            <Form
                name="basic"
                form={form}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                layout="vertical"
            >
                <div className="row m-0">
                    <h4 className=" title themText">اطلاعات سال مالی</h4>
                    <div className="col-sm-6 col-lg-4">
                        <Form.Item
                            label="تاریخ شروع "
                            name="username"
                            rules={[{required: true, message: 'لطفا تاریخ شروع کسب و کار خود را وارد کنید!',},]}
                        >
                            <Input className={'defInput'}/>
                        </Form.Item>
                    </div>
                    <div className="col-sm-6 col-lg-4">
                        <Form.Item
                            label="تاریخ پایان "
                            name="number"
                            rules={[{required: true, message: 'لطفا تاریخ پایان  خود را وارد کنید!',},]}
                        >
                            <Input className={'defInput'}/>
                        </Form.Item>
                    </div>
                    <div className="col-sm-6 col-lg-4">
                        <Form.Item
                            label="عنوان سال مالی "
                            name="number"
                            rules={[{required: true, message: 'لطفا عنوان سال مالی   خود را وارد کنید!',},]}
                        >
                            <Input className={'defInput'}/>
                        </Form.Item>
                    </div>
                    <h4 className=" title themText">اطلاعات انبار</h4>

                    <div className="col-sm-6 col-lg-4">
                        <Form.Item
                            label="سیستم حسابداری انبار "
                            name="number"
                            rules={[{required: true, message: 'لطفا سیستم حسابداری انبار  خود را انتخاب کنید!',},]}
                        >
                            <Select defaultValue="lucy" onChange={handleChange}>
                                <Option value="jack">Jack</Option>
                                <Option value="lucy">Lucy</Option>
                                <Option value="disabled" disabled>
                                    Disabled
                                </Option>
                                <Option value="Yiminghe">yiminghe</Option>
                            </Select>
                        </Form.Item>
                    </div>
                    <div className="col-sm-6 col-lg-4">
                        <Form.Item
                            label="روش ارزیابی انبار"
                            name="number"
                            rules={[{required: true, message: 'لطفا روش ارزیابی انبار  خود را انتخاب کنید!',},]}
                        >
                            <Select defaultValue="lucy" onChange={handleChange}>
                                <Option value="jack">Jack</Option>
                                <Option value="lucy">Lucy</Option>
                                <Option value="disabled" disabled>
                                    Disabled
                                </Option>
                                <Option value="Yiminghe">yiminghe</Option>
                            </Select>
                        </Form.Item>
                    </div>
                    <h4 className=" title themText">تنظیمات مالیاتی</h4>

                    <div className="col-sm-6 col-lg-4">
                        <Form.Item
                            label="نرخ مالیات ارزش افزوده "
                            name="number"
                            rules={[{required: true, message: 'لطفا  نرخ مالیات ارزش افزوده  خود را وارد کنید!',},]}
                        >
                            <Input showCount={false} type='number' className={'defInput'}/>
                        </Form.Item>
                    </div>
                </div>


            </Form>
        </div>
    );
};
export default PackageInfo;