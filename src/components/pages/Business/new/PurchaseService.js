import {Steps} from 'antd';
import React, {useEffect, useState} from 'react';
import BusinessInfo from "./BusinessInfo";
import ChoosePackageBox from "./ChoosePackageBox";
import ChooseSubscription from "./ChooseSubscription";


import {useNavigate, useParams} from "react-router-dom";
import {ShowNotification} from "../../../helper/ShowNotification";
import {
    AddOrderExtensionBusiness,
    CalculatedCostSelectedService,
    CreateBusiness,
    GetServiceListByBusinessTypeID
} from "../../../helper/apiList/apiListBusiness";
import Request from "../../../helper/request/Request";

const {Step} = Steps;

const PurchaseService = ({BusinessTypeList, BusinessData, isUpgrade, BusinessInfoDisabled, create}) => {
    const [current, setCurrent] = useState(0);
    const [serviceList, setServiceList] = useState(BusinessData?.serviceList || []);
    const [BusinessInfoData, setBusinessInfoData] = useState(BusinessData);
    const [loadingStep2, setLoadingStep2] = useState(false);
    const [loadingCreateBusiness, setLoadingCreateBusiness] = useState(false);
    const [data3, setData3] = useState({});
    const [selectedServiceList, setSelectedServiceList] = useState([]);
    const [didMount, setDidMount] = useState(false);
    const [loadingStep1, setLoadingStep1] = useState(false);

    const navigate = useNavigate();
    const params = useParams();


    const next = (data) => {

        switch (current) {
            case 0:
                setBusinessInfoData(data)
                if (!create) {
                    plusStep()

                }

                break;
            case 1:


                if (data.filter(item => item.activated === true).length) {

                     const x = JSON.parse(JSON.stringify(data))
                    let y = []
                    let newData = []
                    let newModuleList = []


                    x.map((item, index) => {
                        if (item.activated) {
                            newModuleList = item.moduleList?.filter(item => item.activated === true)
                            y = newModuleList
                            item.moduleList = y
                            newData.push(item)

                        }
                    })
                    getCalculate(newData)
                    setSelectedServiceList(newData)


                } else {
                    ShowNotification('هیچ سرویسی انتخاب نکرده‌اید!', false)
                }


                break;
            default:
        }


    };


    useEffect(() => {
        if (didMount && create) {
            GetServicesListByBusinessTypeID()

        } else {
            setDidMount(true)
        }
    }, [BusinessInfoData,create])
    const GetServicesListByBusinessTypeID = () => {
        setLoadingStep1(true)
        let BusinessTypeID = {
            BusinessTypeID: BusinessInfoData.businessTypeID,
            IsMultiBranch: BusinessInfoData.isMultiBranch || false
        }


        Request.init(GetServiceListByBusinessTypeID, function (data) {
            setLoadingStep1(false)
            setServiceList(data.data)
            plusStep()
        }, function (error) {
            setLoadingStep1(false)

        }).setMainData(BusinessTypeID).setRouter(navigate).callRequest()


    }
    const getCalculate = (newData) => {
        setLoadingStep2(true)

        let data = {
            'isMultiBranch': BusinessInfoData?.isMultiBranch || false,
            'BusinessID': params.id,
            'numberOfBranch': BusinessInfoData?.maxNumberOfBranch,
            'numberOfUser': BusinessInfoData?.maxNumberOfUser,
            'businessTypeID': BusinessInfoData?.businessTypeID,
            'serviceList': newData,
            'IsUpgrade': !!isUpgrade

        }


        Request.init(CalculatedCostSelectedService, function (data) {
            setLoadingStep2(false)
            setData3(data.data)
            setCurrent(2)
        }, function (error) {
            setLoadingStep2(false)

        }).setMainData(data).setRouter(navigate).callRequest()


    }
    const plusStep = () => {
        setCurrent(current + 1);
    }
    const prev = () => {
        setCurrent(current - 1);
    };

    const selectSubscription = (maxDay) => {
         let data = {


            BusinessName: BusinessInfoData?.businessName,
            businessTypeID: BusinessInfoData?.businessTypeID,
            maxNumberOfUser: BusinessInfoData?.maxNumberOfUser,
             maxNumberOfBranch: BusinessInfoData?.maxNumberOfBranch,
            serviceList: selectedServiceList,
            maxNumberOfDay: maxDay,

             isMultiBranch: BusinessInfoData?.isMultiBranch || false,
        }
        if (!create) {
            data.BusinessID = params.id
        }


        let url = create ? CreateBusiness : AddOrderExtensionBusiness
        setLoadingCreateBusiness(true)

        Request.init(url, function (data) {




            if (data.data.messageID === 0) {
                create ? navigate(`/order/${data.data.valueText}`)
                    :

                    navigate(`/order/${params.id}`)
            } else {
                ShowNotification(data.data.message, false)

            }




            setLoadingCreateBusiness(false)

        }, function (error) {
            setLoadingCreateBusiness(false)

        }).setMainData(JSON.stringify(data)).setRouter(navigate).callRequest()


    }


    let steps = [
        {
            title: <span className=' '>اطلاعات کسب و کار</span>,
            content: <BusinessInfo next={next} loadingStep1={loadingStep1} BusinessInfoDisabled={BusinessInfoDisabled}
                                   BusinessTypeList={BusinessTypeList} formData={BusinessInfoData}/>
        }, {
            title: <span className=''>سیستم‌ ها</span>,
            content: <ChoosePackageBox next={next} prev={prev} loading={loadingStep2} serviceList={[...serviceList]}/>
        }, {
            title: <span className='ml-5'>انتخاب اشتراک</span>,
            content: <ChooseSubscription prev={prev} loadingCreateBusiness={loadingCreateBusiness}
                                         selectSubscription={selectSubscription} formData={data3}/>
        },
    ];


    return (


        <div className=" ">


                <div className="defBox !py-8 mb-3   ">
                    <Steps current={current}>
                        {steps.map((item, index) => (<Step key={index} title={item.title}/>))}
                    </Steps>
                </div>
            <div
                className="defBox">{steps[current]?.content}</div>


        </div>
    );
};


export default PurchaseService;