import {Button, Modal} from 'antd';
import React, {useState} from 'react';

import {BsListCheck} from "@react-icons/all-files/bs/BsListCheck";

const ActiveService = ({serviceList}) => {
    const [open, setOpen] = useState(false);

    console.log(serviceList)
    const showModal = () => {
        setOpen(true);
    };

    const handleOk = () => {
        handleCancel()

    };
    const handleCancel = () => {
        setOpen(false);
    };


    return (<>


        <Button
            className='  iconBtn green' onClick={showModal}><BsListCheck size={18}/></Button>


        <Modal
            width={800}
            open={open}
            title={'لیست سرویسها و زیر سرویس ها'}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[<Button key="submit" className='submitBtn bgBtn ' type="primary" onClick={handleOk}>
                ثبت
            </Button>]}>
            <div
                className="grid gap- xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 gap-3  grid-flow-row">

                {serviceList.map((serviceItem, serviceIndex) => (<>
                    <div key={serviceIndex} className="   defBox">
                        <div className="h-auto">
                            <div className=" ">
                                <div className='flex items-center justify-between'>

                                    <div className="ml-4 ">
                                        <div className="mb-4 font-bold">
                                            {serviceItem.serviceName}</div>
                                    </div>

                                    <img alt={'pakok'} width={100} src={'/logo1.png'}/>
                                </div>


                            </div>

                            {serviceItem.moduleList.length ? <div className="inner w-100">
                                <ul className='p-0'> {serviceItem.moduleList.map((item, index) => (
                                    <li key={index}><p>{item.moduleName}</p></li>))}
                                </ul>
                            </div> : null}

                        </div>
                    </div>
                </>))}

            </div>
        </Modal>
    </>);
};
export default ActiveService;