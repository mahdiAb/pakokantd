import {Button, Modal} from 'antd';
import React, {useState} from 'react';
import Cookies from 'js-cookie'
import {useNavigate, useParams} from "react-router-dom";
import {Logout} from "../../helper/apiList/apiListBusiness";
import Request from "../../helper/request/Request";
import {ShowNotification} from "../../helper/ShowNotification";

const ExitUser = () => {
    const [open, setOpen] = useState(false);
    const [loading, setLoading] = useState(false);
    let {id} = useParams();

    const navigate = useNavigate();
    const showModal = () => {
        setOpen(true);
    };

    const handleOk = () => {


        let data = {
            RefreshToken: Cookies.get('RefreshToken')
        }
        setLoading(true)
        Request.init(Logout, function (data) {
            setLoading(false)
            Cookies.remove(`BusinessToken_${id}`)
            Cookies.remove(`BusinessRefreshToken_${id}`)
            navigate('/')

        }, function (error) {
            ShowNotification({type: false, description: 'خروج از این کسب و کار کامل نشد. دوباره امتحان کنید!',})
            setLoading(false)

        }).setRouter(navigate).setMainData(data).callRequest()


    };
    const handleCancel = () => {
        setOpen(false);
    };


    return (<>


        <div onClick={showModal}>خروج از این کسب و کار</div>


        <Modal
            width={800}
            open={open}
            title={'خروج از این کسب و کار'}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[<Button key="exit" loading={loading} className='borderBtn defBtn  ' type="primary"
                             onClick={handleCancel}>
                انصراف
            </Button>, <Button key="submit" className='submitBtn defBtn' type="primary" onClick={handleOk}>
                خروج
            </Button>

            ]}>
            <div>

                <p className='font-medium text-sm'>آیا از خروج خود از این کسب و کار اطمینان دارید؟</p>

            </div>
        </Modal>
    </>);
};
export default ExitUser;