import React, {useEffect, useState} from 'react';
import {useParams} from "react-router-dom";
import {GetBusinessByBusinessIDForExtension, GetBusinessTypeList} from "../../../helper/apiList/apiListBusiness";
import Request from "../../../helper/request/Request";
import Layout from "../../../microComponents/common/Layout/Layout";
import PurchaseService from "../new/PurchaseService";


const App = () => {
    let [BusinessTypeList, setBusinessTypeList] = useState()
    let [BusinessData, setBusinessData] = useState()
    const params = useParams();
    useEffect(() => {

        (async () => {


            await  Request.init(GetBusinessTypeList, function (data) {
                setBusinessTypeList(data.data)
            }).callRequest()

             let data = {
                BusinessID: params.id
            }
            await Request.init(GetBusinessByBusinessIDForExtension, function (res) {
                setBusinessData(res.data)
            }).setMainData(data).callRequest()

        })();


    }, [params.id])
     return (
        <Layout>

            {
                BusinessTypeList && BusinessData &&
                <PurchaseService BusinessTypeList={BusinessTypeList} BusinessData={BusinessData} BusinessInfoDisabled/>

            }
        </Layout>);
};
export default App;


