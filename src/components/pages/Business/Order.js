import React, {useCallback, useEffect, useState} from 'react';

import {Button, Form, Radio, Skeleton} from "antd";


import {NavLink, useNavigate, useParams} from "react-router-dom";
import Request from "../../helper/request/Request";
import {
    BuyCheckPaymentTypesBusiness,
    DeleteOrder,
    GetOrderByBusinessId,
    Payment
} from "../../helper/apiList/apiListBusiness";
import {ShowNotification} from "../../helper/ShowNotification";
import Layout from "../../microComponents/common/Layout/Layout";
import ShowModuleList from "./new/ShowModuleList";
import {ConvertToPrice} from "../../helper/ConvertToPrice";
import KeyValue from "../../microComponents/common/keyValue/KeyValue";
import OrderLoading from "../../skeletonLoading/OrderLoading";


function Index() {
    const [sendLoading, setSendLoading] = useState(false);
    const [loadingDelete, setLoadingDelete] = useState(false);
    const [bankID, setBankID] = useState()
    const [paymentData, setPaymentData] = useState();
    const navigate = useNavigate();
    const [getLoading, setGetLoading] = useState(true);
    const [orderData, setOrderData] = useState();
    const [didMount, setDidMount] = useState(false);
    const [form] = Form.useForm();
    let {id} = useParams();
    const [visibilityState, setVisibilityState] = useState(true);

    const handleVisibilityChange = useCallback(() => {
        setVisibilityState(document.visibilityState === 'visible');
    }, []);

    useEffect(() => {
        document.addEventListener("visibilitychange", handleVisibilityChange)
        return () => {
            document.removeEventListener("visibilitychange", handleVisibilityChange)
        }
    }, []);

    const deleteOrder = async () => {
        setLoadingDelete(true)
        let data = {
            OrderID: orderData?.orderID
        }
        await Request.init(DeleteOrder, function (res) {
            console.log(res)
            ShowNotification(res.data.message, true)
            setLoadingDelete(false)
            navigate(`/`)
        }, function (error) {
            setLoadingDelete(false)
        }).setMainData(data).setRouter(navigate).callRequest()

    }
    const getPaymentTypes = async () => {
        let data = {


            "ActionID": orderData?.orderID,
            "BusinessID": id
        }


        await Request.init(BuyCheckPaymentTypesBusiness, function (res) {

            setGetLoading(false)
            setPaymentData(res.data)

        }, function (error) {
            setGetLoading(false)
        }).setMainData(data).setRouter(navigate).callRequest()


    }
    useEffect(() => {


        if (visibilityState) {
            let data = {
                BusinessId: id
            }
            Request.init(GetOrderByBusinessId, function (data) {
                setOrderData(data.data)
            }, function (error) {
                console.log(error)
            }).setMainData(data).setRouter(navigate).setMainData(data).callRequest()

        }


    }, [id, visibilityState]);


    useEffect(() => {


        if (didMount) {
            getPaymentTypes()

        } else {
            setDidMount(true)
        }
    }, [orderData])


    const handlePayment = (values) => {
        // values.orderID = orderData?.orderID
        values.UseWallet = values?.UseWallet ? values.UseWallet : false
        values.paymentID = paymentData?.paymentID
        // values.businessID = id

        setSendLoading(true)
        Request.init(Payment, function (data) {
            console.log(data)
            setSendLoading(false)
            window.open(data.data.dtoipgUrlResponse.fullURL);
        }, function (error) {
            console.log(error)
            setSendLoading(false)
        }).setMainData(values).callRequest()


    }
    const onFinishFailed = (values) => {
        console.log(values)
    }
    return <>
        <Layout>
            <div className="">
                <h1 className="font-bold text-base mb-5">صورت حساب</h1>
                {/*<div className="flex justify-between items-center mb-3 defBox">*/}
                {/*    <div className="logoBox flex items-center">*/}
                {/*        <img width={60} src='/Group 379.svg' alt="logo"/>*/}
                {/*        <h5 className='title  mx-2 my-0 font-weight-bold'>item.businessName</h5>*/}
                {/*    </div>*/}
                {/*    <KeyValue value={'PAKOK123'} className={'m-0'} keyText={'صاحب کسب و کار'} keyClass={' my-1'} valueClass={' my-1'}/>*/}
                {/*    <KeyValue value={'item.businessTypeName'} className={'m-0'} keyText={'صنف کسب و کار'} keyClass={' my-1'} valueClass={' my-1'}/>*/}
                {/*</div>*/}

                <>

                    {
                        !getLoading ? orderData?.dtoService.map((item, index) => (


                                <div key={index}
                                     className=" sm:flex items-center justify-between mb-3 defBox">


                                    <KeyValue value={<>
                                        {item.serviceName}
                                        {item.moduleList.length > 0 &&
                                            <ShowModuleList hiddenSwitch title={'ماژول های انتخاب شده'}
                                                            serviceItem={item}
                                                            iconBtn/>}
                                    </>} valueClass={'items-center flex'} keyText={'نام سرویس'} className={'my-2'}/>


                                    <KeyValue value={orderData?.packagelist[0]?.numberOfDay} className={'my-2'}
                                              keyText={'تعداد روز'}/>
                                    <KeyValue value={ConvertToPrice(item.sumPrice)} className={'my-2'} keyText={'قیمت کل'}/>


                                </div>


                            ))
                            : <OrderLoading/>

                    }


                    <div className={`flex ${paymentData ? 'justify-between' : 'justify-end'}`}>
                        <div className="flex-1 defBox ml-3">

                            <Skeleton loading={getLoading} style={{width: '100%'}} paragraph={{rows: 5}} title={false}
                                      rootClassName={'loadingJustifyBetween'}>
                                <Form className={'flex justify-between flex-col h-full'}
                                      form={form}
                                      name="basic"
                                      layout="vertical"
                                      onFinish={handlePayment}
                                      onFinishFailed={onFinishFailed}
                                      autoComplete="0n"
                                      initialValues={{
                                          bankID: paymentData?.ipgList[0]?.bankID,
                                          UseWallet: true
                                      }}
                                >

                                    <div>    {


                                        <Form.Item name={'bankID'}>
                                            <Radio.Group
                                                value={bankID}
                                                onChange={(e) => setBankID(e.target.value)}
                                                // name={'InvoiceSettlementMethodID'}
                                                // value={invoiceSettlementMethodIDValue}
                                            >
                                                {paymentData?.ipgList?.map((item, index) => (


                                                    <Radio value={item?.bankID} className={'defBoxBorder'}>
                                                        <div className=' flex justify-center items-center flex-col'>
                                                            <img width={100} src={'/cropped-Asset-1@10x.png'}/>
                                                            <p className={'my-4 text-center'}>{item?.name}</p>

                                                        </div>
                                                    </Radio>


                                                ))}
                                            </Radio.Group></Form.Item>

                                    }
                                        {paymentData?.moneyWalletRemainingValue ?
                                            <Form.Item name={'UseWallet'}>
                                                <Radio.Group>

                                                    <Radio value={true}><p className={'my-4 text-center'}>
                                                        {
                                                            !paymentData.paymentWithMoneyWallet ?
                                                                'پرداخت از کیف پول و درگاه'
                                                                : ' پرداخت از کیف پول'
                                                        }
                                                    </p></Radio>


                                                    <Radio value={false}>


                                                        <p className={'my-4 text-center'}> پرداخت از درگاه</p>


                                                    </Radio>


                                                </Radio.Group>


                                            </Form.Item>


                                            : null}
                                    </div>
                                    <div className='flex justify-between mb-0 items-center mt-5'>
                                        <div className={'flex  items-center'}>
                                            <NavLink to={`/`} className='defBtn borderBtn'>لیست کسب و
                                                کارها</NavLink>
                                            <Button className='defBtn borderRed mx-2  ' loading={loadingDelete}
                                                    onClick={() => deleteOrder()}>حذف
                                                صورت حساب</Button>
                                        </div>
                                        <Button loading={sendLoading} className='defBtn submitBtn'
                                                htmlType="submit">
                                            پرداخت
                                        </Button>
                                    </div>
                                </Form>
                            </Skeleton>


                        </div>
                        <div className='w-full md:w-fit defBox '>
                            <Skeleton loading={getLoading} style={{width: '270px'}} paragraph={{rows: 5}} title={false}
                                      rootClassName={'loadingJustifyBetween'}>
                                <KeyValue value={orderData?.packagelist[0]?.packageName}
                                          className={'my-4 mx-0 w-full justify-between'}
                                          keyText={'مدت اشتراک'} keyClass={' themText2 ml-5'}/>

                                <KeyValue value={ConvertToPrice(orderData?.packagelist[0]?.sumAmount) + ' تومان '}
                                          className={'my-4 mx-0 w-full justify-between'}
                                          keyText={'هزینه سرویس ها'} keyClass={' themText2 ml-5'}/>

                                <KeyValue
                                    value={ConvertToPrice(orderData?.packagelist[0]?.discountAmount) + ' تومان '}
                                    className={'my-4 mx-0 w-full justify-between'}
                                    keyText={'تخفیف'} keyClass={' themText2 ml-5'}/>


                                <KeyValue value={ConvertToPrice(orderData?.packagelist[0]?.taxAmount) + ' تومان '}
                                          className={'my-4 mx-0 w-full justify-between'}
                                          keyText={' مالیات یر ارزش افزوده'} keyClass={' themText2 ml-5'}/>


                                <KeyValue
                                    value={ConvertToPrice(orderData?.packagelist[0]?.paymentAmount) + ' تومان '}
                                    className={'my-4 mx-0 w-full justify-between'}
                                    keyText={'مجموع مبلغ قابل پرداخت'} keyClass={' themText2 ml-5'}/> </Skeleton>


                        </div>

                    </div>

                </>


            </div>


        </Layout>
    </>
}

export default Index;

