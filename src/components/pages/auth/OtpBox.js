import React, {useEffect, useState} from 'react';
import TextInput from "../../microComponents/common/uikits/TextInput";
 import Request from "../../helper/request/Request";
import {ChangeForgottenPassword, Login} from "../../helper/apiList/apiListBusiness";
import {useNavigate} from "react-router-dom";

import Cookies from 'js-cookie';
import {Form,Button} from "antd";
import FormItem from "../../microComponents/common/uikits/FormItem";
import {ShowNotification} from "../../helper/ShowNotification";
import {number6} from "../../helper/validation/Validation";


const OtpBox = ({
                    addValues,
                    setStep, sendPhone,
                    url = Login,
                    loadingSendPhone = false,
                }) => {
    const [timeLeft, setTimeLeft] = useState(120);
    const [disableTimer, setDisableTimer] = useState(true);
    const [form] = Form.useForm();

    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        if (!timeLeft) {
            setDisableTimer(false)
            return
        }
        const intervalId = setInterval(() => {
            setTimeLeft(timeLeft - 1);
        }, 1000);

        return () => {
            clearInterval(intervalId)
        };

    }, [timeLeft]);



    const sendPass = (values) => {
        console.log(addValues)
        values = {...values,  ...addValues};
        console.log(values)
        setLoading(true)
        Request.init(url, async function (data) {

            setLoading(false)

            if (url === Login) {
                await Cookies.set('Token', data.data.jwtToken, { expires: 7 })
                await Cookies.set('RefreshToken', data.data.refreshToken, { expires: 7 })
                await navigate('/')
            } else if (url === ChangeForgottenPassword) {
                if (data.data.messageID === 0) {

                    setStep(1)
                } else {
                    ShowNotification(data.data.message, false)
                }





            }



        }, function (error) {

            ShowNotification('رمز وارد شده اشتباه است!', false)

            setLoading(false)

        }).loginPages().setRouter(navigate).setMainData(values).callRequest()
    };




    const onFinishFailedChangePass = (errorInfo) => {
        console.log(errorInfo)
    };
    const handleSendPhone = () => {
      sendPhone()
        setTimeLeft(120)
    }
    return (
        <>
            <Form
                form={form}
                name="basic"
                layout="vertical"
                initialValues={{rule: true}}
                onFinish={sendPass}
                onFinishFailed={onFinishFailedChangePass}
                autoComplete="0n"
            >
                <FormItem
                    pattern={number6} title='رمز یکبارمصرف' name={'otp'}>

                    <TextInput
                        htmlType={'tel'}
                    />

                </FormItem>


                <div className="flex items-center justify-between">

                    <span onClick={setStep} className="cursor-pointer">بازگشت</span>
                    <Button type='button' loading={loadingSendPhone} disabled={disableTimer} onClick={handleSendPhone}
                            className={'defBtn textBtn   m-0 p-0 align-items-center'}>
                         <span className='mx-2'>ارسال مجدد {timeLeft > 0 &&
                             <span>{timeLeft}  </span>}
                         </span>
                    </Button>
                    <Form.Item className='mb-0'>
                        <Button loading={loading} className='defBtn submitBtn' htmlType="submit">
                            ورود
                        </Button>
                    </Form.Item>
                </div>
            </Form>

        </>
    );
};

export default OtpBox;