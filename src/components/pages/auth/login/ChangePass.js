import React, {useState} from 'react';
import {Button, Form} from 'antd';


import {useNavigate} from "react-router-dom";
import {ShowNotification} from "../../../helper/ShowNotification";
import Request from "../../../helper/request/Request";
import {ChangeForgottenPassword, GenerateSMSOTP} from "../../../helper/apiList/apiListBusiness";
import FormItem from "../../../microComponents/common/uikits/FormItem";
import PassInput from "../../../microComponents/common/uikits/PassInput";
import OtpBox from "../OtpBox";
import TextInput from "../../../microComponents/common/uikits/TextInput";
import {mobile} from "../../../helper/validation/Validation";

const ChangePass = ({changeStep}) => {
    let [step, setStep] = useState(1)
    const [phone, setPhone] = useState();
    const [form] = Form.useForm();
    const [loading, setLoading] = useState(false);

    const navigate = useNavigate();

    const sendPhone = (values) => {
        if (values.ConfirmNewPassword === values.NewPassword) {
            setPhone(values.MobileNumber)
            setLoading(true)
            Request.init(GenerateSMSOTP, function (data) {
                setLoading(false)
                if (data.data.messageID === 0) {
                    setStep(2)
                } else {
                    ShowNotification({
                        type: false, description: data.data.message
                    })
                }
            }, function (error) {

                setLoading(false)

            }).setRouter(navigate).setMainData(values).callRequest()
        } else {
            ShowNotification(
               'کلمه عبور و تکرار کلمه عبور با هم برابر نیستند!',false
             )
        }

    };


    const onFinishFailedChangePass = (errorInfo) => {
        console.log(errorInfo)
    };
    return (
        <div>
            {
                step === 1 ?

                    <Form
                        form={form}
                        name="basic"
                        layout="vertical"
                        initialValues={{rule: true}}
                        onFinish={sendPhone}
                        onFinishFailed={onFinishFailedChangePass}
                        autoComplete="0n"
                    >
                        <FormItem  title='شماره موبایل' name='MobileNumber' pattern={mobile}>



                            <TextInput
                                htmlType={'tel'}
                            />
                        </FormItem>
                        <FormItem inputType={'password'} title='کلمه عبور' name={'NewPassword'}>


                            <PassInput className='bg-blue-700'/>
                        </FormItem>
                        <FormItem inputType={'password'} title='تکرار کلمه عبور' name={'ConfirmNewPassword'}>


                            <PassInput className='bg-blue-700'/>
                        </FormItem>


                        <div className="flex justify-between">
                            <Button onClick={() => changeStep(1)} className={'defBtn borderBtn'}>ورود با نام
                                کاربری</Button>

                            <Form.Item className='mb-0'>
                                <Button loading={loading} className='defBtn submitBtn' htmlType="submit">
                                    ارسال
                                </Button>
                            </Form.Item>
                        </div>


                    </Form>
                    :
                    <OtpBox loadingSendPhone={loading} url={ChangeForgottenPassword} changeStep={changeStep}
                            setStep={() =>
                            {
                                changeStep()
                                setStep(1)
                            }} addValues={{
                        MobileNumber: form.getFieldValue('MobileNumber'),
                        NewPassword: form.getFieldValue('NewPassword'),
                        ConfirmNewPassword: form.getFieldValue('ConfirmNewPassword')
                    }}
                            MobileNumber={phone} sendPhone={() => sendPhone({
                        MobileNumber: form.getFieldValue('MobileNumber'),
                        NewPassword: form.getFieldValue('NewPassword'),
                        ConfirmNewPassword: form.getFieldValue('ConfirmNewPassword')
                    })}
                            NewPassword={form.getFieldValue('NewPassword')}
                            ConfirmNewPassword={form.getFieldValue('ConfirmNewPassword')}
                    />
            }       </div>
    );
};

export default ChangePass;