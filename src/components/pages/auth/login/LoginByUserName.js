import React, {useEffect, useState} from 'react';
import {Button, Form} from 'antd';
import {useNavigate} from "react-router-dom";

import Cookies from 'js-cookie'
import {ShowNotification} from "../../../helper/ShowNotification";
import Request from "../../../helper/request/Request";
import {Login} from "../../../helper/apiList/apiListBusiness";
import TextInput from "../../../microComponents/common/uikits/TextInput";
import FormItem from "../../../microComponents/common/uikits/FormItem";
import PasswordInput from "../../../microComponents/common/uikits/PassInput";
import ChangePass from "./ChangePass";

const LoginByUserName = ({changeTextLoginTab}) => {
    let [step, setStep] = useState(1)
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate();
    const onFinish = (values) => {
        setLoading(true)

        Request.init(Login, async function (res) {
            setLoading(false)

            await Cookies.set('Token', res.data.jwtToken)
            await Cookies.set('RefreshToken', res.data.refreshToken)


            await navigate('/')
        }, function (error) {
            setLoading(false)
            ShowNotification('نام کاربری یا کلمه عبور اشتباه است!', false)

        }).loginPages().setRouter(navigate).setMainData(values).callRequest()


    };
useEffect(()=>{
    changeTextLoginTab(step)
},[step])
    const onFinishFailed = (errorInfo) => {
        console.log(errorInfo)
    };

    return (<div className="formBox py-0">

        {step === 1 ? <Form
                name="basic"
                layout="vertical"
                initialValues={{rule: true}}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="on"
            >

                <FormItem inputType={'number'} title='شماره موبایل' name={'UserName'} patternType={'MobileNumber'}>


                    <TextInput className='bg-blue-700'/>
                </FormItem>
                <FormItem inputType={'password'} title='کلمه عبور' name={'password'}>


                    <PasswordInput className='bg-blue-700'/>
                </FormItem>


                <div className="flex justify-between">
                    <Button onClick={() => setStep(2)} className={'defBtn borderBtn'}>فراموشی رمز عبور</Button>

                    <Form.Item className='mb-0'>
                        <Button loading={loading} className='defBtn submitBtn' htmlType="submit">
                            ورود
                        </Button>
                    </Form.Item>
                </div>
            </Form> :

            <ChangePass changeStep={() => setStep(1)}/>
        }

    </div>);
};


export default LoginByUserName;