import React, {useState} from 'react';
import {Tabs} from 'antd';
import {NavLink} from "react-router-dom";
import LoginByUserName from "./LoginByUserName";
import LoginByOtp from "./LoginByOtp";


const Login = () => {
    let [textType, setTextType] = useState()
    let tabItems = [
        {
            label: `${textType == 1 ? 'ورود با نام کاربری' : 'فراموشی رمز عبور'}`,
            key: 1,
            children: <LoginByUserName changeTextLoginTab={(x) => setTextType(x)}/>,
        },
        {
            label: `ورود با رمز یکبارمصرف  `,
            key: 2,
            children: <LoginByOtp/>,
        },
    ]
    return (
        <div className='flex w-full h-screen  '>
            <div className=' max-w-sm w-full mx-auto mt-20  '>
                <div className='  '>
                    <div className='defBox bgImageBox'>


                        <div className="logoBox d-flex">
                            <img className='m-auto' width={'50%'} src="/logo1.png" alt="logo"/>
                        </div>
                        <Tabs
                            className='centerTab'
                            defaultActiveKey="1"
                            items={tabItems}
                        />


                    </div>
                    <NavLink to='/register'><p className='text-left mt-3 text-sm'>ثبت نام در سامانه</p></NavLink>
                </div>
            </div>

        </div>

    );
};

export default Login;