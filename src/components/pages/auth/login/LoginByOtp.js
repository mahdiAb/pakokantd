import React, {useState} from 'react';
import {Button, Form} from 'antd';


import {useNavigate} from "react-router-dom";
import {ShowNotification} from "../../../helper/ShowNotification";
import {GenerateSMSOTP} from "../../../helper/apiList/apiListBusiness";
import Request from "../../../helper/request/Request";
import FormItem from "../../../microComponents/common/uikits/FormItem";
import TextInput from "../../../microComponents/common/uikits/TextInput";
import OtpBox from "../OtpBox";
import {mobile} from "../../../helper/validation/Validation";

const LoginByOtp = () => {
    const [form] = Form.useForm();
    let [step, setStep] = useState(1)
    const navigate = useNavigate();
    const [phone, setPhone] = useState();

    const [loadingSendPhone, setLoadingSendPhone] = useState(false);


    const sendPhone = (values) => {
        console.log(values)
        setStep(2)

        setPhone(values.MobileNumber)
        setLoadingSendPhone(true)
         Request.init(GenerateSMSOTP, function (data) {
            setLoadingSendPhone(false)
            if (data.data.messageID === 0) {
                setStep(2)
            } else {
                ShowNotification(  data.data.message ,false)
            }


            console.log(data)

        }, function (error) {
            console.log(error)
            setLoadingSendPhone(false)

        }).loginPages().setRouter(navigate).setMainData(values).callRequest()


    }


    const onFinishFailedSendPhone = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };


    return (<div className="formBox py-0 px-1">   {step === 1 ? <Form
                form={form}
                name="basic"
                layout="vertical"
                onFinish={sendPhone}
                onFinishFailed={onFinishFailedSendPhone}
                autoComplete="on"
            >


                <FormItem htmlType={'tel'} title='شماره موبایل' name={'MobileNumber'}
                          pattern={mobile}>


                    <TextInput/>
                </FormItem>


                <div className="d-flex justify-content-end">



                    <div className="flex justify-end">


                        <Form.Item className='mb-0'>



                            <Button loading={loadingSendPhone} className='defBtn submitBtn' htmlType="submit">
                                ارسال
                            </Button>



                        </Form.Item>
                    </div>




                </div>
            </Form> :

             <OtpBox loadingSendPhone={loadingSendPhone} addValues={{MobileNumber: form.getFieldValue('MobileNumber')}}
                    setStep={() => setStep(1)}
                    MobileNumber={phone} sendPhone={()=>sendPhone({MobileNumber:form.getFieldValue('MobileNumber')})}/>


        }


        </div>
    );
};
export default LoginByOtp;