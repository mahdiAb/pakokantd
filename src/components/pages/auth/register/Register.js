import React, {useState} from 'react';
import {NavLink, useNavigate} from "react-router-dom";
import Request from "../../../helper/request/Request";
import {Login, RegisterUser} from "../../../helper/apiList/apiListBusiness";
import {ShowNotification} from "../../../helper/ShowNotification";
import {Button, Checkbox, Form} from "antd";
import FormItem from "../../../microComponents/common/uikits/FormItem";
import TextInput from "../../../microComponents/common/uikits/TextInput";
import Cookies from "js-cookie";
import OtpBox from "../OtpBox";
import {mobile} from "../../../helper/validation/Validation";


// let initialValues = {
//     rule: false,
//     fname: '',
//     lname: '',
//     MobileNumber: '',
//
// }


const Register = () => {
    let [step, setStep] = useState(1)
    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);
    const [form] = Form.useForm();

    const handleGenerateSMSOTP = (values) => {
        setLoading(true)


        Request.init(RegisterUser, function (data) {
            setLoading(false)
            if (data.data.messageID === 0) {
                setStep(2)
            } else {
                ShowNotification(data.data.message, false)

            }
        }, function (error) {
            setLoading(false)

        }).loginPages().setRouter(navigate).setMainData(values).callRequest()


    };
    // const onFinish = (values) => {
    //     setLoading(true)
    //
    //     Request.init(Login, async function (res) {
    //         setLoading(false)
    //
    //         await Cookies.set('Token', res.data.jwtToken)
    //         await Cookies.set('RefreshToken', res.data.refreshToken)
    //
    //
    //         await navigate('/')
    //     }, function (error) {
    //         setLoading(false)
    //         ShowNotification('نام کاربری یا کلمه عبور اشتباه است!', false)
    //
    //     }).loginPages().setRouter(navigate).setMainData(values).callRequest()
    //
    //
    // };

    const onFinishFailed = (errorInfo) => {
        console.log(errorInfo)
    };

    return (
        <div className='flex w-full h-screen  '>
            <div className=' max-w-sm w-full mx-auto mt-20  '>
                <div className='  '>
                    <div className=' defBox bgImageBox '>


                        <div className="logoBox d-flex">
                            <img className='m-auto' width={'50%'} src="/logo1.png" alt="logo"/>
                        </div>
                        <div className="formBox py-0">

                            {step === 1 ? <Form  form={form}
                                    name="basic"
                                    layout="vertical"
                                    initialValues={{rule: true}}
                                    onFinish={handleGenerateSMSOTP}
                                    onFinishFailed={onFinishFailed}
                                    autoComplete="on"
                                >

                                    <FormItem title='نام' name={'fname'} required>


                                        <TextInput/>
                                    </FormItem>
                                    <FormItem title='نام خانوادگی' name={'lname'} required>


                                        <TextInput/>
                                    </FormItem>
                                    <FormItem inputType={'tel'} title='شماره موبایل' name={'MobileNumber'} pattern={mobile}
                                              required>


                                        <TextInput/>
                                    </FormItem>



                                    <Form.Item
                                        name="rule"
                                        valuePropName="checked"
                                        rules={[   {
                                            validator: (_, value) =>
                                                value ? Promise.resolve() : Promise.reject(new Error('برای ثبت نام نیاز است که قوانین و مقررات پاکوک را مطالعه و قبول کنید.')),
                                        } ]}
                                    >
                                        <Checkbox><NavLink className={'text-blue'} target='_blank' to={'http://pakok.ir/rules'}>قوانین و مقررات پاکوک</NavLink> را مطالعه نموده و با کلیه موارد آن موافقم.</Checkbox>
                                    </Form.Item>



                                    <div className="flex justify-end">


                                        <Form.Item className='mb-0'>
                                            <Button loading={loading} className='defBtn submitBtn' htmlType="submit">
                                                ارسال
                                            </Button>
                                        </Form.Item>
                                    </div>
                                </Form> :

                                <OtpBox setStep={() => setStep(1)} addValues={{
                                    rule: form.getFieldValue('rule'),
                                    MobileNumber: form.getFieldValue('MobileNumber'),
                                    lname: form.getFieldValue('lname'),
                                    fname: form.getFieldValue('fname')
                                }}
                                        sendPhone={handleGenerateSMSOTP}
                                        loadingSendPhone={loading}/>
                            }

                        </div>


                    </div>
                    <NavLink to='/login'><p className='text-left mt-3 text-sm'>ورود سامانه</p></NavLink>
                </div>
            </div>

        </div>



    )
}


export default Register;