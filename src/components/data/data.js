export const data2 = [
    {
        ID: 1,
        ProductID: null,
        ProductDescription: null,
        Value: null,
        UnitAmount: null,
        DiscountAmount: null,

    },



]


let maxID = data2[data2.length - 1].ID;

export const getMaxID = () => {
    maxID += 1;
    return maxID;

}


export const initialDataCreateInvoice = [
    {


        'productID': null,

        'productDescription': "",
        'value': 0,
        'unitAmount': 0,
        'taxRate': 0,
        'discountAmount': 0,
        'amountBeforDiscount': 0,
        'amountAfterDiscount': 0,
        'otherTaxRate': 0,
        'legalAmountRate': 0,
        'amount': 0,
        'taxAmount': 0,
        'otherTaxAmount': 0,
        'legalAmount': 0,


    },

]


export const invoiceSettlementMethodIDJson = [
    {
        value: '1',
        label: 'پرداخت نقدی',
    },
    {
        value: '2',
        label: 'پرداخت نسیه',
    },
    {
        value: '3',
        label: 'پرداخت نقدی نسیه',
    },

]