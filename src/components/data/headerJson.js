import React from 'react';
import {HiOutlineUsers} from "@react-icons/all-files/hi/HiOutlineUsers";
import Invoice from "../pages/services/invoice/Invoice";
import Setting from "../pages/services/setting/Setting";
import {MdPlaylistAddCheck} from "@react-icons/all-files/md/MdPlaylistAddCheck";
import {MdAttachMoney} from "@react-icons/all-files/md/MdAttachMoney";
import Financial from "../pages/financial/Financial";


export const tabsData = [
    {
        id: 0,
        text: 'صورت حساب',
        icon: <MdPlaylistAddCheck size={20}/>,
        content: <Invoice/>,
    },
    {
        id: 1,
        text: 'اطلاعات پایه',
        icon: <HiOutlineUsers size={16}/>,
        content: <Setting/>,
    },
    {
        id: 1,
        text: 'مالی',
        icon: <MdAttachMoney size={18}/>,
        content: <Financial/>,
    },
    // {
    //     id: 0,
    //     text: 'اقساط',
    //     icon: <GrMoney size={16}/>,
    //     content: <Installment/>,
    // },
    // {
    //     id: 1,
    //     text: 'حسابداری',
    //     icon: <GiMoneyStack size={16}/>,
    //     content: <Acc/>,
    //
    // },
    // {
    //     id: 2,
    //     text: 'مدیریت محتوا',
    //     icon: <BsReverseLayoutTextWindowReverse size={16}/>,
    //     content: 'Find ssstab content',
    // },
    // {
    //     id: 3,
    //     text: 'نوبتدهی',
    //     icon: <HiOutlineUsers size={16}/>,
    //     content: 'Find tab content',
    // },



];


