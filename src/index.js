import React from 'react';
import ReactDOM from 'react-dom/client';
import './styles/FarsiNumerals/fontiran.css'
import './styles/FarsiNumerals/style.css'
import {Provider} from "react-redux";
import store from "./components/redux/store/store";
import 'ag-grid-community/styles/ag-theme-alpine.css';
import 'ag-grid-community/styles/ag-grid.css';
import './styles/formItems.css'
import './styles/servicesTabs.css'
import './styles/sidebar.css'
import './styles/table.css'
import './styles/paginate.css'
import './styles/notification.css'

import './index.css'
import './styles/header.css'
import './styles/CommitTransaction.css'
import './styles/globals.css'
import {createBrowserRouter, RouterProvider,} from "react-router-dom";
import Main from "./components/pages/services/Main";
import Login from "./components/pages/auth/login/Login";
import BusinessList from "./components/pages/Business/BusinessList";

import {Helmet} from "react-helmet";

import {ConfigProvider} from "antd";
import {JalaliLocaleListener} from "antd-jalali";
import fa_IR from "antd/lib/locale/fa_IR";
import "antd/dist/reset.css";
import CreateBusiness from "./components/pages/Business/new/CreateBusiness";
import Order from "./components/pages/Business/Order";
import Register from "./components/pages/auth/register/Register";
import EditBusiness from "./components/pages/Business/new/EditBusiness";
import ExtensionBusiness from "./components/pages/Business/ExtensionBusiness/ExtensionBusiness";
import UpgradeBusiness from "./components/pages/Business/upgradeBusiness/UpgradeBusiness";
import CommitTransaction from "./components/pages/Payment/CommitTransaction/CommitTransaction";


const router = createBrowserRouter([
    {
        path: "/",
        element: <BusinessList/>,
    },
    {
        path: "/createBusiness",
        element: <CreateBusiness/>,
    },
    {
        path: "/Transaction/:id",
        element: <CommitTransaction/>,
    },
    {
        path: "/editBusiness/:id",
        element: <EditBusiness/>,
    },
    {
        path: "/ExtensionBusiness/:id",
        element: <ExtensionBusiness/>,
    },
    {
        path: "/upgradeBusiness/:id",
        element: <UpgradeBusiness/>,
    },
    {
        path: "/business/:id",
        element: <Main/>,
    },
    {
        path: "/order/:id",
        element: <Order/>,
    },

    {
        path: "/login",
        element: <Login/>,
    },
    {
        path: "/register",
        element: <Register/>,
    },



]);
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Provider store={store}> <ConfigProvider locale={fa_IR}  direction="rtl">
        <Helmet>
            <meta charSet="utf-8" />
            <title>pakok</title>
        </Helmet>
        <JalaliLocaleListener/>
        <RouterProvider router={router}/>

    </ConfigProvider>

    </Provider>
);
